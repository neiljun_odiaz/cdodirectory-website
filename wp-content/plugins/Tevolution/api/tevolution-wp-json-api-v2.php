<?php
/**
 * This file use for set tevolution curl.
 * Curl file is make for faster result responce.
 *
 * @package WordPress
 * @subpackage Tevolution
 **/

if (! defined( 'ABSPATH' )) {
    exit;
}

add_action( 'rest_api_init', 'tmpl_get_post_city_slug' );
/**
 * City slug.
 */
function tmpl_get_post_city_slug()
{
    register_rest_route( 'tevolution/v2', '/city/(?P<slug>[\w-]+)', array(
        'methods' => 'GET',
        'callback' => 'get_post_city_slug',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_popular_posts' );
/**
 * Get popular posts.
 */
function tmpl_get_popular_posts()
{
    register_rest_route( 'tevolution/v2', '/popular/post/(?P<post_type>[\w-]+)/popular-per/(?P<popular_per>[\w-]+)/number/(?P<number>[\w-_0-9]+)', array(
        'methods' => 'GET',
        'callback' => 'get_popular_posts',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_recent_review_comments' );
/**
 * Get recent review comments.
 */
function tmpl_get_recent_review_comments()
{
    register_rest_route( 'tevolution/v2', '/recent/review', array(
        'methods' => 'GET',
        'callback' => 'get_recent_review_comments',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_browse_by_category_tag' );
/**
 * Get browse by category tag.
 */
function tmpl_get_browse_by_category_tag()
{
    register_rest_route( 'tevolution/v2', '/browse/category-tag', array(
        'methods' => 'GET',
        'callback' => 'get_browse_by_category_tag',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_neighborhood' );
/**
 * Get neighborhood.
 */
function tmpl_get_neighborhood()
{
    register_rest_route( 'tevolution/v2', '/browse/neighborhood', array(
        'methods' => 'GET',
        'callback' => 'get_neighborhood',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_event_calendar_widget_datewise' );
/**
 * Get event calendar widget datewise.
 */
function tmpl_get_event_calendar_widget_datewise()
{
    register_rest_route( 'tevolution/v2', '/event/datewise', array(
        'methods' => 'GET',
        'callback' => 'get_event_calendar_widget_datewise',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_all_categories_list_widget' );
/**
 * Get all categories list widget.
 */
function tmpl_get_all_categories_list_widget()
{
    register_rest_route( 'tevolution/v2', '/browse/all-categories-list', array(
        'methods' => 'GET',
        'callback' => 'get_all_categories_list_widget',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_display_post_widget' );
/**
 * Get all display post widget.
 */
function tmpl_get_display_post_widget()
{
    register_rest_route( 'tevolution/v2', '/display/post-widget', array(
        'methods' => 'GET',
        'callback' => 'get_all_display_post_widget_widget',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_neighborhood_widget' );
/**
 * Get all neighborhood post widget.
 */
function tmpl_get_neighborhood_widget()
{
    register_rest_route( 'tevolution/v2', '/neighborhood/post-widget', array(
        'methods' => 'GET',
        'callback' => 'get_all_neighborhood_post_widget_widget',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_city_post_type_with_postnumber' );
/**
 * Get city post type.
 */
function tmpl_get_city_post_type_with_postnumber()
{
    register_rest_route( 'tevolution/v2', '/city/(?P<slug>[\w-]+)/post/type/(?P<type>[\w-]+)/number/(?P<number>[\w-_0-9]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_post_type',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_city_post_type' );
/**
 * Get city post type all.
 */
function tmpl_get_city_post_type()
{
    register_rest_route( 'tevolution/v2', '/city/(?P<slug>[\w-]+)/post/type/(?P<type>[\w-]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_post_type',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_tevolution_post_type' );
/**
 * Get city post type all.
 */
function tmpl_get_tevolution_post_type()
{
    register_rest_route( 'tevolution/v2', '/post/type/(?P<type>[\w-]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_post_type',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_city_type_category' );
/**
 * Get city type category
 */
function tmpl_get_city_type_category()
{
    register_rest_route( 'tevolution/v2', '/city/(?P<slug>[\w-]+)/post/type/(?P<type>[\w-]+)/taxonomy/(?P<taxonomy>[\w-]+)/category_id/(?P<category_id>[\w-]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_type_category',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_templatic_category' );
/**
 * Get city type category.
 */
function tmpl_get_templatic_category()
{
    register_rest_route( 'tevolution/v2', '/post/type/(?P<type>[\w-]+)/taxonomy/(?P<taxonomy>[\w-]+)/category_id/(?P<category_id>[\w-]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_type_category',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_city_type_category_name' );
/**
 * Get city type category name.
 */
function tmpl_get_city_type_category_name()
{
    register_rest_route( 'tevolution/v2', '/city/(?P<slug>[\w-]+)/post/type/(?P<type>[\w-]+)/taxonomy/(?P<taxonomy>[\w-]+)/category_name/(?P<category_name>[\w-_0-9]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_type_category_name',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_templatic_category_name' );
/**
 * Get city type category name.
 */
function tmpl_get_templatic_category_name()
{
    register_rest_route( 'tevolution/v2', '/post/type/(?P<type>[\w-]+)/taxonomy/(?P<taxonomy>[\w-]+)/category_name/(?P<category_name>[\w-_0-9]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_type_category_name',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_city_type_tag_name' );
/**
 * Get city type tag name.
 */
function tmpl_get_city_type_tag_name()
{
    register_rest_route( 'tevolution/v2', '/city/(?P<slug>[\w-]+)/post/type/(?P<type>[\w-]+)/taxonomy/(?P<taxonomy>[\w-]+)/tag_name/(?P<tag_name>[\w-_0-9]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_type_tag_name',
    ) );
}

add_action( 'rest_api_init', 'tmpl_get_templatic_tag_name' );
/**
 * Get city type tag name.
 */
function tmpl_get_templatic_tag_name()
{
    register_rest_route( 'tevolution/v2', '/post/type/(?P<type>[\w-]+)/taxonomy/(?P<taxonomy>[\w-]+)/tag_name/(?P<tag_name>[\w-_0-9]+)', array(
        'methods' => 'GET',
        'callback' => 'get_city_type_tag_name',
    ) );
}


/**
 * Return popular posts in widget Popular Posts.
 *
 * @param string $post_type                 Post Type.
 * @param string $popular_per               Popular as per.
 * @param number $number                    Number of popular post.
 */
function get_popular_posts($post_type, $popular_per, $number)
{

    if ('views' == $popular_per) {
            $args_popular = array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',
                    'posts_per_page' => $number,
                    'meta_key' => 'viewed_count',
                    'orderby' => 'meta_value_num',
                    'meta_value_num' => 'viewed_count',
                    'order' => 'DESC',
            );
    } elseif ('dailyviews' == $popular_per) {
            $args_popular = array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',
                    'posts_per_page' => $number,
                    'meta_key' => 'viewed_count_daily',
                    'orderby' => 'meta_value_num',
                    'meta_value_num' => 'viewed_count_daily',
                    'order' => 'DESC',
            );
    } else {
            $args_popular = array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',
                    'posts_per_page' => $number,
                    'orderby' => 'comment_count',
                    'order' => 'DESC',
            );
    }

    $location_post_type = get_option( 'location_post_type' );
    if (is_array( $location_post_type ) && ! empty( $location_post_type )) {
        foreach ($location_post_type as $location_post_types) {
                  $post_types = explode( ',', $location_post_types );
                  $post_type1[] = $post_types[0];
        }
    }

    /* filter for current city wise populer posts */
    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) && in_array( $post_type, $post_type1 )) {
            add_filter( 'posts_where', 'location_multicity_where' );
    }

    $popular_post_query = new WP_Query( $args_popular );

    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
            remove_filter( 'posts_where', 'location_multicity_where' );
    }

    // $response = new WP_JSON_Response();
    // $response->set_data($popular_post_query->posts);
    return $popular_post_query->posts;
}

/**
 * Function for getting recent comments.
 *
 * @param array $filter                 Argument to show recente review.
 */
function get_recent_review_comments( WP_REST_Request $request)
{
    $filter = $request->get_param( 'filter' );
    $g_size = isset( $filter['g_size'] )? $filter['g_size'] : 30;
    $no_comments = isset( $filter['count'] )? $filter['count'] : 10;
    $comment_lenth = isset( $filter['comment_lenth'] )? $filter['comment_lenth'] : 60;
    $post_type = isset( $filter['post_type'] )? $filter['post_type'] : 'post';
    $title = isset( $filter['title'] )? $filter['title'] : '';

    global $wpdb, $tablecomments, $tableposts,$rating_table_name;
    $tablecomments = $wpdb->comments;
    $tableposts    = $wpdb->posts;

    if (post_type_exists( $post_type )) {
        $post_type = $post_type;
    } else {
        $post_type = 'post';
    }
    $args = array(
        'status' => 'approve',
        'karma' => '',
        'number' => $no_comments,
        'offset' => '',
        'orderby' => 'comment_date',
        'order' => 'DESC',
        'post_type' => $post_type,
    );
    $location_post_type = get_option( 'location_post_type' );
    if (is_array( $location_post_type ) && ! empty( $location_post_type )) {
        foreach ($location_post_type as $location_post_types) {
                $post_types = explode( ',', $location_post_types );
                $post_type1[] = $post_types[0];
        }
    }

    /* filter for current city wise populer posts */
    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) && in_array( $post_type, $post_type1 )) {
        add_filter( 'comments_clauses', 'location_comments_clauses' );
    }
    $comments = get_comments( $args );
    /* remove filter for current city wise populer posts */
    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) && in_array( $post_type, $post_type1 )) {
        remove_filter( 'comments_clauses', 'location_comments_clauses' );
    }
    $html = '';
    if ($comments) {
        if ( '' != $title ) {
            $html .= '<h3 class="widget-title">' . $title . '</h3>';
        }
        $html .= '<ul class="recent_comments">';
        foreach ($comments as $comment) {
            $comment_id           = $comment->comment_ID;
            $comment_content      = strip_tags( $comment->comment_content );
            $comment_excerpt      = wp_trim_words( $comment_content, $comment_lenth, '' );
            $permalink            = get_permalink( $comment->ID ) . '#comment-' . $comment->comment_ID;
            $comment_author_email = $comment->comment_author_email;
            $comment_post_ID      = $comment->comment_post_ID;
            $post_title           = stripslashes( get_the_title( $comment_post_ID ) );
            $permalink            = get_permalink( $comment_post_ID );
            $size = 60;
            $html .= "<li class='clearfix'><span class=\"li" . $comment_id . '">';
            if ('' == @$comment->comment_type || 'comment' == $comment->comment_type) {
                $html .= '<a href="' . $permalink . '">';
                if (get_user_meta( $comment->user_id, 'profile_photo', true )) {
                    $html .= '<img class="avatar avatar-' . absint( $size ) . ' photo" width="' . absint( $size ) . '" height="' . absint( $size ) . '" src="' . get_user_meta( $comment->user_id, 'profile_photo', true ) . '" />';
                } else {
                    $html .= get_avatar( $comment->comment_author_email, 60 );
                }

                $html .= '</a>';
            } elseif (('trackback' == $comment->comment_type) || ('pingback' == $comment->comment_type)) {
                $html .= '<a href="' . $permalink . '">';
                if (get_user_meta( $comment->user_id, 'profile_photo', true )) {
                    $html .= '<img class="avatar avatar-' . absint( $size ) . ' photo" width="' . absint( $size ) . '" height="' . absint( $size ) . '" src="' . get_user_meta( $comment->user_id, 'profile_photo', true ) . '" />';
                } else {
                    $html .= get_avatar( $comment->comment_author_email, 60 );
                }
                $html .= '</a>';
            }
            $html .= "</span>\n";
            $html .= '<div class="review_info" >' ;
            $html .= '<a href="' . $permalink . '" class="title">' . $post_title . '</a>';
            $tmpdata = get_option( 'templatic_settings' );
            $rating_table_name = $wpdb->prefix . 'ratings';
            if ($tmpdata['templatin_rating'] == 'yes') :
                $post_rating = $wpdb->get_var( $wpdb->prepare( "select rating_rating from $rating_table_name where comment_id='%d'",$comment_id ) );
                if (function_exists( 'draw_rating_star_plugin' )) {
                    /*fetch rating from tevolution.*/
                    $html .= "<div class='rating'>" . apply_filters( 'tmpl_show_tevolution_rating', '', $post_rating ) . '</div>';
                }
            endif;
            if (function_exists( 'tmpl_rating_html' )) {
                $html .= tmpl_rating_html( $comment_id, true );
            }
            $html .= $comment_excerpt;
            if (function_exists( 'supreme_prefix' )) {
                $pref = supreme_prefix();
            } else {
                $pref = sanitize_key( apply_filters( 'hybrid_prefix', get_template() ) );
            }
            $theme_options = get_option( $pref . '_theme_settings' );
            if (isset( $theme_options['templatic_excerpt_link'] ) && $theme_options['templatic_excerpt_link'] != '') {
                $read_more = $theme_options['templatic_excerpt_link'];
            } else {
                $read_more = esc_html__( 'Read more &raquo;', 'templatic' );
            }
            $view_comment = esc_html__( 'View the entire comment', 'templatic' );
            $html .= '<a class="comment_excerpt" href="' . $permalink . '" title="' . $view_comment . '">';
            $html .= '&nbsp;' . $read_more;
            $html .= '</a></div>';
            $html .= '</li>';
        } // End foreach().
        $html .= '</ul>';
    } // End if().

    $result[] = $html;

    // $response = new WP_JSON_Response();
    // $response->set_data($result);
    return $result;
}

/**
 * Widget Browse by category and tag.
 *
 * @param array $filter                 Argument for browse by category widget.
 */
function get_browse_by_category_tag($filter = array())
{
    global $current_cityinfo;
    $browseby = isset( $filter['browseby'] )? $filter['browseby'] : '';
    $categories_count = isset( $filter['categories_count'] )? $filter['categories_count'] : 0;
    $post_type = isset( $filter['post_type'] ) ? apply_filters( 'widget_post_type', $filter['post_type'] ): '';

    /* Get all the taxonomies for this post type */
    $taxonomies = get_object_taxonomies( (object) array(
        'post_type' => $post_type,
        'public' => true,
        '_builtin' => true,
    ) );

    if ('tags' == $browseby) {
        if ('post' != $post_type) {
                    $taxo = $taxonomies[1];
        } else {
                    $taxo = 'ptags';
        }
    } else {
        if ('post' != $post_type) {
                    $taxo = $taxonomies[0];
        } else {
                    $taxo = 'category';
        }
    }

    if (is_plugin_active( 'woocommerce/woocommerce.php' ) && $filter['post_type'] == 'product') {
            $taxo = $taxonomies[1];
    }

    $html = '';
    /* browse by tags */
    if ($browseby == 'tags') {
        $html .= '<div class="browse_by_tag">';
        $args = array(
            'taxonomy' => $taxo,
        );

        if (false === ( $terms = get_transient( '_tevolution_query_browsetags' . $post_type . $cur_lang_code ) )  && get_option( 'tevolution_cache_disable' ) == 1) {
                $terms = get_terms( $taxo, $args );
                set_transient( '_tevolution_query_browsetags' . $post_type . $cur_lang_code, $terms, 12 * HOUR_IN_SECONDS );
        } elseif (get_option( 'tevolution_cache_disable' ) == '') {
                $terms = get_terms( $taxo, $args );
        }

        if ($terms) :
                $html .= '<ul>';
            foreach ($terms as $term) {
                if ($taxo != '' && $term->slug != '') {
                    $html .= "<li><a href='" . get_term_link( $term->slug, $taxo ) . "'>" . __( $term->name, 'templatic' ) . '</a></li>';
                }
            }
                $html .= '</ul>';
        else :
                $html .= esc_html__( 'No Tag Available', 'templatic' );
        endif;

        $html .= '</div>';
    } else {
        /* browse by categories */

        $cat_args = array(
                            'taxonomy' => $taxo,
                            'orderby' => 'name',
                            'show_count' => $categories_count,
                            'hide_empty' => 1,
                            'echo'     => 0,
                            'hierarchical' => 'true',
                            'pad_counts' => 0,
                            'title_li' => '',
        );

        $transient_name = ( ! empty( $current_cityinfo ))? $current_cityinfo['city_slug']: '';
        if (false === ( $widget_category_list = get_transient( '_tevolution_query_browsecategories' . $post_type . $transient_name . $cur_lang_code )) && get_option( 'tevolution_cache_disable' ) == 1) {
                do_action( 'tevolution_category_query' );
                $widget_category_list = wp_list_categories( $cat_args );
                set_transient( '_tevolution_query_browsecategories' . $post_type . $transient_name . $cur_lang_code, $widget_category_list, 12 * HOUR_IN_SECONDS );
        } elseif (get_option( 'tevolution_cache_disable' ) == '') {
                do_action( 'tevolution_category_query' );
                $widget_category_list = wp_list_categories( $cat_args );
        }

        $html .= '<ul class="browse_by_category">';
        $html .= $widget_category_list;
        $html .= '</ul>';
    }// End if().

    $result[] = $html;

    return $result;
}

/**
 * Event plugin event-calender mouse hover return result.
 *
 * @param array $filter                 Argument for event calendar widget.
 */
function get_event_calendar_widget_datewise( WP_REST_Request $request ) {

    $filter = $request->get_param( 'filter' );
    global $post,$wpdb,$current_cityinfo;

    $todaydate = isset( $filter['date'] )? $filter['date'] : '';
    $urlddate = isset( $filter['urlddate'] )? $filter['urlddate'] : 0;

    $page_id = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts where post_content like '%[calendar_event]%' and post_type='page' and post_status='publish' limit 0,1" ) );
    if (function_exists( 'icl_object_id' )) {
        $page_id = icl_object_id( $page_id, 'page', false );
    }
    /*set link of calendar as per wpml*/
    $thelink = get_permalink( $page_id ) . "?cal_date=$urlddate";
    /*register post status recurring*/
    register_post_status( 'recurring' );
    /*query to fetch events to show on calendar widget*/

    $args = array(
    'post_type' => 'event',
                    'posts_per_page' => 5,
                    'post_status' => array( 'recurring','publish' ),
                    'meta_key' => 'st_date',
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'meta_query' => array(
                    'relation' => 'AND',
                        array(
                                'key' => 'st_date',
                                'value' => $todaydate,
                                'compare' => '<=',
                                'type' => 'DATE',
                        ),
                        array(
                                'key' => 'end_date',
                                'value' => $todaydate,
                                'compare' => '>=',
                                'type' => 'DATE',
                                ),
                        array(
                                'key' => 'event_type',
                                'value' => 'Regular event',
                                'compare' => '=',
                                'type' => 'text',
                        ),
                    ),
                );
    $location_post_type = get_option( 'location_post_type' );
    if (is_array( $location_post_type ) && count( $location_post_type ) > 1) {
        $location_post_type = implode( ',', $location_post_type );
    } else {
        $location_post_type = $location_post_type[0];
    }
    /*if location manager plugin is activated and events post type is selected in manage location than fetch events city wise*/
    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) && in_array( 'event', $location_post_type )) {
        global $cityid;
        if (isset( $_REQUEST['city_id'] )) {
            $cityid = intval( $_REQUEST['city_id'] );
        }
        add_filter( 'posts_where', 'location_city_filter' );
    }
    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )  && strstr( $location_post_type, 'event,' )) {
        add_filter( 'posts_where', 'location_multicity_where' );
    }

    $my_query1 = null;
    $my_query1 = new WP_Query( $args );
    
    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) && in_array( 'event', $location_post_type )) {
        remove_filter( 'posts_where', 'location_multicity_where' );
    }
    $post_info = '';
    global $posts;
    if ($my_query1->have_posts()) {
        while ($my_query1->have_posts()) :
            $my_query1->the_post();

            /* separate out recurring events with regular events */
            $is_recurring = get_post_meta( $post->ID, 'event_type', true );
                          /*html for recurring event in pop up*/
            if (tmpl_is_parent( $post )) {
                $post_info .= '
                <a class="event_title" href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a><small>' .
                '<span class="wid_event_list"><b class="label">' . esc_html__( 'Location :', 'templatic' ) . '</b><b class="label_info">' . get_post_meta( $post->ID, 'address', true ) . '</b></span>' .
                '<span class="wid_event_list"><b class="label">' . esc_html__( 'Start Date :', 'templatic' ) . '</b><b class="label_info">' . get_formated_date( get_post_meta( $post->ID, 'st_date', true ) ) . ' ' . get_formated_time( get_post_meta( $post->post_parent, 'st_time', true ) ) . '</b></span>' .
                '<span class="wid_event_list"><b class="label">' . esc_html__( 'End Date :', 'templatic' ) . '</b><b class="label_info">' . get_formated_date( get_post_meta( $post->ID, 'end_date', true ) ) . ' ' . get_formated_time( get_post_meta( $post->post_parent, 'end_time', true ) ) . '</b></span></small>';
            } else {
                /*html for regular event in pop up*/
                if (strtolower( $is_recurring ) == strtolower( 'Regular event' )) {
                    $post_info .= '
                    <a class="event_title" href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a><small>' .
                    '<span class="wid_event_list"><b class="label">' . esc_html__( 'Location :', 'templatic' ) . '</b><b class="label_info">' . get_post_meta( $post->ID, 'address', true ) . '</b></span>' .
                    '<span class="wid_event_list"><b class="label">' . esc_html__( 'Start Date :', 'templatic' ) . '</b><b class="label_info">' . get_formated_date( get_post_meta( $post->ID, 'st_date', true ) ) . ' ' . get_formated_time( get_post_meta( $post->ID, 'st_time', true ) ) . '</b></span>' .
                    '<span class="wid_event_list"><b class="label">' . esc_html__( 'End Date :', 'templatic' ) . '</b><b class="label_info">' . get_formated_date( get_post_meta( $post->ID, 'end_date', true ) ) . ' ' . get_formated_time( get_post_meta( $post->ID, 'end_time', true ) ) . '</b></span></small>';
                }
            }
        endwhile;
        if ($my_query1->found_posts > 5) {
            $post_info .= "<a class=\"more_events\" href=\"$thelink\" >" . esc_html__( 'View more', 'templatic' ) . '</a>';
        }
    } else {
        $post_info .= esc_html__( 'No Event in this date', 'templatic' );
    }

    $result[] = $post_info;
    // $response = new WP_JSON_Response();
    // $response->set_data($result);
    return $result;
}

/**
 * This function return the category icon, location theme only.
 *
 * @param url $term_icon                Category icon url.
 */
function tmpl_show_category_icon($term_icon)
{
    if ('' == $term_icon) {
            $term_icon = get_stylesheet_directory_uri() . '/images/map-icon.png';
    }

    return '<img src="' . $term_icon . '" alt=' . $term_icon . '/>';
}
/**
 * This function return All categories list.
 *
 * @param array $filter                 Argument for all categories list Widget.
 */
function get_all_categories_list_widget(WP_REST_Request $request ) {
    
    $filter = $request->get_param( 'filter' );
    
    global $current_cityinfo, $sitepress;
    $current_cityinfo['city_slug'] = $filter['city_slug'];
    $current_cityinfo['city_id'] = $filter['city_id'];
    $cur_lang_code = isset( $filter['language'] ) ? $filter['language']  : 'en';
    $post_type = isset( $filter['post_type'] )? $filter['post_type'] : 'listing';
    $category_level = isset( $filter['category_level'] )? $filter['category_level'] : 1;
    $number_of_category = isset( $filter['number_of_category'] )? $filter['number_of_category'] : 6;
    $hide_empty_cat = ($filter['hide_empty_cat'] == '') ? '0' : apply_filters( 'widget_hide_empty_cat', $filter['hide_empty_cat'] );
    $taxonomies = get_object_taxonomies( (object) array(
        'post_type' => $post_type,
        'public' => true,
        '_builtin' => true,
    ) );

    $args5 = array(
            'orderby' => 'name',
            'taxonomy' => $taxonomies[0],
            'order' => 'ASC',
            'parent' => '0',
            'show_count' => 0,
            'hide_empty' => $hide_empty_cat,
            'pad_counts' => true,
        );
    //echo "===".$cur_lang_code ;
	if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
		// $sitepress->switch_lang( $cur_lang_code ); This was removed because after new WPML updates it's not working.
		$cur_lang_code = ICL_LANGUAGE_CODE;
	}

        $html = '';
        $html .= '<section class="category_list_wrap row 00">';

        /* set wp_categories on transient */
    if (get_option( 'tevolution_cache_disable' ) == 1 && false === ( $categories = get_transient( '_tevolution_query_catwidget' . $post_type . $cur_lang_code ) )) {
        $categories = get_categories( $args5 );
        set_transient( '_tevolution_query_catwidget' . $post_type . $cur_lang_code, $categories, 12 * HOUR_IN_SECONDS );
    } elseif (get_option( 'tevolution_cache_disable' ) == '') {
                $categories = get_categories( $args5 );
    }

    if (! isset( $categories['errors'] )) {
        foreach ($categories as $category) {
                    /* set child wp_categories on transient */

            $transient_name = ( ! empty( $current_cityinfo ) ) ? $current_cityinfo['city_slug'] : '';
            if (function_exists( 'icl_object_id' )) {
				$en_cat_id =  icl_object_id( $category->term_id, 'classifiedscategory', true, $cur_lang_code) ;
			}else{
				$en_cat_id = $category->term_id;
			}
            
            if (get_option( 'tevolution_cache_disable' ) == 1 && false === ( $featured_catlist_list = get_transient( '_tevolution_query_catwidget' . $en_cat_id . $post_type . $transient_name . $cur_lang_code ) )) {
                do_action( 'tevolution_category_query' );
				if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
					$sitepress->switch_lang( $cur_lang_code );
				}
                $featured_catlist_list = wp_list_categories( 'title_li=&child_of=' . $en_cat_id . '&echo=0&depth=' . $category_level . '&number=' . $number_of_category . '&taxonomy=' . $taxonomies[0] . '&show_count=1&hide_empty=' . $hide_empty_cat . '&pad_counts=0&show_option_none=' );
                set_transient( '_tevolution_query_catwidget' . $en_cat_id . $post_type . $transient_name . $cur_lang_code, $featured_catlist_list, 12 * HOUR_IN_SECONDS );
            } elseif (get_option( 'tevolution_cache_disable' ) == '') {
                do_action( 'tevolution_category_query' );
				if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
					$sitepress->switch_lang( $cur_lang_code );
				}
                $featured_catlist_list = wp_list_categories( 'title_li=&child_of=' . $en_cat_id . '&echo=0&depth=' . $category_level . '&number=' . $number_of_category . '&taxonomy=' . $taxonomies[0] . '&show_count=1&hide_empty=' . $hide_empty_cat . '&pad_counts=0&show_option_none=' );
            }

            if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
                remove_filter( 'terms_clauses', 'locationwise_change_category_query', 10, 3 );
            }

            $parent = get_term( $category->term_id, $taxonomies[0] );
            if (1 == $hide_empty_cat) {
                if ($parent->count != 0 || $featured_catlist_list != '') {
                    if ($parent) {

                         $category_headimage = get_term_meta( $parent->term_id, 'category_headimage', true );

                            if ($category_headimage !='') {
                                $classn = 'no-cat-img';
                            }else{
                                $classn = '';
                            }

                        $html .= '<article class="category_list large-4 medium-4 small-6 xsmall-12 columns '.$classn.'">';
                        
                        if ($category_headimage !='') 
                        {

                            if($category_headimage ==''){
                                    $imageurl = get_theme_mod('header_imagecontrol');
                            }else{
                                    $imageurl = $category_headimage ;
                            }

                                $imgpath = $imageurl;
                                if($imgpath !=''){
                                     $html .= '<div class="catimg" style="background-image: url('.$imgpath.')""></div>';
                                }else{
                                    $html .= '<div class="catimg" style="background-image: url('.TEVOLUTION_PAGE_TEMPLATES_URL.'images/no-image-200x200.jpg)"></div>';
                                }
                        }
                        else
                        {
                            $html .= '<div class="catimg" style="background-image: url('.TEVOLUTION_PAGE_TEMPLATES_URL.'images/no-image-200x200.jpg)"></div>';
                        }

                        //$html .= '<article class="category_list large-4 medium-4 small-6 xsmall-12 columns">';
                        $parents = '<a href="' . get_term_link( $parent, $taxonomies[0] ) . '" title="' . esc_attr( $parent->name ) . '">' . apply_filters( 'list_cats', $parent->name, $parent ) . '</a><i class="fa fa-angle-right arrow-click"></i>';
                        if (1 == $hide_empty_cat) {
                            if (0 != $parent->count) {
                                $html .= '<h3>';
                                // Do_action('show_categoty_map_icon', $parent->term_icon);
                                if (function_exists( 'show_categoty_map_icon' )) {
                                    $html .= tmpl_show_category_icon( $parent->term_icon );
                                }
                                $html .= $parents;
                                $html .= '</h3>';
                            }
                        } else {
                            $html .= '<h3>';
                            // Do_action('show_categoty_map_icon', $parent->term_icon);
                            if (function_exists( 'show_categoty_map_icon' )) {
                                $html .= tmpl_show_category_icon( $parent->term_icon );
                            }
                            $html .= $parents;
                            $html .= '</h3>';
                        }

                        if (@$featured_catlist_list != '') {
                            if (0 != $number_of_category) {
                                if (0 == $parent->count) {
                                    $html .= '<h3>';
                                    // Do_action('show_categoty_map_icon', $parent->term_icon);
                                    if (function_exists( 'show_categoty_map_icon' )) {
                                        $html .= tmpl_show_category_icon( $parent->term_icon );
                                    }
                                    $html .= $parents;
                                    $html .= '</h3>';
                                }
                                $html .= '<ul>';
                                $html .= $featured_catlist_list;
                                $html .= '<li class="view"> <a href="' . get_term_link( $parent, $taxonomies[0] ) . '">';
                                $html .= esc_html__( 'View all &raquo;', 'templatic' );
                                $html .= '</a> </li></ul>';
                            }
                        }
                         $html .= '</article>';
                    }// End if().
                }// End if().
            } else {
				if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
						$sitepress->switch_lang( $cur_lang_code );
				}

                $category_headimage = get_term_meta( $parent->term_id, 'category_headimage', true );

                if ($category_headimage !='') {
                    $classn = 'no-cat-img';
                }else{
                    $classn = '';
                }

                    $html .= '<article class="category_list large-4 medium-4 small-6 xsmall-12 columns '.$classn.'">';
                    
                    if ($category_headimage !='') 
                    {

                        if($category_headimage ==''){
                                $imageurl = get_theme_mod('header_imagecontrol');
                        }else{
                                $imageurl = $category_headimage ;
                        }

                            $imgpath = $imageurl;
                            if($imgpath !=''){
                                 $html .= '<div class="catimg" style="background-image: url('.$imgpath.')""></div>';
                            }else{
                                $html .= '<div class="catimg" style="background-image: url('.TEVOLUTION_PAGE_TEMPLATES_URL.'images/no-image-200x200.jpg)"></div>';
                            }
                    }
					else
					{
						$html .= '<div class="catimg" style="background-image: url('.TEVOLUTION_PAGE_TEMPLATES_URL.'images/no-image-200x200.jpg)"></div>';
					}
                    if ($parent && $taxonomies[0]) {
                        $parents = '<a href="' . get_term_link( $parent, $taxonomies[0] ) . '" title="' . esc_attr( $parent->name ) . '">' . apply_filters( 'list_cats', $parent->name, $parent ) . '</a><i class="fa fa-angle-right arrow-click"></i>';
                        $html .= '<h3>';

                        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                    if (is_plugin_active( 'Tevolution-CategoryIcon/tevolution-categoryicon.php' )) {
                        if (@$parent->category_icon == '') {
                            // Do_action('show_categoty_map_icon', $term_icon);
                            if (function_exists( 'show_category_icon' )) {
                                    $html .= tmpl_show_category_icon( $term_icon );
                            }
                        }
                    } else {
                            // Do_action('show_categoty_map_icon', $parent->term_icon);
                        if (function_exists( 'show_category_icon' )) {
                                    $html .= tmpl_show_category_icon( $parent->term_icon );
                        }
                    }

                   
                        $html .= $parents;
                        $html .= '</h3>';

                    if (@$featured_catlist_list != '') {
                        if (0 != $number_of_category) {
                            $html .= '<ul>';
                            $html .= $featured_catlist_list;
                            $html .= '<li class="view"> <a href="' . get_term_link( $parent, $taxonomies[0] ) . '">';
                            $html .= esc_html__( 'View all &raquo;', 'templatic' );
                            $html .= '</a> </li></ul>';
                        }
                    }
                }

                $html .= '</article>';
            }// End if().
        }// End foreach().
		$html .= '<script>
			jQuery(document).ready(function(){
				jQuery(".arrow-click").click(function(){
					jQuery(this).parent().parent().find("ul").toggle();
					jQuery(this).parent().toggleClass( "show" );
				});
			});
		</script>';
    } else {
        $html .= '<p>' . esc_html__( 'Invalid Category.', 'templatic' ) . '</p>';
    }// End if().
    $html .= '</section>';
	
    $result[] = $html;
    // $response = new WP_JSON_Response();
    // $response->set_data($result);
    return $result;
}
/**
 * This function return neighborhood post.
 *
 * @param array $filter                 Argument for neighborhood post Widget.
 */
function get_all_neighborhood_post_widget_widget( WP_REST_Request $request ) {
    
    $filter = $request->get_param( 'filter' );
    
    global $post, $post_number,$miles;
    
    $post = get_post( $filter['current_post'] );
    $post_number = $filter['post_number'];
    $miles = $filter['miles'];
    
    remove_all_filters('posts_where' );
    remove_all_filters('posts_join' );
    add_filter('posts_where', 'directory_nearby_filter' );
    if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
        add_filter('posts_where', 'wpml_listing_milewise_search_language' );
    }
    add_filter('posts_join', 'templ_search_map_where_filter' );


    $args = array(
        'post__not_in' => array( $filter['current_post'] ),
        'post_status' => 'publish',
        'post_type' => $filter['post_type'],
        'posts_per_page' => $filter['post_number'],
        'ignore_sticky_posts' => 1,
        'orderby' => 'rand'
        );
    if ( is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) ) {
        add_filter('posts_where', 'location_multicity_where' );
    }
    $wp_query_near = new WP_Query( $args );
    if ( is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) ) {
        remove_filter('posts_where', 'location_multicity_where' );
    }
    if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
        remove_filter('posts_where', 'wpml_listing_milewise_search_language' );
    }
    if ( function_exists('tmpl_single_page_default_custom_field' ) ) {
        define( 'DOING_AJAX', true );
        $varname = tmpl_single_page_default_custom_field( $filter['post_type'] );
    }
    
    $html = '';
    if ( $wp_query_near->have_posts( ) ) :
        $html .= '<ul class="nearby_distance">';
        while ( $wp_query_near->have_posts( ) ) {
            $wp_query_near->the_post();
            $html .= '<li class="nearby clearfix">';

            if (has_post_thumbnail( ) ) {
                $post_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'tevolution_thumbnail' );
                $post_images = $post_img[0];
            } else {
                $post_img = bdw_get_images_plugin(get_the_ID(), 'tevolution_thumbnail' );
                $post_images = $post_img[0]['file'];
            }
            $image = ( $post_images) ? $post_images : apply_filters('nearest_post_thumb_image',TEVOLUTION_DIRECTORY_URL . 'images/no-image.png' );
            
            $html .= '<div class="nearby_image"> 
                <a href="'. get_permalink( get_the_ID()) .'"> 
                    <img src="'. $image .'" alt="'. esc_attr(get_the_title( $post->post_id ) ) .'" title="'. esc_attr(get_the_title( $post->post_id ) ) .'" class="thumb" />
                </a> 
            </div>
            <div class="nearby_content">
                <h4><a href="'. get_the_permalink( $post->post_id ) .'">';
                    $html .= get_the_title($post->post_id);
                $html .= '</a></h4>';
                if ( $varname['address'] ) :
                    $html .= ' <p class="address">';
                        
                        $address = get_post_meta(get_the_ID(), 'address', true );
                        $html .=  $address;
                        
                    $html .= '</p>';
                endif;
            $html .= '</div>
            
            </li>';
        }
        $html .= '</ul>';
    else:
        $html .= esc_html__( 'Sorry! There are no nearby results found', 'templatic' );
    endif;
    remove_filter('posts_where', 'nearby_filter' );
    wp_reset_query();
    $result[] = $html;
    // $response = new WP_JSON_Response();
    // $response->set_data($result);
    return $result;
}
/**
 * This function return post.
 *
 * @param array $filter                 Argument for display post Widget.
 */
function get_all_display_post_widget_widget( WP_REST_Request $request ) {
    
    $filter = $request->get_param( 'filter' );
    
    $taxonomies = get_object_taxonomies( (object) array(
                                                    'post_type' => $filter['post_type'],
                                                    'public'    => true,
                                                    '_builtin'  => true,
                                                )
                );
    
    
    if ( $filter['post_type_taxonomy'] ) {
        $cat_id = $filter['post_type_taxonomy'];
    }

    if ( is_plugin_active( 'woocommerce/woocommerce.php' ) && 'product' == $filter['post_type'] ) {
        $taxonomies[0] = $taxonomies[1];
    }

    if ( $filter['post_type_taxonomy'] && 'post' != $filter['post_type'] ) {

        $taxonomy_fetch = get_term( $cat_id, $taxonomies[0] );
        if ( empty( $taxonomy_fetch ) ) {
            $taxonomy_fetch = get_term( $cat_id, $taxonomies[1] );
        }

        $featured_arg = array(
                            'post_type' => $filter['post_type'],
                            'showposts' => $filter['post_number'],
                            'orderby'   => $filter['orderby'],
                            'order'     => $filter['order'],
                            'post_status' => 'publish',
                            'tax_query' => array(
                                                array(
                                                    'taxonomy'  => $taxonomy_fetch->taxonomy,
                                                    'field'     => 'id',
                                                    'terms'     => array( $cat_id ),
                                                    'operator'  => 'IN',
                                                    ),
                                                ),
                            );
    } else if ( $filter['post_type_taxonomy'] && 'post' == $filter['post_type'] ) {

        $featured_arg = array(
                            'post_type'     => $filter['post_type'],
                            'showposts'     => $filter['post_number'],
                            'orderby'       => $filter['orderby'],
                            'order'         => $filter['order'],
                            'post_status'   => 'publish',
                            'tax_query'     => array(
                                                    array(
                                                        'taxonomy' => 'category',
                                                        'field'    => 'id',
                                                        'terms'    => array( $cat_id ),
                                                        'operator' => 'IN',
                                                        ),
                                                ),
                            );
    } else {
        if ( $cat_id ) {
            $cats = array( $cat_id );
        }

        $featured_arg = array(
                            'post_type'     => $filter['post_type'],
                            'post_status'   => 'publish',
                            'showposts'     => $filter['post_number'],
                            'orderby'       => $filter['orderby'],
                            'category__in'  => $cats,
                            'order'         => $filter['order'],
            );
    } // End if().

    remove_all_actions( 'posts_orderby' );

    if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) && function_exists( 'templatic_widget_wpml_filter' ) ) {
        if ( 'post' == $filter['post_type'] ) {
            remove_all_actions( 'posts_join' );
            add_filter( 'posts_join','tmpl_wpml_posts_joins' );
        } else {
            add_filter( 'posts_where','templatic_widget_wpml_filter' );
        }
    }
    $html = '';
    /* Add Do action for insert extra post where filter */
    do_action( 'post_listing_widget_before_post_where', $filter );
    $featured_posts = new WP_Query( $featured_arg );
    
    if ( ! empty( $filter['title'] ) && 0 != $featured_posts->found_posts ) {
        $html .= '<h3 class="widget-title">' . apply_filters( 'widget_title', $filter['title'] ) . '</h3>';
    }

    if ( function_exists( 'templatic_widget_wpml_filter' ) ) {
        remove_filter( 'posts_where','templatic_widget_wpml_filter' );
    }
    /* Add Do action for remove extra added post where filter */
    do_action( 'post_listing_widget_after_post_where', $filter );

    global $post;
    
    if ( $featured_posts->have_posts() ) :
        $html .= '<div class="listing_post_wrapper">';
             $postClass = get_post_class(); 
            while ( $featured_posts->have_posts() ) : $featured_posts->the_post();
                $html .= '<div class="post type-post status-publish format-standard has-post-thumbnail hentry category-blog "><div class="post-blog-image">';
                    
                    /*Show Avatar */
                    if ( ! empty( $filter['show_gravatar'] ) ) :
                        $html .= '<span class="' . esc_attr( $filter['gravatar_alignment'] ) . '">';
                        $html .= wp_kses_post( get_avatar( get_the_author_meta( 'ID' ), $filter['gravatar_size'] ) );
                        $html .= '</span>';
                    endif;
                    /* show post title*/
                    $html .= do_action( 'listing_post_before_image', $filter );
                    $a     = get_intermediate_image_sizes();
                    $width = '';
                    $height = '';
                    global $_wp_additional_image_sizes;
                    $_wp_additional_image_sizes_count = count( $_wp_additional_image_sizes );
                    for ( $i = 0; $i < $_wp_additional_image_sizes_count; $i++ ) {
                        $a = array_keys( $_wp_additional_image_sizes );
                        $a_count = count( $a );
                        for ( $k = 0; $k < $a_count; $k++ ) {
                            if ( $a[ $k ] == $filter['image_size'] ) {
                                $width  = $_wp_additional_image_sizes[ $a[ $k ] ]['width'];
                                $height = $_wp_additional_image_sizes[ $a[ $k ] ]['height'];
                            }
                        }
                    }
                    if ( ! $width ) {
                        $width = '250px';
                    }

                    if ( ! $height ) {
                        $height = '165px';
                    }
                    /* Display image */

                    if ( ! empty( $filter['show_image'] ) ) :

                        $post_image = '';
                        if ( has_post_thumbnail( $post->ID ) ) {
                            $img_arr = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $filter['image_size'] );
                            $post_image = $img_arr[0];
                            $style = '';
                        } elseif ( function_exists( 'bdw_get_images_plugin' ) ) {
                            $post_img = bdw_get_images_plugin( $post->ID,$filter['image_size'] );
                            $post_image = @$post_img[0]['file'];
                            $style = '';
                        }
                        if ( ! $post_image ) {
                            $theme_options = get_option( supreme_prefix() . '_theme_settings' );
                            $post_image = apply_filters( 'supreme_noimage-url', '//placehold.it/250x165/F6F6F6/333&text=No%20image' );

                            $style = apply_filters( 'supreme_noimage-size', 'width="50px" height="50px"' );

                        }
                        ?>

                        <?php if ( @$featured ) {
                                $html .= '<span class="featured_tag">' . esc_html__( 'Featured', 'templatic' ) . '</span>';
                        }
                        $html .= '<a href="'. esc_url( get_permalink() ).'" title="'. the_title_attribute( 'echo=0' ).'" rel="bookmark" class="featured-image-link"><img src="'. esc_url( $post_image ).'" width="'. intval( $width ).'" height="'. intval( $height ) .'" alt="'. the_title_attribute( 'echo=0' ) .'" '. wp_kses_post( $style ).'/></a>';
                        
                endif; 
            $html .= '</div>';
            
                do_action( 'listing_post_after_image', $filter );

                $more_text = $filter['more_text'];
            if ( function_exists( 'icl_register_string' ) ) {
                icl_register_string( 'templatic', $args['widget_id'] . 'more_text' . $more_text, $more_text );
                $more_text = icl_t( 'templatic', $args['widget_id'] . 'more_text' . $more_text, $more_text );
            }

                $html .=  "<div class='entry-header'>";
                do_action( $post->post_type . '_before_title', $filter );
            if ( ! empty( $filter['show_title'] ) ) :
                $html .= '<h2><a href="'.get_the_permalink().'" title="'.the_title_attribute( 'echo=0' ).'">'. the_title_attribute( 'echo=0' ).'</a></h2>';
            endif;
            if ( 'post' != $post->post_type ) {
                $html .= do_action( $post->post_type . '_after_title', $filter );
            }
            if ( ! empty( $filter['show_content'] ) ) :
                $html .= "<div class='post-summery'>";
                if ( 'excerpt' == $filter['show_content'] ) :
                        $html .= get_the_excerpt();
                    elseif ( 'content-limit' == $filter['show_content'] ) :
                        $html .= the_content_limit( (int) $filter['content_limit'], esc_html( $more_text ) );
                    else :
                        $html .= get_the_content( esc_html( $more_text ) );
                    endif;
                    $html .=  '</div>';
                endif;
                do_action( 'listng_post_after_content' );
                $html .=  '</div>';

                $html .=  '</div>';
            endwhile;
        wp_reset_query();
    $html .= '</div>';
    endif;
    $result[] = $html;
    // $response = new WP_JSON_Response();
    // $response->set_data($result);
    return $result;
}


/**
 * Return city details using city slug
 *
 * @param array $request            City Slug.
 */
function get_post_city_slug(WP_REST_Request $request)
{

    $slug = $request['slug'];

    if (empty( $slug )) {
        return new WP_Error( 'json_post_invalid_id', __( 'Invalid City slug.' ), array(
            'status' => 404,
        ) );
    }

    // Link headers (see RFC 5988)
    global $wpdb, $country_table, $zones_table, $multicity_table, $current_cityinfo, $wp_query;
    $country_table = $wpdb->prefix . 'countries';
    $zones_table = $wpdb->prefix . 'zones';
    $multicity_table = $wpdb->prefix . 'multicity';

    $cityinfo = $wpdb->get_results( $wpdb->prepare( "SELECT mc.*,mc.message as msg,c.country_name,c.country_flg,z.zone_name FROM $multicity_table mc,$zones_table z,$country_table c where c.country_id=mc.country_id AND z.zones_id=mc.zones_id AND  mc.city_slug =%s order by cityname ASC", $slug ) );

    // $response = new WP_JSON_Response();
    // $response->set_data($cityinfo);
    return $cityinfo;
}


/**
 * This function return filter posts whose city and post type pass as filter.
 *
 * @param array $request            City Slug and post type.
 */
function get_city_post_type_all(WP_REST_Request $request)
{

    return get_city_post_type( $request['slug'], $request['type'], null );
}

/* This function return filter posts whose city and post type pass as filter */
/**
 * This function return filter posts whose city and post type pass as filter.
 *
 * @param array $request            City Slug and post type.
 */
function get_city_post_type(WP_REST_Request $request)
{

    $slug = $request['slug'];
    $type = $request['type'];
    $number = $request['number'];

    if (! $number) {
        $number = -1;
    }

    global $wpdb;

    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
        if (empty( $slug ) || empty( $type )) {
            return new WP_Error( 'json_post_invalid_id', esc_html__( 'Invalid slug or post type.' ), array(
                'status' => 404,
            ) );
        }

        $city_id = get_city_id( $slug );
        if ('' == $city_id) {
            return new WP_Error( 'json_post_invalid_id', esc_html__( 'Invalid city slug.' ), array(
                'status' => 404,
            ) );
        }
    } else {
        $type = $request['type'];
    }

    if (empty( $number )) {
        $number = '-1';
    }

    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
        $args = array(
            'post_type' => $type,
            's' => sanitize_text_field( wp_unslash( $_REQUEST['s'] ) ),
            'posts_per_page' => $number,
            'meta_query' =>
            array(
                array(
                    'key' => 'post_city_id',
                    'value' => $city_id,
                    'compare' => 'RLIKE',
                ),
            ),
        );
    } else {
        $args = array(
            'post_type' => $type,
            'posts_per_page' => $number,
        );
    }

    $my_query = new WP_Query( $args );

    // $response = new WP_JSON_Response();
    $post_array = array();
    $custom_feild = array();
    $args = array(
        'post_type' => 'custom_fields',
    'posts_per_page' => -1,
    );

    $loop = new WP_Query( $args );

    $post_types = $loop->get_posts();

    foreach ($my_query->posts as $post) {
        $featured_img = get_the_post_thumbnail_url( $post->ID, 'thumbnail' );
        $post->featured_img = $featured_img;
        $querystr = 'SELECT guid FROM ' . $wpdb->prefix . 'posts WHERE post_parent =' . $post->ID . " AND post_type = 'attachment' ";
        $pageposts = $wpdb->get_results( $wpdb->prepare( $querystr ) );
        $post->image_gallery = array();
        $post->html_var = array();
        $post->html_var['package_id'] = get_post_meta( $post->ID, 'package_select', true );
        $post->html_var['package_name'] = get_the_title( get_post_meta( $post->ID, 'package_select', true ) );
        foreach ($pageposts as $key => $value) {
             $post->image_gallery[] = $value->guid;
        }
        foreach ($post_types as $post_type) {
            $html_var = get_post_meta( $post_type->ID, 'htmlvar_name', true );
            if (get_post_meta( $post->ID, $html_var, true ) != '') {
                $post->html_var[ $html_var ] = get_post_meta( $post->ID, $html_var, true );
            }
        }
    }
    return $my_query->posts;
}

/* This function return filter posts whose city, post type, taxonomt and category id pass as filter */
/**
 * This function return filter posts whose city, post type, taxonomt and category id pass as filter.
 *
 * @param array $request               City Slug and post type and other arguments in an array.
 */
function get_city_type_category(WP_REST_Request $request)
{

    $slug = $request['slug'];
    $type = $request['type'];
    $taxonomy = $request['taxonomy'];
    $category_id = $request['category_id'];

    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
        if (empty( $slug ) || empty( $type ) || empty( $taxonomy ) || empty( $category_id )) {
            return new WP_Error( 'json_post_invalid_id', esc_html__( 'Invalid slug or post type or taxonomy or categry id.' ), array(
                'status' => 404,
            ) );
        }

        $city_id = get_city_id( $slug );
        if ('' == $city_id) {
            return new WP_Error( 'json_post_invalid_id', esc_html__( 'Invalid city slug.' ), array(
                'status' => 404,
            ) );
        }
    } else {
        if (empty( $type ) || empty( $taxonomy ) || empty( $category_id )) {
                return new WP_Error( 'json_post_invalid_id', esc_html__( 'Invalid slug or post type or taxonomy or categry id.' ), array(
                    'status' => 404,
                ) );
        }
    }

    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
            $args = array(
                'post_type' => $type,
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => "$taxonomy",
                        'field' => 'id',
                        'terms' => $category_id,
                    ),
                ),
                'meta_query' =>
                array(
                    array(
                        'key' => 'post_city_id',
                        'value' => $city_id,
                        'compare' => '=',
                    ),
                ),
            );
    } else {
            $args = array(
                'post_type' => $type,
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => "$taxonomy",
                        'field' => 'id',
                        'terms' => $category_id,
                    ),
                ),
            );
    }// End if().

    $my_query = new WP_Query( $args );

    // $response = new WP_JSON_Response();
    global $htmlvar_name;
    $htmlvar_name = tmpl_get_category_list_customfields( $type );
    foreach ($my_query->posts as $post) {
        foreach ($htmlvar_name as $var) {
                $custom_feild[ $var[ htmlvar_name ] ] = get_post_meta( $post->ID, $var[ htmlvar_name ], true );
        }
            $post->custom_feild = $custom_feild;
            $thumbnail_id = get_post_thumbnail_id( $post->ID );
            $thumbnail = wp_get_attachment_image_src( $thumbnail_id );
            $data['featured_image'] = $thumbnail[0];
            $post_img = bdw_get_images_plugin( $data['ID'], 'large' );
            $post->gallery_images = $post_img;
    }
    return $my_query->posts;
}

/**
 * This function return filter posts whose city, post type, taxonomt and category slug pass as filter.
 *
 * @param array $request               City Slug and post type and other arguments in an array.
 */
function get_city_type_category_name(WP_REST_Request $request)
{
    $slug = $request['slug'];
    $type = $request['type'];
    $taxonomy = $request['taxonomy'];
    $category_name = $request['category_name'];

    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
        if (empty( $slug ) || empty( $type ) || empty( $taxonomy ) || empty( $category_name )) {
            return new WP_Error( 'json_post_invalid_id', esc_html__( 'Invalid slug or post type or taxonomy or categry id.' ), array(
                'status' => 404,
            ) );
        }

        $city_id = get_city_id( $slug );
        if ('' == $city_id) {
            return new WP_Error( 'json_post_invalid_id', esc_html__( 'Invalid city slug.' ), array(
                'status' => 404,
            ) );
        }
    } else {
        if (empty( $type ) || empty( $taxonomy ) || empty( $category_name )) {
            return new WP_Error( 'json_post_invalid_id', esc_html__( 'Invalid slug or post type or taxonomy or categry id.' ), array(
                'status' => 404,
            ) );
        }
    }
    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
        $args = array(
            'post_type' => $type,
            'posts_per_page' => -1,
            'tax_query' =>
            array(
                array(
                    'taxonomy' => "$taxonomy",
                    'field' => 'slug',
                    'terms' => "$category_name",
                ),
            ),
            'meta_query' =>
            array(
                array(
                    'key' => 'post_city_id',
                    'value' => $city_id,
                    'compare' => '=',
                ),
            ),
        );
    } else {
        $args = array(
            'post_type' => $type,
            'posts_per_page' => -1,
            'tax_query' =>
            array(
                array(
                    'taxonomy' => "$taxonomy",
                    'field' => 'slug',
                    'terms' => "$category_name",
                ),
            ),
        );
    }// End if().

    $my_query = new WP_Query( $args );
     global $htmlvar_name;
    $htmlvar_name = tmpl_get_category_list_customfields( $type );
    foreach ($my_query->posts as $post) {
        foreach ($htmlvar_name as $var) {
            if ($var[ htmlvar_name ] != 'post_title') {
                $custom_feild[ $var[ htmlvar_name ] ] = get_post_meta( $post->ID, $var[ htmlvar_name ], true );
            }
        }
        $post->custom_feild = $custom_feild;
        $thumbnail_id = get_post_thumbnail_id( $post->ID );
        $thumbnail = wp_get_attachment_image_src( $thumbnail_id );
        $data['featured_image'] = $thumbnail[0];
        $post_img = bdw_get_images_plugin( $data['ID'], 'large' );
        $post->gallery_images = $post_img;
    }
    return $my_query->posts;
}

/**
 * This function return filter posts whose city, post type, taxonomt and tag slug pass as filter.
 *
 * @param array $request               City Slug and post type and other arguments in an array.
 */
function get_city_type_tag_name(WP_REST_Request $request)
{

    $slug = $request['slug'];
    $type = $request['type'];
    $taxonomy = $request['taxonomy'];
    $tag_name = $request['tag_name'];
    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
        if (empty( $slug ) || empty( $type ) || empty( $taxonomy ) || empty( $tag_name )) {
            return new WP_Error( 'json_post_invalid_id', __( 'Invalid slug or post type or taxonomy or categry id.' ), array(
                'status' => 404,
            ) );
        }

        $city_id = get_city_id( $slug );
        if ('' == $city_id) {
            return new WP_Error( 'json_post_invalid_id', __( 'Invalid city slug.' ), array(
                'status' => 404,
            ) );
        }
    } else {
        if (empty( $type ) || empty( $taxonomy ) || empty( $tag_name )) {
            return new WP_Error( 'json_post_invalid_id', __( 'Invalid slug or post type or taxonomy or categry id.' ), array(
                'status' => 404,
            ) );
        }
    }

    if (is_plugin_active( 'Tevolution-LocationManager/location-manager.php' )) {
        $args = array(
            'post_type' => $type,
            'meta_query' =>
            array(
                array(
                    'key' => 'post_city_id',
                    'value' => $city_id,
                    'compare' => '=',
                ),
            ),
            'tax_query' =>
            array(
                array(
                    'taxonomy' => "$taxonomy",
                    'field' => 'slug',
                    'terms' => "$tag_name",
                ),
            ),
        );
    } else {
            $args = array(
                'post_type' => $type,
                'tax_query' =>
                array(
                    array(
                        'taxonomy' => "$taxonomy",
                        'field' => 'slug',
                        'terms' => "$tag_name",
                    ),
                ),
            );
    }

    $my_query = new WP_Query( $args );
    global $htmlvar_name;
    $htmlvar_name = tmpl_get_category_list_customfields( $type );
    foreach ($my_query->posts as $post) {
        foreach ($htmlvar_name as $var) {
            $custom_feild[ $var[ htmlvar_name ] ] = get_post_meta( $post->ID, $var[ htmlvar_name ], true );
        }
        $post->custom_feild = $custom_feild;
        $thumbnail_id = get_post_thumbnail_id( $post->ID );
        $thumbnail = wp_get_attachment_image_src( $thumbnail_id );
        $data['featured_image'] = $thumbnail[0];
        $post_img = bdw_get_images_plugin( $data['ID'], 'large' );
        $post->gallery_images = $post_img;
    }
    return $my_query->posts;
}
/**
 * Return city id from city slug.
 *
 * @param string $multi_city    Contain city slug.
 */
function get_city_id($multi_city)
{

    global $wpdb, $multicity_table;
    $multicity_table = $wpdb->prefix . 'multicity';
    $sql = "SELECT * FROM $multicity_table where city_slug='%s'";
    $default_city = $wpdb->get_results( $wpdb->prepare( $sql, $multi_city ) );
    $default_city_id = $default_city[0]->city_id;
    return $default_city_id;
}
