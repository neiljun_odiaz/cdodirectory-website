<?php

/**

 * Function related to claim any post.

 *

 * @package Wordpress

 */



if ( ! defined( 'ABSPATH' ) ) {

	exit;

}

/**

 * This function will add a metabox for latest claims listing in word press dashboard

 */

function add_claim_dashboard_metabox() {

	global $wp_meta_boxes, $current_user;

	if ( is_super_admin( $current_user->ID ) ) {

		wp_add_dashboard_widget( 'claim_dashboard_metabox',OWNERSHIP_CLAIMS, 'fetch_claims' );

		@$wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'];

	}

}



/**

 * This function fetches claims in a metabox displaying on wordpress dashboard

 */

function fetch_claims() {

	global $wpdb; ?>

	<script type="text/javascript" async >

		/* <![CDATA[ */

		function confirmSubmit(str ) {

			var answer = confirm("<?php echo wp_kses_post( DELETE_CONFIRM_ALERT ); ?>");

			if ( answer) {

				window.location = "<?php echo esc_url( site_url() ); ?>/wp-admin/index.php?poid="+str;

				alert( '<?php echo wp_kses_post( ENTRY_DELETED ); ?>' );

			}

		}

		function claimer_showdetail(str)

		{

			if ( document.getElementById( 'comments_'+str) .style.display == 'block' )	{

				document.getElementById( 'comments_'+str) .style.display = 'none';

			} else {

				document.getElementById( 'comments_'+str) .style.display = '';

			}

		}

		/* ]]> */

	</script>

	<!--this style added for hide loader in owner claim at dashboard-->

	<style type="text/css">

		body.index-php #TB_window{background: #fcfcfc !important;}

	</style>

	<?php /* DISPLAY CLAIM DATA IN TABLE */

	echo "<table class='widefat'>

	<thead>

		<tr>

			<th style='width:30%;'>" . __( 'Claim On', 'templatic' ) . "</th>

			<th style='width:30%;'>" . CLAIMER_TEXT . '</th>

            <th>' . STATUS . '</th>

            <th>' . ACTION_TEXT . '</th>

        </tr></thead>';

		$claim_post_ids = $wpdb->get_col( "SELECT ID FROM $wpdb->posts WHERE post_type = 'claim' ORDER BY post_date DESC LIMIT 0,7" );

	if ( count( $claim_post_ids ) != 0 ) {

			$counter = 0;

		foreach ( $claim_post_ids as $claim_post_id ) :

				$data = get_post_meta( $claim_post_id, 'post_claim_data', true );

			/* FETCH CLAIM DATA */

			$post_id = $data['post_id'];

			$auth_data = get_userdata( $data['author_id'] );

			$post_title = $data['post_title'];

			$claimer_name = $data['claimer_name'];

			$name = str_word_count( $claimer_name,1 );

			$claimer_contact = $data['claimer_contact'];

			$claimer_email = $data['claimer_email'];

			$author_id = $data['author_id'];

			$status = $data['claim_status'];

			$msg = $data['claim_msg'];

			$udata = get_userdata( $author_id );

			?>

			<tr>

				<td>

					<?php echo esc_attr( $claim_post_id );?>&nbsp;<a href="<?php echo esc_url( site_url() ) . '/wp-admin/post.php?post=' . esc_attr( $post_id ) . '&action=edit';?>" title="<?php echo wp_kses_post( VIEW_CLAIM ); ?>"><?php esc_html__( 'By', 'templatic-admin' ); ?><?php echo esc_attr( $post_title )?></a>

				</td>

				<td><?php echo esc_attr( $claimer_name );?>: <?php echo esc_attr( $claimer_email );?></td>

				<?php if ( 'approved' == $status && get_post_meta( $post_id, 'is_verified', true ) == 1 ) :?>

					<td id="verified"><?php echo wp_kses_post( YES_VERIFIED ); ?></td>

				<?php elseif ( 'declined' == $status ) : ?>

					<td id="declined"><?php echo wp_kses_post( DECLINED ); ?></td>

				<?php else : ?>

					<td id="unapproved"><?php echo wp_kses_post( PENDING ); ?></td>

				<?php endif;?>

				<td>

					<a href="#TB_inline?width=600&height=600&inlineId=claimed_win_<?php echo esc_attr( $claim_post_id );?>" id="claimed_w_<?php echo esc_attr( $claim_post_id );?>" class="thickbox" title="<?php echo esc_html__( 'View claim details', 'templatic-admin' ); ?>"><i class="fas fa-info"></i></a>&nbsp;&nbsp;

					<?php if ( 'approved' == $status && get_post_meta( $post_id, 'is_verified', true ) == 1 ) { ?>

					<a href="<?php echo esc_url( site_url() ) . '/wp-admin/post.php?post=' . esc_attr( $post_id ) . '&action=edit&decline=yes&clid=' . esc_attr( $claim_post_id );?>" title="<?php echo wp_kses_post( DECLINE_CLAIM ); ?>"><i class="fas fa-times"></i></a>&nbsp;&nbsp;

					<?php } else { ?>

					<a href="<?php echo esc_url( site_url() ) . '/wp-admin/post.php?post=' . esc_attr( $post_id ) . '&action=edit&verified=yes&clid=' . esc_attr( $claim_post_id ) . '&user=' . esc_attr( $claimer_name )?>" title="<?php echo wp_kses_post( VERIFY_CLAIM ); ?>"><i class="fas fa-check-circle"></i></a>&nbsp;&nbsp;

					<?php } ?>

					<a href="javascript:void(0);" onclick="return confirmSubmit( <?php echo esc_attr( $claim_post_id ); ?> );" title="<?php echo wp_kses_post( DELETE_CLAIM ); ?>"><i class="fas fa-minus-circle"></i></a>

				</td>

			</tr>



			<div id="claimed_win_<?php echo esc_attr( $claim_post_id ); ?>" class="clm_cls" style="display:none;width:400px; height:400px;">



				<h2><?php echo esc_html__( 'Author Details', 'templatic-admin' ); ?></h2>

				<p class="tev_description">

					<?php echo '<strong>' . esc_html__( 'Name', 'templatic-admin' ) . '</strong>';

					echo ': ' . esc_attr( $auth_data->display_name); ?>

				</p>

				<p class="tev_description">

					<?php echo '<strong>' . esc_html__( 'Email', 'templatic-admin' ) . '</strong>';

					echo ': ' . esc_attr( $auth_data->user_email);

					?>

				</p>





				<h2><?php echo esc_html__( 'Claimer Details', 'templatic-admin' ); ?></h2>

				<p class="tev_description">

					<?php echo '<strong>' . esc_html__( 'Name', 'templatic-admin' ) . '</strong>';

					echo ': ' . esc_attr( $data['claimer_name'] );?>

				</p>

				<p class="tev_description">

					<?php echo '<strong>' . esc_html__( 'Email', 'templatic-admin' ) . '</strong>';

					echo ': ' . esc_attr( $data['claimer_email'] );?>

				</p>

				<p class="tev_description">

					<?php echo '<strong>' . esc_html__( 'Conatct No. ', 'templatic-admin' ) . '</strong>';

					echo ': ' . esc_attr( $data['claimer_contact'] );?>

				</p>

				<p class="tev_description">

					<?php echo '<strong>' . esc_html__( 'Message', 'templatic-admin' ) . '</strong>';

					echo ': ' . esc_attr( $data['claim_msg'] );?>

				</p>

				<?php do_action( 'tmpl_extra_claiming_details', $data ); ?>

			</div>



			<?php

			$c = $counter ++;

			endforeach;

	} else {

			echo '<tr><td>' . NO_CLAIM_REQUEST . '</td></tr>';

	} // End if().

		echo '</table>';

}



	/* deleting the claim on click of delete button of dashboard metabox */

if ( isset( $_REQUEST['poid'] ) && '' != sanitize_text_field( wp_unslash( @$_REQUEST['poid'] ) ) ) {

		$vclid = intval( $_REQUEST['poid'] );

		wp_delete_post( $vclid,true );

}

	/* eof - fetch claims in dashboard metabox */



	/**

	 * This function will add a metabox on add/edit page of every post.

	 */

function add_claim_metabox_posts() {

		global $post, $wpdb, $post_id;

	if ( isset( $_REQUEST['post'] ) && '' != $_REQUEST['post'] ) {

			$post_id = intval( $_REQUEST['post'] );

	} else {

			$post_id = intval( @$post->ID );

	}

	if ( '' != @$post_id ) {

			$tmpdata = get_option( 'templatic_settings' );

			$post_type = $tmpdata['claim_post_type_value'];

		if ( $post_type ) {

			foreach ( $post_type as $type ) :

				if ( '' != $post_id ) :

					$post_content = $wpdb->get_row( "SELECT post_content FROM $wpdb->posts WHERE $wpdb->posts.post_content = '" . $post_id . "' and $wpdb->posts.post_type = 'claim'" );

					add_meta_box( 'claim_post', 'Claim post', 'fetch_meta_options', $type, 'side', 'high' );

					endif;

					endforeach;

		}

	}

}



		/**

		 * While approve the claim for recurring events insert the recurring post as per start date and end date.

		 *

		 * @param int $post_id post id.

		 */

function add_verified_user_recurring( $post_id ) {

			$args = array(

				'post_type' => 'event',

				'posts_per_page' => -1,

				'post_status' => 'recurring',

				'post_parent' => $post_id,

				'meta_query' => array(

					'relation' => 'AND',

					array(

						'key'   => '_event_id',

						'value'  => $post_id,

						'compare' => '=',

						'type'  => 'text',

						),

					),

				);

			$post_query = null;

			$post_query = new WP_Query( $args );

	if ( isset( $_REQUEST['clid'] ) ) {

		$post_title = get_the_title( sanitize_text_field( wp_unslash( $_REQUEST['clid'] ) ) );

	}

			$post_content = $post_id;

			$post_author = 1;

	if ( $post_query ) {

		while ( $post_query->have_posts() ) : $post_query->the_post();

				global $post;

				$claim_post_type = array(

					'post_title'  => $post_title,

					'post_content' => $post->ID,

					'post_status' => 'publish',

					'post_author' => 1,

					'post_type'  => 'claim',

					'post_excerpt' => 'approved',

					);

				$las_rec_post_id = wp_insert_post( $claim_post_type ); /* INSERT QUERY */

				add_post_meta( $las_rec_post_id, 'is_verified', 1 );

				/* INSERTING CLAIM INFORMATION IN POST META TABLE */

			if ( isset( $_REQUEST['clid'] ) ) {

				$data = get_post_meta( intval( $_REQUEST['clid'] ) , 'post_claim_data', true ); /* FETCH CLAIM ID */

			}

				$data['post_id'] = $las_rec_post_id;

				add_post_meta( $las_rec_post_id, 'post_claim_data', $data );

				endwhile;

				wp_reset_query();

	}

}

		/**

		 * This function will fetch the claim data in post add/edit page.

		 */

function fetch_meta_options() {

			global $wpdb;

			$claim_status = '';

			/* VERIFY THE USER */

	if ( (isset( $_REQUEST['verified'] ) && 'yes' == sanitize_text_field( wp_unslash( $_REQUEST['verified'] ) ) ) && (isset( $_REQUEST['user'] ) && '' != sanitize_text_field( wp_unslash( $_REQUEST['user'] ) ) ) && (isset( $_REQUEST['sclid'] ) || isset( $_REQUEST['clid'] ) )) {

				$clid = intval( $_REQUEST['clid'] );

				global $current_user, $post_id;

				$sclid = intval( $_REQUEST['sclid'] );

				/* if self verify the claim */

				if ( isset( $sclid ) && '' != $sclid ) {

					$clid1 = $post_id;

					global $post;

					$title = get_the_title( @$post_id );

					$claim_post_id = $post_id; /* claimed post id */

					$claim_post_type = array(

						'post_title' => __( 'Claim for - ', ADMINDOMAN ) . $title . '',

						'post_content' => '' . $claim_post_id . '',

						'post_excerpt' => 'approved',

						'post_status' => 'publish',

						'post_author' => 1,

						'post_type' => 'claim',

						);

					$claim_id = wp_insert_post( $claim_post_type ); /* INSERT QUERY */

					$claim_post = array(

						'post_id' => $post_id,

						'request_uri' => '',

						'link_url' => $link_url,

						'claimer_id' => $current_user->ID,

						'author_id' => $current_user->ID,

						'post_title' => $title,

						'claimer_name' => $current_user->display_name,

						'claimer_email' => $current_user->user_email,

						'claimer_contact' => '',

						'claim_msg' => '',

						'claim_status' => 'approved',

						);

					update_post_meta( $claim_id, 'post_claim_data', $claim_post ); /* UPDATING THE WHOLE CLAIM DATA ARRAY */

					$clid = $claim_id;

					add_post_meta( $post_id, 'is_verified', 1 );

					update_post_meta( $post_id, 'is_verified', 1 );

		}

				/* End verify the claim */

				sanitize_text_field( wp_unslash( $_REQUEST['user'] ) );

				/* update claim status when the admin verifies the author */

				$data = get_post_meta( $clid, 'post_claim_data', true );

			

				$post_id = $data['post_id'];

				$request_uri = $data['request_uri'];

				$link_url = $data['link_url'];

				$claimer_id = $data['claimer_id'];

				$post_title = $data['post_title'];

				$claimer_name = $data['claimer_name'];

				$claimer_email = $data['claimer_email'];

				$claimer_contact = $data['claimer_contact'];

				$author_id = $data['author_id'];

				$claim_status = $data['claim_status'];

				$claim_msg = $data['claim_msg'];

				if ( '' == @$post_id ) {

							$post_id = $post->ID;

				}

				$claim_post = array(

					'post_id' => $post_id,

					'request_uri' => $request_uri,

					'link_url' => $link_url,

					'claimer_id' => $claimer_id,

					'author_id' => $author_id,

					'post_title' => $post_title,

					'claimer_name' => $claimer_name,

					'claimer_email' => $claimer_email,

					'claimer_contact' => $claimer_contact,

					'claim_msg' => $claim_msg,

					'claim_status' => 'approved',

					);

				update_post_meta( $clid, 'post_claim_data', $claim_post ); /* UPDATING THE WHOLE CLAIM DATA ARRAY */

				$event_type = get_post_meta( $post_id, 'event_type', true );

				if ( trim( strtolower( $event_type ) ) == trim( strtolower( 'Recurring Event' ) ) ) {

							add_verified_user_recurring( $post_id );

				}

				/* post does not verify from claim listing */

				update_post_meta( $post_id, 'is_verified', 1 );

				$wpdb->update( $wpdb->posts, array(

					'post_excerpt' => 'approved',

					),

					array(

					'ID' => $clid,

					)

				);



		if ( get_option( 'users_can_register' ) ) :

			if ( ( '' == $claimer_id || '0' == $claimer_id ) && sanitize_text_field( wp_unslash( $_REQUEST['user'] ) ) ) {

						$post_aurhot_id = add_verified_user( $clid ); /* call a function to add verified user */

			} else {

				if ( isset( $_REQUEST['verify'] ) && 'self' == $_REQUEST['verify'] ) {

							add_verified_user( $post_id ); /* call a function to add verified user */

							$post_aurhot_id = $current_user->ID;

				} else {

							$post_aurhot_id = add_verified_user( $clid ); /* call a function to add verified user */

				}

			} else :

					$post_aurhot_id = add_verified_user( $clid ); /* call a function to add verified user */

					endif;

					/* select author name in drop down which have been selected. */



					?>

					<script type="text/javascript" async>

						jQuery(window) .load(function(e ) {

							var author = "<?php echo esc_attr( $post_aurhot_id ); ?>";

							jQuery("#post_author_override option[value='"+author+"']") .attr( 'selected', 'selected' );

						});

					</script>

					<?php



	} elseif ( (isset( $_REQUEST['verified'] ) && 'no' == $_REQUEST['verified'] ) && (isset( $_REQUEST['clid'] ) &&  '' != $_REQUEST['clid'] ) && isset( $_REQUEST['clid'] ) ) {

		global $post;

		$clid = intval( $_REQUEST['clid'] );

		$data = get_post_meta( $clid, 'post_claim_data', true );

		$post_id = $data['post_id'];

		delete_post_meta( $clid, 'post_claim_data' );

		wp_delete_post( $clid );

		if ( $post_id == '' ) {

			$post_id = $post->ID;

		}

		delete_post_meta( $post_id, 'is_verified', 1 );

	} elseif ( (isset( $_REQUEST['decline'] ) && 'yes' == $_REQUEST['decline'] ) && (isset( $_REQUEST['clid'] ) && '' != $_REQUEST['clid'] ) ) {

					global $current_user;

					$clid = intval( $_REQUEST['clid'] );

					/* update claim status when the admin declines the author */

					$data = get_post_meta( $clid, 'post_claim_data', true );

					$post_id = $data['post_id'];

					$request_uri = $data['request_uri'];

					$link_url = $data['link_url'];

					$claimer_id = $data['claimer_id'];

					$post_title = $data['post_title'];

					$claimer_name = $data['claimer_name'];

					$claimer_email = $data['claimer_email'];

					$claimer_contact = $data['claimer_contact'];

					$author_id = $data['author_id'];

					$claim_status = $data['claim_status'];

					$claim_msg = $data['claim_msg'];

					$post = array(

						'post_id' => $post_id,

						'request_uri' => $request_uri,

						'link_url' => $link_url,

						'claimer_id' => $claimer_id,

						'author_id' => $author_id,

						'post_title' => $post_title,

						'claimer_name' => $claimer_name,

						'claimer_email' => $claimer_email,

						'claimer_contact' => $claimer_contact,

						'claim_msg' => $claim_msg,

						'claim_status' => 'declined',

						);

					update_post_meta( $clid, 'post_claim_data', $post ); /* UPDATING THE WHOLE CLAIM DATA ARRAY */

		if ( isset( $sclid ) && '' != $sclid ) {

						update_post_meta( $post_id, 'is_verified', 1 );

		} else {

						update_post_meta( $post_id, 'is_verified', 0 );

		}

					$wpdb->update( $wpdb->posts, array(

						'post_excerpt' => 'declined',

						), array(

						'ID' => $clid,

					) );

	} // End if().

				/* PRINT THE DATA IN METABOX */

				$data = get_post_meta( @$clid, 'post_claim_data', true );

	if ( 'approved' == $data['claim_status'] && '1' == get_post_meta( $data['post_id'], 'is_verified', true ) ) {

					global $post;

					$post_id = $data['post_id'];

					echo '<p>' . esc_html__( '1 user has claimed for this post. ', 'templatic-admin' ) . '</p>';

		if ( isset( $_REQUEST['post'] ) ) {

		?>

<a href="<?php echo esc_url( site_url() . '/wp-admin/post.php?post=' . sanitize_text_field( wp_unslash( $_REQUEST['post'] ) ) . '&action=edit&verified=no&clid=' . $clid );?>" title="<?php echo wp_kses_post( REMOVE_CLAIM_REQUEST ); ?>"><?php echo wp_kses_post( REMOVE_CLAIM_REQUEST ); ?></a>

		<?php

		}

	} else {

					global $current_user, $post;

		if ( isset( $_REQUEST['clid'] ) ) {

			$id = sanitize_text_field( wp_unslash( $_REQUEST['clid'] ) );

		}

		if ( isset( $_REQUEST['post'] ) ) {

			$post_claim_id = $wpdb->get_col( $wpdb->prepare( "SELECT ID from $wpdb->posts WHERE (post_content = %d OR post_content = %d) AND post_status = 'publish' AND (post_excerpt = 'approved' OR post_excerpt = '' ) and post_type='claim'", sanitize_text_field( wp_unslash( $_REQUEST['post'] ) ), $post->ID ) ); /* FETCH TOTAL NUMBER OF CLAIMS FOR A POST */

		}

					$data = get_post_meta( $id, 'post_claim_data', true );

					$post_id = $data['post_id'];

		if ( '' == count( $post_claim_id ) ) {

			$post_id = $_REQUEST['post'];

			echo '<p>' . wp_kses_post( NO_CLAIM ) . '</p>'; ?>

						<a href="<?php echo esc_url( site_url() . '/wp-admin/post.php?post=' . $post_id . '&verify=self&action=edit&verified=yes&sclid=1&user=' . $current_user->ID );?>" title="<?php echo esc_html__( 'Set as verify', 'templatic-admin' ); ?>" target="_self" class="verify_this">

							<strong><?php echo esc_html__( 'Self Verify', 'templatic-admin' ); ?></strong>

						</a>

						<?php

						do_action( 'tmpl_extra_claim_details' );

		} else {

						/* condition to display the count of claims in metabox */

			if ( count( $post_claim_id ) == 1 ) :

							echo '<p>' . intval( count( $post_claim_id ) ) . ' ' . esc_html__( 'user has claimed for this post. ', 'templatic' ) . '</p>';

						else :

							echo '<p>' . intval( count( $post_claim_id ) ) . ' ' . esc_html__( 'users have claimed for this post. ', 'templatic' ) . '</p>';

						endif;

						?>



						<?php

						foreach ( $post_claim_id as $key => $val ) :

							$data = get_post_meta( $val, 'post_claim_data', true );

							if ( 'pending' == $data['claim_status'] ) :

								$user_data = get_userdata( $data['claimer_id'] );

								$claim_user = get_post_meta( $val, 'post_claim_data', true );

								$name = str_word_count( $claim_user['claimer_name'],1 );?>

						<ul>

							<li>

								<a id="verify_claim" href="<?php echo esc_url( site_url() . '/wp-admin/post.php?post=' . $post->ID . '&action=edit&verified=yes&clid=' . $val . '&user=' . $name[0] );?>" title="<?php echo wp_kses_post( VERIFY_CLAIM ); ?>" class="verify_this">

									<strong><?php echo wp_kses_post( VERIFY_CLAIM ); ?></strong>

								</a> /

								<a id="decline_claim" href="<?php echo esc_url( site_url() . '/wp-admin/post.php?post=' . $post->ID . '&action=edit&decline=yes&clid=' . $val . '&user=' . $name[0] );?>" title="<?php echo wp_kses_post( DECLINE_CLAIM ); ?>" class="verify_this">

									<strong><?php echo wp_kses_post( DECLINE_CLAIM ); ?></strong>

								</a>

								<?php $current_link = get_author_posts_url( @$user_data->ID );

								if ( '' != $user_data && '0' != $data['claimer_id'] ) {?>

								<a href="<?php echo esc_url( $current_link ); ?>"><?php echo wp_kses_post( $user_data->display_name ); ?></a>

								<?php } else { echo wp_kses_post( $name[0] ); }?>

							</li>

						</ul>

						<?php

						do_action( 'tmpl_extra_claim_details' );

						else :

							?>

						<a href="<?php echo esc_url( site_url() . '/wp-admin/post.php?post=' . sanitize_text_field( wp_unslash( $_REQUEST['post'] ) ) . '&action=edit&verified=no&clid=' . $val );?>" title="<?php echo wp_kses_post( REMOVE_CLAIM_REQUEST ); ?>"><?php echo wp_kses_post( REMOVE_CLAIM_REQUEST ); ?></a>

						<?php

						endif; /* Finish claim status pending if condition */

						endforeach; /* finish post claim id foreach */

		}// End if().

	}// End if().

}

			/**

			 * This funtion will add a user who has been verified for the claimed post.

			 *

			 * @param int $clid  claim id.

			 */

function add_verified_user( $clid ) {

				global $wpdb, $post;

	if ( isset( $_REQUEST['verify'] ) && 'self' == $_REQUEST['verify'] ) {

					global $current_user;

					get_post_meta( $clid, 'is_verified', true );

					$user_name = $current_user->user_name;

					$name = $user_name;

					$user_email = $current_user->user_email;

					$get_user_data_by_id = '';

					$user_has_flag = 0;

		if ( isset( $_REQUEST['post'] ) ) {

			$post_id = intval( @$_REQUEST['post'] );

		}

					$data['claim_status'] = 'approved';

	} else {

					$data = get_post_meta( $clid, 'post_claim_data', true );

					get_post_meta( $clid, 'is_verified', true );

					$user_name = $data['claimer_name'];

					$name = $user_name;

					$user_email = $data['claimer_email'];

					$get_user_data_by_id = '';

					$user_has_flag = 0;

					$post_id = 0;

	}

				$user_pass = '';

	if ( isset( $_REQUEST['post'] ) ) {

		$post_id = intval( @$_REQUEST['post'] );

	}

					$get_post_data = get_post( $post_id );



	if ( ! empty( $get_post_data ) ) {

					$post_title = $get_post_data->post_title;

	}

	if ( '' != $user_name ) {

					$get_current_user = get_user_by( 'login', $user_name );

		if ( @$get_current_user->ID > 0 ) {

						$get_user_data_by_id = @$get_current_user->ID;

						$user_has_flag = 1;

		} else {

						$user_pass = wp_generate_password( 12,false );

						$get_user_data_by_id = wp_create_user( $user_name, $user_pass, $user_email );

						/**

						 * Uncomment this for checking spam users

						 */

			// $data = array( 'username' => $get_user_data_by_id. '=' . $user_name, 'from' => 'claim' );

			// $wpdb->insert( 'wp_check_user', $data);

						$user_has_flag = 0;

		}

	}

	if ( $get_user_data_by_id ) {

					$user_info = get_userdata( $get_user_data_by_id );

					$user_login = $user_info->user_login;

					/* $user_pass = $user_info->user_pass; */

					$post_url_link = '<a href="' . esc_url( sanitize_text_field( wp_unslash( $_REQUEST['link_url1'] ) ) ) . '">' . $post_title . '</a>';

					$email_subject = esc_html__( 'Claim verified for - ', 'templatic' ) . $post_title;

					$fromEmail = get_option( 'admin_email' );

					$fromEmailName = stripslashes( get_option( 'blogname' ) );

					$msg = '<p>' . esc_html__( 'Dear', 'templatic' ) . $user_login . ', </p>';

					$msg .= '<p>' . esc_html__( 'The Claim for the', 'templatic' ) . ' <a href="' . get_permalink( sanitize_text_field( wp_unslash( $_REQUEST['post'] ) ) ) . '">' . $post_title . '</a>' . esc_html__( ' has been verified. ', 'templatic' ) . '</p>';

		if ( 0 == $user_has_flag ) {

						$msg .= '<p>' . esc_html__( 'You can login with the following credentials :', 'templatic' ) . '</p>';

						$msg .= '<p>' . esc_html__( 'Username:', 'templatic' ) . ' [#user_login#]</p>';

						$msg .= '<p>' . esc_html__( 'Password:', 'templatic' ) . ' [#user_password#]</p>';

						$msg .= '<p>' . esc_html__( "You can login from [#site_login_url#] or copy this link and paste it to your browser's address bar: ", 'templatic' ) . '[#site_login_url_link#]</p>';

		}

					$msg .= '<p>' . esc_html__( 'Thanks, ', 'templatic' ) . '<br/> [#site_name#] </p>';

					$client_message = $msg;

					$subject = $email_subject;

					$yourname_link = $yourname;

					$store_login = '';

					$store_login_link = '';

		if ( function_exists( 'get_tevolution_login_permalink' ) ) {

						$store_login = '<a href="' . get_tevolution_login_permalink() . '">' . __( 'Click Login', 'templatic' ) . '</a>';

						$store_login_link = get_tevolution_login_permalink();

		}

					$search_array = array( '[#user_name#]', '[#user_login#]', '[#user_password#]', '[#site_name#]', '[#site_login_url#]', '[#site_login_url_link#]' );

					$replace_array = array( $user_login, $user_login, $user_pass, $fromEmailName, $store_login, $store_login_link );

					$client_message = str_replace( $search_array, $replace_array, $client_message );

					/* CALL A MAIL FUNCTION */

					templ_send_email( $fromEmail, $fromEmailName, $user_email, $user_login, $subject, $client_message, $extra = '' );

	}

				/* UPDATING THE CLAIM DATA */

				$user_info = get_userdata( $get_user_data_by_id );

				$user_id = $user_info->ID;

				$data = get_post_meta( $clid, 'post_claim_data', true );

				$post_id = $data['post_id'];

				$request_uri = $data['request_uri'];

				$link_url = $data['link_url'];

				$claimer_id = $data['claimer_id'];

				$post_title = $data['post_title'];

				$claimer_name = $data['claimer_name'];

				$claimer_email = $data['claimer_email'];

				$claimer_contact = $data['claimer_contact'];

				$author_id = $data['post_author_id'];

				$claim_status = $data['claim_status'];

				$claim_msg = $data['claim_msg'];

				/* UPDATING THE WHOLE CLAIM DATA ARRAY */

				$post_data = array(

					'post_id' => $post_id,

					'request_uri' => $request_uri,

					'link_url' => $link_url,

					'claimer_id' => $user_id,

					'author_id' => $user_id,

					'post_title' => $post_title,

					'claimer_name' => $claimer_name,

					'claimer_email' => $claimer_email,

					'claimer_contact' => $claimer_contact,

					'claim_msg' => $claim_msg,

					'claim_status' => $claim_status,

					);

				update_post_meta( $post->ID, 'post_claim_data', $post_data );

				/* UPDATING THE POST TABLE */

				$wpdb->get_results( "Update $wpdb->posts set post_author ='" . $user_id . "' where ID = '" . $post_id . "' and post_status = 'publish'" );

				return $get_user_data_by_id;

}



/**

 * This function posts the data of the claim form, creates a post and saves data in postmeta.

 *

 * @param array $post_details 					Post array.

 */

function insert_claim_ownership_data( $post_details ) {

	global $wpdb, $post;

	if ( isset( $_POST['claimer_name'] ) ) {

		/* CODE TO CHECK WP-RECAPTCHA */

		$tmpdata = get_option( 'templatic_settings' );

		$display = $tmpdata['user_verification_page'];

		if ( in_array( 'claim', $display ) ) {

			/*fetch captcha private key*/

			$privatekey = $tmpdata['secret'];

			/*get the response from captcha that the entered captcha is valid or not*/

			$response = wp_remote_get( 'https://www.google.com/recaptcha/api/siteverify?secret=' . $privatekey . '&response=' . sanitize_text_field( wp_unslash( $_REQUEST['g-recaptcha-response'] ) ) . '&remoteip=' . getenv( 'REMOTE_ADDR' ) );

			/*decode the captcha response*/

			$responde_encode = json_decode( $response['body'] );

			/*check the response is valid or not*/

			if ( ! $responde_encode->success ) {

				echo '<script>alert(' . esc_html_e( 'Your claim for this post has been sent successfully. ', 'templatic' ) . ');</script>';

			} else {

				echo '<script>alert(' . esc_html__( 'Invalid captcha. Please try again. ', 'templatic' ) . ')</script>';

				return false;

			}

		}

		/**

		 * END OF CODE - CHECK WP-RECAPTCHA.

		 */

		/**

		 * POST CLAIM FORM VALUES.

		 */

		$yourname = $post_details['claimer_name'];

		$youremail = $post_details['claimer_email'];

		$your_number = $post_details['claimer_contact'];

		$c_number = $post_details['claimer_contact'];

		$message = $post_details['claim_msg'];

		$claim_post_id = $post_details['post_id'];

		$post_title = $post_details['post_title'];

		$user_id = $current_user->ID;

		$author_id = $post_details['author_id'];

		if ( '' != $claim_post_id ) {

			/* added limit to query for query performance */

			$sql = "select ID,post_title from $wpdb->posts where ID ='" . $claim_post_id . "' LIMIT 0,1";

			$postinfo = $wpdb->get_results( $sql );

			foreach ( $postinfo as $postinfo_obj ) {

				$post_title = $postinfo_obj->post_title;

			}

		}

		$user_ip = $_SERVER['REMOTE_ADDR'];

		/* INSERTING CLAIM POST TYPE IN POST TABLE */

		$id = get_the_title( $post->ID );

		$claim_post_type = array(

			'post_title' => esc_html__( 'Claim for - ', 'templatic-admin' ) . $id . '',

			'post_content' => '' . $claim_post_id . '',

			'post_status' => 'publish',

			'post_author' => 1,

			'post_type' => 'claim',

			);

		/* INSERT QUERY */

		$post_id = wp_insert_post( $claim_post_type );

		/* INSERTING CLAIM INFORMATION IN POST META TABLE */

		add_post_meta( $post_id, 'post_claim_data', $post_details );

		/* END OF CODE - INSERT VALUES */

		$q = $wpdb->get_row( "SELECT * FROM $wpdb->users WHERE ID = 1 LIMIT 0,1" );

		$to_email = get_option( 'admin_email' );

		$to_name = $q->user_login;

		$site_name = '<a href="' . site_url() . '">' . get_option( 'blogname' ) . '</a>';

		$tmpdata = get_option( 'templatic_settings' );

		$email_subject = $tmpdata['claim_ownership_subject'];

		$claim = $tmpdata['claim_ownership_content'];

		if ( '' == @$email_subject ) {

			$email_subject = __( 'New Claim Submitted', 'templatic-admin' );

		}

		$subject_search_array = array( '[#post_id#]' );

		$subject_replace_array = array( $post_id );

		if ( '' == @$claim ) {

			$claim = __( '<p>Dear admin,</p><p>[#claim_name#] has submitted a claim for the post below.</p><p>[#message#]</p><p>Link: [#post_title#]</p><p>From: [#your_name#]</p><p>Email: [#claim_email#]<p>Phone Number: [#your_number#]</p>', 'templatic-admin' );

		}

		$filecontent_arr1 = $claim;

		$filecontent_arr2 = $filecontent_arr1;

		$client_message = $filecontent_arr2;

		$subject = $email_subject;

		if ( isset( $_REQUEST['link_url'] ) ) {

			$post_url_link = '<a href="' . esc_url( sanitize_text_field( wp_unslash( $_REQUEST['link_url'] ) ) ) . '">' . $post_title . '</a>';

			$post_link = '<a href="' . esc_url( sanitize_text_field( wp_unslash( $_REQUEST['link_url'] ) ) ) . '">' . esc_url( sanitize_text_field( wp_unslash( $_REQUEST['link_url'] ) ) ) . '</a>';

		}

		$yourname_link = sanitize_text_field( wp_unslash( $yourname ) );

		$admin_email = get_option( 'admin_email' );

		$store_login = '';

		$store_login_link = '';

		if ( function_exists( 'get_tevolution_login_permalink' ) ) {

			$store_login = '<a href="' . get_tevolution_login_permalink() . '">' . __( 'Click Login', 'templatic' ) . '</a>';

			$store_login_link = get_tevolution_login_permalink();

		}



		$search_array = array( '[#to_name#]', '[#post_title#]', '[#message#]', '[#your_name#]', '[#your_number#]', '[#post_url_link#]', '[#post_link#]', '[#site_name#]', '[#admin_email#]', '[#site_login_url#]', '[#site_login_url_link#]' );

		$replace_array = array( $to_name, $post_title, $message, $yourname_link, $your_number, $post_url_link, $post_link, $site_name, $admin_email, $store_login, $store_login_link );

		$client_message = str_replace( $search_array, $replace_array, $client_message );

		/* CALL A MAIL FUNCTION */

		templ_send_email( $youremail, $yourname, $to_email, $to_name, $subject, $client_message, $extra = '' );

	} // End if().

}



/*

 * Claimed listing - this function will return the listing is claimed or not.

 *

 */



add_shortcode( 'claim_ownership', 'tmpl_claim_ownership' );

/**

 * This function is for claim ownership.

 */

function tmpl_claim_ownership() {

	global $post, $wpdb;

	if ( is_single() || is_page() && ! is_page_template( 'page-template_form.php' ) ) :

		insert_claim_ownership_data( $_POST );



		if ( 1 == get_post_meta( $post->ID, 'is_verified', true ) ) {



		} else {

			$current_ip = $_SERVER['REMOTE_ADDR'];/* FETCH CURRENT USER IP ADDRESS */

			$post_claim_id = $wpdb->get_col( "SELECT ID from $wpdb->posts WHERE ( post_content = '" . $post->ID . "' ) AND post_status = 'publish' AND (post_excerpt = 'approved' OR post_excerpt = '' ) AND post_type='claim' limit 0,1" );

			if ( count( $post_claim_id ) > 0 ) {

				foreach ( $post_claim_id as $key => $val ) {

					$data = get_post_meta( $val, 'post_claim_data', true ); /* FETCH CLAIM ID */

					if ( $post->ID == $data['post_id'] ) {

						$user_ip = $data['claimer_ip']; /* FETCH IP ADDRESS OF CLAIMED POST */

						if ( $current_ip == $user_ip && '' != $user_ip ) { ?>

					<a class="claimed" href="javascript:void(0)"><?php esc_html_e( apply_filters( 'tmpl_already_claimed_text', 'Already Claimed' ), 'templatic' ); ?></a>

					<?php

						} else { ?>

				<a href="javascript:void(0)" id="trigger_id" title="<?php esc_html_e( 'Claim For This', 'templatic' );

				echo ' ' . wp_kses_post( ucfirst( $post->post_type ) );?>" data-open="tmpl_claim_listing" class="<?php echo wp_kses_post( apply_filters( 'tmpl_claim_button_class', 'i_claim c_sendtofriend' ) ); ?>" ><?php _e( apply_filters( 'tmpl_claiming_text', 'Claim Ownership' ), 'templatic' );?></a>

				<?php

						add_action( 'wp_footer', 'tevolution_claim_form' ); /* action for footer to include claim listing form  */

						}

					}

				}

			} else {

				add_action( 'wp_footer', 'tevolution_claim_form' ); /* action for footer to include claim listing form */

	?>

	<a href="javascript:void(0)" id="trigger_id" title="<?php esc_html_e( 'Claim For This Listing', 'templatic' ); ?>" data-open="tmpl_claim_listing" class="<?php echo wp_kses_post( apply_filters( 'tmpl_claim_button_class', 'i_claim c_sendtofriend' ) ); ?>"><?php _e( apply_filters( 'tmpl_claiming_text', 'Claim Ownership' ), 'templatic' );?></a>

	<?php

			}

		}

endif;

}



/**

 * Include the claim ownership form in footer.

 */

function tevolution_claim_form() {

	include_once( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-claim_ownership/popup_claim_form.php' );

}



/* while claim a listing from front end insert the data in post table*/

add_action( 'wp_ajax_tevolution_claimowner_ship', 'tevolution_claimowner_ship' );

add_action( 'wp_ajax_nopriv_tevolution_claimowner_ship', 'tevolution_claimowner_ship' );

/**

 * Claim ownership.

 */

function tevolution_claimowner_ship() {

	global $wpdb, $post;

	if ( isset( $_REQUEST['claimer_name'] ) ) {

		/* code to check wp-recaptcha */

		$tmpdata = get_option( 'templatic_settings' );

		$display = $tmpdata['user_verification_page'];

		if ( is_array( $display ) && in_array( 'claim', $display ) ) {

			/*fetch captcha private key*/

			$privatekey = $tmpdata['secret'];

			/*get the response from captcha that the entered captcha is valid or not*/

			$response = wp_remote_get( 'https://www.google.com/recaptcha/api/siteverify?secret=' . $privatekey . '&response=' . $_REQUEST['g-recaptcha-response'] . '&remoteip=' . getenv( 'REMOTE_ADDR' ) );

			/*decode the captcha response*/

			$responde_encode = json_decode( $response['body'] );

			/**

			 * Check the response is valid or not.

			 */

			/**

			 *Show response error.

			 */

			if ( is_wp_error( $response ) ) {

				$error_message = $response->get_error_message();

				echo wp_kses_post( $error_message ) . ' <br/>';

				esc_html_e( 'Please contact your host provider. ','templatic' );

				die;

			}

			if ( ! $responde_encode->success ) {

				echo '1';

				exit;

				echo '<script>alert(' . esc_html__( 'Invalid captcha. Please try again. ', 'templatic' ) . ');</script>';

				return false;

			}

		}

		do_action( 'tmpl_before_submit_claim', $_REQUEST );

		/**

		 * End of code - check wp-recaptcha.

		 */

		/**

		 * POST CLAIM FORM VALUES.

		 */

		if ( isset( $_REQUEST['claimer_name'] ) ) {

			$yourname = sanitize_text_field( wp_unslash( $_REQUEST['claimer_name'] ) );

		}

		if ( isset( $_REQUEST['claimer_email'] ) ) {

			$youremail = sanitize_text_field( wp_unslash( $_REQUEST['claimer_email'] ) );

		}

		if ( isset( $_REQUEST['claimer_contact'] ) ) {

			$your_number = sanitize_text_field( wp_unslash( $_REQUEST['claimer_contact'] ) );

		}

		if ( isset( $_REQUEST['claimer_contact'] ) ) {

			$c_number = sanitize_text_field( wp_unslash( $_REQUEST['claimer_contact'] ) );

		}

		if ( isset( $_REQUEST['claim_msg'] ) ) {

			$message = stripslashes( sanitize_text_field( wp_unslash( $_REQUEST['claim_msg'] ) ) );

		}

		if ( isset( $_REQUEST['post_id'] ) ) {

			$claim_post_id = intval( $_REQUEST['post_id'] );

		}

		if ( isset( $_REQUEST['post_title'] ) ) {

			$post_title = sanitize_text_field( wp_unslash( $_REQUEST['post_title'] ) );

		}

		$user_id = $current_user->ID;

		if ( isset( $_REQUEST['author_id'] ) ) {

			$author_id = intval( $_REQUEST['author_id'] );

		}

		if ( '' != $claim_post_id ) {

			/* added limit to query for query performance */

			$sql = "select ID,post_title from $wpdb->posts where ID = %d LIMIT 0,1";

			$postinfo = $wpdb->get_results( $wpdb->prepare( $sql, $claim_post_id ) );

			foreach ( $postinfo as $postinfo_obj ) {

				$post_title = $postinfo_obj->post_title;

			}

		}

		$user_ip = $_SERVER['REMOTE_ADDR'];

		/* inserting claim post type in post table */

		$id = get_the_title( $post->ID );

		$claim_post_type = array(

			'post_title' => esc_html__( 'Claim for - ', 'templatic-admin' ) . $id . '',

			'post_content' => '' . $claim_post_id . '',

			'post_status' => 'publish',

			'post_author' => 1,

			'post_type' => 'claim',

			);

		$post_id = wp_insert_post( $claim_post_type ); /* INSERT QUERY */

		/**

		 * Inserting claim information in post meta table.

		 */

		add_post_meta( $post_id, 'post_claim_data', $_REQUEST );

		/* end of code - insert values */

		$q = $wpdb->get_row( "SELECT * FROM $wpdb->users WHERE ID = 1 LIMIT 0,1" );

		$to_email = get_option( 'admin_email' );

		$to_name = $q->user_login;

		$tmpdata = get_option( 'templatic_settings' );

		$email_subject = @stripslashes( $tmpdata['claim_ownership_subject'] );

		$email_content = @stripslashes( $tmpdata['claim_ownership_content'] );

		$site_name = '<a href="' . site_url() . '">' . get_option( 'blogname' ) . '</a>';

		if ( '' == @$email_content ) {

			$email_subject = __( 'New Claim Submitted', 'templatic-admin' );

		}

		if ( '' == @$email_content ) {

			$email_content = __( '<p>Dear admin,</p><p> [#claim_name#] has claimed for this post</p><p>[#message#]</p><p>Link: [#post_title#]</p><p>From: [#your_name#]</p><p>Email: [#claim_email#]<p>Phone Number: [#your_number#]</p>', 'templatic-admin' );

		}

		$post_url_link = '<a href="' . get_permalink( $claim_post_id ) . '">' . $post_title . '</a>';

		$post_link = '<a href="' . get_permalink( $claim_post_id ) . '">' . get_permalink( $claim_post_id ) . '</a>';

		$subject_search_array = array( '[#post_title#]' );

		$subject_replace_array = array( $post_title );

		$email_subject = str_replace( $subject_search_array, $subject_replace_array, $email_subject );

		$subject = $email_subject;

		$yourname_link = $yourname;

		$site_name = stripslashes( get_option( 'blogname' ) );

		$admin_email = get_option( 'admin_email' );

		$store_login = '';

		$store_login_link = '';

		if ( function_exists( 'get_tevolution_login_permalink' ) ) {

			$store_login = '<a href="' . get_tevolution_login_permalink() . '">' . __( 'Click Login', 'templatic' ) . '</a>';

			$store_login_link = get_tevolution_login_permalink();

		}

		$search_array = array( '[#claim_name#]', '[#to_name#]', '[#post_title#]', '[#message#]', '[#your_name#]', '[#your_number#]', '[#post_url_link#]', '[#claim_email#]', '[#post_link#]', '[#site_name#]', '[#admin_email#]', '[#site_login_url#]', '[#site_login_url_link#]' );

		$replace_array = array( $yourname, $to_name, $post_url_link, $message, $yourname_link, $your_number, $post_url_link, $youremail, $post_link, $site_name, $admin_email, $store_login, $store_login_link );

		$client_message = str_replace( $search_array, $replace_array, $email_content );

		/**

		 * Call a mail function.

		 */

		esc_html_e( 'Your claim for this post has been sent successfully. ', 'templatic' );



		templ_send_email( $youremail, $yourname, $to_email, $to_name, $subject, $client_message, $extra = '' );

		exit;

	} // End if().

}





/**

 ************************* LOAD THE BASE CLASS *******************************

 * The WP_List_Table class isn't automatically available to plugins, so we need

 * to check if it's available and load it if necessary.

 */

if ( ! class_exists( 'Tmpl_WP_List_Table' ) ) {

	include_once( WP_PLUGIN_DIR . '/Tevolution/templatic.php' );

}

/**

 * Class templ_claimlist_table table

 */

class templ_claimlist_table extends Tmpl_WP_List_Table {

	/**

	 * Fetch all the data and store them in an array.

	 * Call a function that will return all the data in an array and we will assign that result to a variable $_posttaxonomy. FIRST OF ALL WE WILL FETCH DATA FROM POST META TABLE STORE THEM IN AN ARRAY $_posttaxonomy.

	 *

	 * @param var $_posttaxonomy define category.

	 * @param int $claim_post_id  declare id.

	 */

	function fetch_claimpost_data( $_posttaxonomy, $claim_post_id ) {

		$clid = $claim_post_id;

		$author_id = $_posttaxonomy['author_id'];

		$auth_data = get_userdata( $author_id );

		$post_id = $_posttaxonomy['post_id'];

		$title = $_posttaxonomy['post_title'];

		$claimant = $_posttaxonomy['claimer_name'];

		$claim_date = $_posttaxonomy['poublish_date'];

		$status = $_posttaxonomy['claim_status'];

		$action = '';

		$data = get_post_meta( $claim_post_id, 'post_claim_data', true );

		/**

		 * FETCH CLAIM DATA.

		 */

		$post_id = $data['post_id'];

		$post_title = $data['post_title'];

		$claimer_name = $data['claimer_name'];

		$name = str_word_count( $claimer_name,1 );



		$edit_url = get_permalink( $_posttaxonomy['post_id'] );



		if ( 'approved' == $status && 1 == get_post_meta( $post_id, 'is_verified', true ) ) :

			$status = YES_VERIFIED;

	elseif ( 'declined' == $status ) :

		$status = DECLINED;

	else :

		$status = PENDING;

	endif;



	?>

	<script type="text/javascript" async >

		/* <![CDATA[ */

		function confirmSubmit(str ) {

			var answer = confirm("<?php echo wp_kses_post( DELETE_CONFIRM_ALERT ); ?>");

			if ( answer) {

				window.location = "<?php echo esc_url( site_url() ); ?>/wp-admin/index.php?poid="+str;

				alert( '<?php echo wp_kses_post( ENTRY_DELETED ); ?>' );

			}

		}



		/* ]]> */

	</script>



	<?php

	$action = '';

	$action .= '<a href="#TB_inline?width=600&height=600&inlineId=claimed_details_' . $clid . '" id="claimed_' . $clid . '" class="thickbox" title="' . __( 'View claim details', 'templatic-admin' ) . '"><i class="fas fa-info"></i></a> &nbsp;&nbsp;';

	if ( 'Verified' == $status && 1 == get_post_meta( $post_id, 'is_verified', true ) ) {

		$action .= '<a href=' . site_url() . '/wp-admin/post.php?post=' . $post_id . '&action=edit&decline=yes&clid=' . $clid . ' title=' . DECLINE_CLAIM . '><i class="fas fa-times"></i></a>&nbsp;&nbsp;';

	} else {

		$action .= '<a href=' . site_url() . '/wp-admin/post.php?post=' . $post_id . '&action=edit&verified=yes&clid=' . $clid . '&user=' . $claimer_name . ' title=' . VERIFY_CLAIM . '><i class="fas fa-check-circle"></i></a>&nbsp;&nbsp;';

	}

	$action .= '<a href="javascript:void(0);confirmSubmit( ' . $clid . ' );"><i class="fas fa-minus-circle"></i></a>';

	$claim_post_data = get_post( $clid );

	$claim_post_date = $claim_post_data->post_date;

	$meta_data = array(

		'ID'		=> $claim_post_id,

		'post_id'	=> $post_id,

		'post_title'	=> '<strong><a href="' . $edit_url . '">' . $title . '</a></strong>',

		'claimant' 	=> $claimant,

		'claim_date' => date_i18n( get_option( 'date_format' ), strtotime( $claim_post_date ) ),

		'status' 	=> $status,

		'action' 	=> $action,

		);?>

		<div id="claimed_details_<?php echo intval( $clid ); ?>" style="display:none;width:400px; height:400px;">

			<h2><?php echo esc_html__( 'Author Details', 'templatic-admin' ); ?></h2>

			<p class="tev_description">

				<?php echo '<strong>' . esc_html__( 'Name', 'templatic-admin' ) . '</strong>';

				echo ': ' . wp_kses_post( $auth_data->display_name ); ?>

			</p>

			<p class="tev_description">

				<?php echo '<strong>' . esc_html__( 'Email', 'templatic-admin' ) . '</strong>';

				echo ': ' . wp_kses_post( $auth_data->user_email ); ?>

			</p>





			<h2><?php echo esc_html__( 'Claimer Details', 'templatic-admin' ); ?></h2>

			<p class="tev_description">

				<?php echo '<strong>' . esc_html__( 'Name', 'templatic-admin' ) . '</strong>';

				echo ': ' . wp_kses_post( $_posttaxonomy['claimer_name'] ); ?>

			</p>

			<p class="tev_description">

				<?php echo '<strong>' . esc_html__( 'Email', 'templatic-admin' ) . '</strong>';

				echo ': ' . wp_kses_post( $_posttaxonomy['claimer_email'] ); ?>

			</p>

			<p class="tev_description">

				<?php echo '<strong>' . esc_html__( 'Conatct No. ', 'templatic-admin' ) . '</strong>';

				echo ': ' . wp_kses_post( $_posttaxonomy['claimer_contact'] ); ?>

			</p>

			<p class="tev_description">

				<?php echo '<strong>' . esc_html__( 'Message', 'templatic-admin' ) . '</strong>';

				echo ': ' . wp_kses_post( $_posttaxonomy['claim_msg'] ); ?>

			</p>

			<?php do_action( 'tmpl_extra_claiming_details', $_posttaxonomy ); ?>

		</div>

		<?php

		return $meta_data;

	}



	/**

	 * Fetch taxonomy data.

	 */

	function taxonomy_data() {

		global $post, $wpdb;

		$claim_posts = array();

		$claim_post_ids = $wpdb->get_col( "SELECT ID FROM $wpdb->posts WHERE post_type = 'claim' order by ID desc" );

		if ( count( $claim_post_ids ) != 0 ) {

			$counter = 0;

			foreach ( $claim_post_ids as $claim_post_id ) {

				$data = get_post_meta( $claim_post_id, 'post_claim_data', true );

				$claim_posts[] = $this->fetch_claimpost_data( $data, $claim_post_id );

			}

		}



		return $claim_posts;

	}

	/* eof - fetch taxonomy data */



	/**

	 * Define the columns for the table.

	 */

	function get_columns() {

		$columns = array(

			'cb' => '<input type="checkbox" />',

			'post_id' => __( 'ID', 'templatic-admin' ),

			'post_title' => __( 'Claim On', 'templatic-admin' ),

			'claimant' => __( 'Claimant', 'templatic-admin' ),

			'claim_date' => __( 'Date', 'templatic-admin' ),

			'status' => __( 'Status', 'templatic-admin' ),

			'action' => __( 'Action', 'templatic-admin' ),

			);

		return $columns;

	}

	/**

	 * Detect when a bulk action is being triggered.

	 */

	function process_bulk_action() {

		if ( 'delete' === $this->current_action() && isset( $_REQUEST['checkbox'] ) ) {

			foreach ( $_REQUEST['checkbox'] as $postid ) {

				wp_delete_post( $postid );

			}

			$url = site_url() . '/wp-admin/admin.php';

			wp_redirect( $url . '?page=ownership_listings&claim_msg=delsuccess' );

			exit;

		}

	}

	/**

	 * Prepare items.

	 */

	function prepare_items() {

		$per_page = $this->get_items_per_page( 'clistings_per_page', 10 );

		$columns = $this->get_columns(); /* call function to get the columns */

		$hidden = array();

		$sortable = array();

		$sortable = $this->get_sortable_columns(); /* get the sortable columns */

		$this->_column_headers = array( $columns, $hidden, $sortable );

		$this->process_bulk_action(); /* function to process the bulk actions */

		$data = $this->taxonomy_data(); /* retirive the package data */



		/**

		 * Function that sorts the columns.

		 *

		 * @param var $a variable a.

		 * @param var $b variable b.

		 */

		function usort_reorder( $a, $b ) {

			$orderby = ( isset( $_REQUEST['orderby'] ) && ! empty( $_REQUEST['orderby'] ) )  ? sanitize_text_field( wp_unslash( $_REQUEST['orderby'] ) ) : 'title'; /*If no sort, default to title*/

			$order = ( isset( $_REQUEST['order'] ) && ! empty( $_REQUEST['order'] ) )  ? sanitize_text_field( wp_unslash( $_REQUEST['order'] ) ) : 'asc'; /*If no order, default to asc*/

			$result = strcmp( $a[ $orderby ], $b[ $orderby ] ); /*Determine sort order*/

			return ( 'asc' === $order ) ? $result : -$result; /*Send final sort direction to usort*/

		}



		$current_page = $this->get_pagenum(); /* get the pagination */

		$total_items = count( $data ); /* calculate the total items */

		if ( is_array( $data ) ) {

			$this->found_data = array_slice( $data,( ( $current_page -1 ) * $per_page ), $per_page ); /* trim data for pagination*/

		}

		$this->items = $this->found_data;

		/**

		 * Assign sorted data to items to be used elsewhere in class.

		 */

		/* register pagination options */



		$this->set_pagination_args( array(

			'total_items' => $total_items,   /* WE have to calculate the total number of items */

			'per_page'  => $per_page,    /* WE have to determine how many items to show on a page */

			)

		);

	}



	/**

	 * To avoid the need to create a method for each column there is column_default that will process any column for which no special method is defined.

	 *

	 * @param var $item item.

	 * @param var $column_name column name.

	 */

	function column_default( $item, $column_name ) {

		switch ( $column_name ) {

			case 'cb':

			case 'post_id':

			case 'post_title':

			case 'claimant':

			case 'claim_date':

			case 'status':

			case 'action':

			return $item[ $column_name ];

			default:

			return print_r( $item, true ); /* Show the whole array for troubleshooting purposes */

		}

	}



	/**

	 * Define the columns to be sorted.

	 */

	function get_sortable_columns() {

		$sortable_columns = array(

			'title' => array( 'title', true ),

			);

		return $sortable_columns;

	}



	/**

	 * Define the links displaying below the title.

	 *

	 * @param var $item item.

	 */

	function column_title( $item ) {

		return '';

	}



	/**

	 * Define the bulk actions.

	 */

	function get_bulk_actions() {

		$actions = array(

			'delete' => 'Delete permanently',

			);

		return $actions;

	}



	/**

	 * Check box to select all the taxonomies.

	 *

	 * @param var $item item is defined.

	 */

	function column_cb( $item ) {

		return sprintf(

			'<input type="checkbox" name="checkbox[]" id="checkbox[]" value="%s" />', $item['ID']

		);

	}

}



/* Add verified budge on detail page */

add_action( 'after_title_h1', 'tmpl_after_title_returns' );

/**

 * This funtion is return title

 */

function tmpl_after_title_returns() {

	global $post;

	if ( get_post_meta( $post->ID, 'is_verified', true ) == 1 ) {

		echo '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="fa-stack has-tip tip-right" title="' . esc_html__( 'Verified','templatic' ) . '"><i class="fas fa-certificate fa-stack-2x"></i><i class="fas fa-check fa-stack-1x"></i></span>';

	}

}

