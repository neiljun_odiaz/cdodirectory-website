<?php
/**
 * Define constants for claim ownership.
 *
 * @package WordPress
 */

global $wp_query, $wpdb, $wp_rewrite;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/* activating claim ownership */

if ( (isset( $_REQUEST['activated'] ) && 'claim_ownership' == $_REQUEST['activated'] ) && (isset( $_REQUEST['true'] ) && 1 == $_REQUEST['true'] ) ) {
	update_option( 'claim_ownership', 'Active' );
	update_option( 'claim_enabled', 'Yes' );
	$types['claim_post_type_value'] = get_post_types();
	$tmpdata = get_option( 'templatic_settings' );
	update_option( 'templatic_settings', array_merge( $tmpdata, $types ) );
} elseif ( (isset( $_REQUEST['deactivate'] ) && 'claim_ownership' == sanitize_text_field( wp_unslash( $_REQUEST['deactivate'] ) ) ) && (isset( $_REQUEST['true'] ) && 0 == sanitize_text_field( wp_unslash( @$_REQUEST['true'] ) ) ) ) {
	delete_option( 'claim_enabled' );
	delete_option( 'claim_ownership' );
}

/* eof - claim ownership activation */

/* define Claim Ownership Constants variable	*/
define( 'ID_TEXT', esc_html__( 'ID', 'templatic-admin' ) );
define( 'CLAIM_AUTHOR_NAME_TEXT', esc_html__( 'Author', 'templatic-admin' ) );
define( 'CLAIMER_TEXT', esc_html__( 'Claimant', 'templatic-admin' ) );
define( 'CONTACT_NUM_TEXT', esc_html__( 'Contact', 'templatic-admin' ) );
define( 'CONTACT_EMAIL_TEXT', esc_html__( 'Email', 'templatic-admin' ) );
define( 'ACTION_TEXT', esc_html__( 'Action', 'templatic-admin' ) );
define( 'DETAILS_CLAIM', esc_html__( 'Detail', 'templatic-admin' ) );
define( 'VERIFY_CLAIM', esc_html__( 'Verify', 'templatic-admin' ) );
define( 'DECLINE_CLAIM', esc_html__( 'Decline', 'templatic-admin' ) );
define( 'DECLINED', esc_html__( 'Declined', 'templatic-admin' ) );
define( 'VIEW_CLAIM', esc_html__( 'View Post', 'templatic-admin' ) );
define( 'DELETE_CLAIM', esc_html__( 'Delete this request', 'templatic-admin' ) );
define( 'NO_CLAIM', esc_html__( 'No Claim request for this post. ', 'templatic-admin' ) );
define( 'REMOVE_CLAIM_REQUEST', esc_html__( 'Remove Claim Request', 'templatic-admin' ) );
define( 'YES_VERIFIED', esc_html__( 'Verified', 'templatic-admin' ) );
define( 'POST_VERIFIED_TEXT', esc_html__( 'This post is verified. ', 'templatic-admin' ) );
define( 'CLAIM_BUTTON', esc_html__( 'Claim this post', 'templatic-admin' ) );
define( 'OWNER_VERIFIED', esc_html__( 'Owner Verified Listing', 'templatic-admin' ) );
define( 'OWNER_TEXT', esc_html__( 'Do you own this post?', 'templatic-admin' ) );
define( 'VERIFY_OWNERSHIP_FOR', esc_html__( 'Verify your ownership for', 'templatic-admin' ) );
define( 'FULL_NAME', esc_html__( 'Full Name', 'templatic-admin' ) );
define( 'EMAIL', esc_html__( 'Your Email', 'templatic-admin' ) );
define( 'CONTACT', esc_html__( 'Contact No', 'templatic-admin' ) );
define( 'CLAIM', esc_html__( 'Your Claim', 'templatic-admin' ) );
define( 'DELETE_CONFIRM_ALERT', esc_html__( 'Are you sure want to delete this claim?', 'templatic-admin' ) );
define( 'ENTRY_DELETED', esc_html__( 'Claim Deleted', 'templatic-admin' ) );
define( 'NO_CLAIM_REQUEST', esc_html__( 'No post has been claimed on your site. ', 'templatic-admin' ) );
define( 'STATUS', esc_html__( 'Status', 'templatic-admin' ) );
define( 'PENDING', esc_html__( 'Pending', 'templatic-admin' ) );
define( 'SELECT_POST_TYPE', esc_html__( 'Select Post Types', 'templatic-admin' ) );
define( 'SELECT_DISPLAY_TYPE', esc_html__( 'Select Display Type', 'templatic-admin' ) );
define( 'LINK', esc_html__( 'Link', 'templatic-admin' ) );
define( 'BUTTON', esc_html__( 'Button', 'templatic-admin' ) );
define( 'ALREADY_CLAIMED', esc_html__( apply_filters( 'tmpl_already_claimed_text', 'Already Claimed' ), 'templatic-admin' ) );

/* including a functions file */
if ( file_exists( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-claim_ownership/claim_functions.php' ) ) {
	include( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-claim_ownership/claim_functions.php' );
}
add_action( 'templ_add_admin_menu_', 'templ_add_claim_page_menu', 13 );

/**
 * Include menu after custom fields menu.
 */
function templ_add_claim_page_menu() {
	$menu_title3 = __( 'Submitted Claims', 'templatic-admin' );
	add_submenu_page( 'templatic_system_menu', $menu_title3, $menu_title3, 'administrator', 'ownership_listings', 'tmpl_claimed_listing' );

}

/**
 * Load claim listings page.
 */
function tmpl_claimed_listing() {
	include( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-claim_ownership/manage_claim_listings.php' );
}
/* call a function to add dashboard metabox */
add_action( 'wp_dashboard_setup', 'add_claim_dashboard_metabox' );

/* call a function to add metabox in post types */
add_action( 'admin_init', 'add_claim_metabox_posts' );

/*
	Add Action for display basic setting data
*/
	add_action( 'templatic_general_setting_data', 'claim_setting_data', 11 );


/**
 * Display the claim section in general setting menu section
 *
 * @param var $column define column.
 */
function claim_setting_data( $column ) {
		$tmpdata = get_option( 'templatic_settings' );
		?>
		<table id="general_claim_setting" class="tmpl-general-settings form-table">
			<tr>
				<td colspan="2">
					<label for="ilc_tag_class"><p class="tevolution_desc"><?php echo sprintf( esc_html__( 'Claim ownership allows visitors to claim a certain post on your site as their own. For details on how this works visit the %s. ', 'templatic-admin' ), '<a href="//templatic.com/docs/tevolution-guide/#claim_settings" title="Claim Settings" target="_blank">documentation guide</a>' ); ?></p></label>
				</td>
			</tr>
			<tr>
				<th><label><?php echo esc_html__( 'Enable claim ownership for', 'templatic-admin' );?></label></th>
				<td>
					<?php $value = @$tmpdata['claim_post_type_value'];
					$posttaxonomy = get_option( 'templatic_custom_post' );
					if ( ! empty( $posttaxonomy ) ) {
						foreach ( $posttaxonomy as $key => $_posttaxonomy ) :
							if ( 'admanager' == $key || 'booking' == $key ) {
								continue;
							}
							?>
							<div class="element">
								<label for="<?php echo 'claim_' . wp_kses_post( $key ); ?>"><input type="checkbox" name="claim_post_type_value[]" id="<?php echo 'claim_' . wp_kses_post( $key ); ?>" value="<?php echo wp_kses_post( $key ); ?>" <?php if ( @$value && in_array( $key, $value ) ) { echo wp_kses_post( 'checked=checked' ); } ?>>&nbsp;<?php echo wp_kses_post( $_posttaxonomy['label'] ); ?></label>
							</div>
						<?php endforeach;
					} else {
						echo sprintf( esc_html__( ' No custom post type has been created at your site yet. Please %s to list it here. ', 'templatic-admin' ), '<a href="?page=ownership_listings"> create it </a>' );
					}?><p class="description"><?php echo esc_html__( 'Once enabled, a "Claim Ownership" button will appear inside detail pages (above the tabs). ', 'templatic-admin' )?></p><br />
					<p class="description"><?php echo wp_kses_post( wp_unslash( __( 'Notes: <br />1. Check this feature without logging in to the site. <br />2. The claim ownership button will not remain visible for the users who has submitted the specific listings. ', 'templatic-admin' ) ) )?></p><br />
				</td>
			</tr>
			<tr>
				<th></th>
				<td style="float:right;">
					<p><a href="<?php echo esc_url( site_url() . '/wp-admin/admin.php?page=ownership_listings' ); ?>"><?php esc_html_e( 'View all claimed lIstings', 'templatic-admin' ); ?></a></p>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<p class="submit" style="clear: both;">
						<input type="submit" name="Submit" class="button button-primary button-hero" value="<?php echo esc_html__( 'Save All Settings', 'templatic-admin' );?>" />

						<input type="hidden" name="settings-submit" value="Y" />
					</p>
				</td>
			</tr>
		</table>
		<?php
}
	/* Finish claim setting data */
