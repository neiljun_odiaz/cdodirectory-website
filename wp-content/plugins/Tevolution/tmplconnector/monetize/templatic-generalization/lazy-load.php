<?php
/**
 * Description: Lazy load images to improve page load times. Uses jQuery.sonar to only load an image when it's visible in the viewport.
 */

if ( ! class_exists( 'Tmpl_LazyLoad_Images' ) ) :

class Tmpl_LazyLoad_Images {

	protected static $enabled = true;

	static function init() {
		if ( is_admin() )
			return;

		if ( ! apply_filters( 'lazyload_is_enabled', true ) ) {
			self::$enabled = false;
			return;
		}
		
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'tmpl_add_scripts' ) );
		add_action( 'wp_head', array( __CLASS__, 'tmpl_setup_filters' ), 9999 ); // we don't really want to modify anything in <head> since it's mostly all metadata, e.g. OG tags
	}

	static function tmpl_setup_filters() { 
		global $post;
		global $post;
		
		$is_submit = get_post_meta( @$post->ID, 'is_tevolution_submit_form', true );
		if( 1 != $is_submit ) {
			add_filter( 'the_content', array( __CLASS__, 'tmpl_add_image_placeholders' ), 99 ); // run this later, so other content filters have run, including image_add_wh on WP.com
			add_filter( 'post_thumbnail_html', array( __CLASS__, 'tmpl_add_image_placeholders' ), 11 );
			//add_filter( 'get_avatar', array( __CLASS__, 'tmpl_add_image_placeholders' ), 11 );
		}
	}

	static function tmpl_add_scripts() {
		global $post;
		
		$is_submit = get_post_meta( @$post->ID, 'is_tevolution_submit_form', true );
		if( 1 != $is_submit ) {
			wp_enqueue_script( 'wpcom-lazy-load-images',  self::get_url( 'js/lazy-load.js' ), array( 'jquery', 'jquery-sonar' ), '', true );
			wp_enqueue_script( 'jquery-sonar', self::get_url( 'js/jquery.sonar.min.js' ), array( 'jquery' ), '', true );
		}
	}

	static function tmpl_add_image_placeholders( $content ) {
		if ( ! self::is_enabled() )
			return $content;

		// Don't lazyload for feeds, previews, mobile
		if( is_feed() || is_preview() )
			return $content;

		// Don't lazy-load if the content has already been run through previously
		if ( false !== strpos( $content, 'data-lazy-src' ) )
			return $content;

		// This is a pretty simple regex, but it works
		$content = preg_replace_callback( '#<(img)([^>]+?)(>(.*?)</\\1>|[\/]?>)#si', array( __CLASS__, 'tmpl_process_image' ), $content );

		return $content;
	}

	static function tmpl_process_image( $matches ) {
		// In case you want to change the placeholder image
		$placeholder_image = apply_filters( 'tmpl_lazyload_images_placeholder_image', CUSTOM_FIELDS_URLPATH . 'images/lightbox-blank.gif' );

		$old_attributes_str = $matches[2];
		$old_attributes = wp_kses_hair( $old_attributes_str, wp_allowed_protocols() );

		if ( empty( $old_attributes['src'] ) ) {
			return $matches[0];
		}

		$image_src = $old_attributes['src']['value'];

		// Remove src and lazy-src since we manually add them
		$new_attributes = $old_attributes;
		unset( $new_attributes['src'], $new_attributes['data-lazy-src'] );

		$new_attributes_str = self::build_attributes_string( $new_attributes );

		return sprintf( '<img src="%1$s" data-lazy-src="%2$s" %3$s><noscript>%4$s</noscript>', esc_url( $placeholder_image ), esc_url( $image_src ), $new_attributes_str, $matches[0] );
	}

	private static function build_attributes_string( $attributes ) {
		$string = array();
		foreach ( $attributes as $name => $attribute ) {
			$value = $attribute['value'];
			if ( '' === $value ) {
				$string[] = sprintf( '%s', $name );
			} else {
				$string[] = sprintf( '%s="%s"', $name, esc_attr( $value ) );
			}
		}
		return implode( ' ', $string );
	}

	static function is_enabled() {
		return self::$enabled;
	}

	static function get_url( $path = '' ) {
		return plugins_url( ltrim( $path, '/' ), __FILE__ );
	}
}

function tmpl_lazyload_images_add_placeholders( $content ) {
	return Tmpl_LazyLoad_Images::add_image_placeholders( $content );
}

add_action( 'init', array( 'Tmpl_LazyLoad_Images', 'init' ) );

endif;
