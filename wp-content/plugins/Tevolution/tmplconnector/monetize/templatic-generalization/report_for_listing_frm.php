<?php
/**
 * Popup forms.
 *
 * @package Wordpress
 * @subpackage Tevolution
 */

global $post, $wp_query;
$tmpdata = get_option( 'templatic_settings' );
$display = @$tmpdata['user_verification_page'];

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div id="report_listing_frm" class="reveal-modal tmpl_login_frm_data clearfix" data-reveal>
	<form name="report_listing_form" id="report_listing_form" action="#" method="post">
		<input type="hidden" id="send_post_id" name="post_id" value="<?php echo intval( $post->ID );?>"/>
		<div class="email_to_friend">
			<h3 class="h3"><?php esc_html_e( 'Report Listing', 'templatic' );?></h3>
			<a class="modal_close" href="javascript:;"></a>
			<!--<a class="close-reveal-modal" href="#">x</a>-->
		</div>
		<div class="form_row clearfix" >
			<label><?php esc_html_e( 'Listing name', 'templatic' );?>: <span class="indicates"></span></label>
			<input name="report_listing_name" id="report_listing_name" type="text" readonly value="<?php echo $post->post_title;?>" />
			<span id="report_listing_nameInfo"></span>
		</div>
		<div class="form_row clearfix" >
			<label><?php esc_html_e( 'Your name', 'templatic' );?>: <span class="indicates">*</span></label>
			<input name="re_yourname" id="re_yourname" type="text" />
			<span id="re_yournameInfo"></span>
		</div>
		<div class="form_row clearfix" >
			<label> <?php esc_html_e( 'Your email', 'templatic' );?>: <span class="indicates">*</span></label>
			<input name="re_youremail" id="re_youremail" type="text" />
			<span id="re_youremailInfo"></span>
		</div>
		<div class="form_row clearfix" >
			<label><?php esc_html_e( 'Reason', 'templatic' );?>: </label>
			<textarea name="report_reason" id="report_reason" cols="10" rows="5" ><?php esc_html_e( 'Reason for reporting this listing', 'templatic' ); ?></textarea>
			<span id="report_reasonInfo"></span>
		</div>
		<div class="send_info_button clearfix">
			<input name="Send" type="submit" value="<?php esc_html_e( 'Send', 'templatic' )?> " class="button send_button" />
			<span id="process_send_friend" style="display:none;"><i class="fa fa-circle-o-notch fa-spin"></i></span>
			<strong id="send_report_msg" class="process_state"></strong>
		</div>
	</form>
</div>
