<?php
/**
 * Add to favourite.
 *
 * @package Wordpress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'REMOVE_FAVOURITE_TEXT', __( 'Added', 'templatic' ) );
/**
 * This function would add properly to favorite listing and store the value in wp_usermeta table user_favorite field.
 *
 * @param int $post_id post_id.
 * @param var $language language.
 */
function add_to_favorite( $post_id, $language = '' ) {
	global $current_user, $post;
	if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
		global $sitepress;
		$sitepress->switch_lang( $language );
	}
	
	$user_meta_data = array();
	$ar = get_user_meta( $current_user->ID, 'user_favourite_post', true );
	if ( ! empty( $ar ) ) {
		$user_meta_data = get_user_meta( $current_user->ID, 'user_favourite_post', true );
	}
	$user_meta_data[] = $post_id;
	
	$remove_fav_text = __( 'Remove from favorites', 'templatic' );
	update_user_meta( $current_user->ID, 'user_favourite_post', $user_meta_data );
	echo '<a href="javascript:void(0);" title="' . wp_kses_post( wp_unslash( $remove_fav_text ) ) . '" class="removefromfav added" data-id=' . intval( $post_id ) . ' onclick="javascript:addToFavourite(\'' . intval( $post_id ) . '\', \'remove\' );"><i class="fas fa-heart"></i>' . esc_html__( 'Favourite', 'templatic' ) . '</a>';
}
/**
 * This function would remove the favorited property earlier.
 *
 * @param integer $post_id 			Post id.
 * @param string  $language 		Language of WPML.
 */
function remove_from_favorite( $post_id, $language = '' ) {
	global $current_user, $post;
	if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
		global $sitepress;
		$sitepress->switch_lang( $language );
	}
	$user_meta_data = array();
	$user_meta_data = get_user_meta( $current_user->ID, 'user_favourite_post', true );
	if ( in_array( $post_id, $user_meta_data ) ) {
		$user_new_data = array();
		foreach ( $user_meta_data as $key => $value ) {
			if ( $post_id == $value ) {
				$value = '';
			} else {
				$user_new_data[] = $value;
			}
		}
		$user_meta_data	= $user_new_data;
	}
	if ( function_exists( 'tmpl_wp_is_mobile' ) && ! tmpl_wp_is_mobile() ) :
		$add_to_favorite = __( 'Add to favorites', 'templatic' );
	else :
		$add_to_favorite = __( 'Favorites', 'templatic' );
	endif;
	update_user_meta( $current_user->ID, 'user_favourite_post', $user_meta_data );
	echo '<a class="addtofav removed" title="' . wp_kses_post( wp_unslash( strip_tags( $add_to_favorite ) ) ) . '" href="javascript:void(0);" data-id=' . intval( $post_id ) . ' onclick="javascript:addToFavourite(\'' . intval( $post_id ) . '\', \'add\' );"><i class="far fa-heart"></i>';
	esC_html_e( 'Add to favorites', 'templatic' );
	echo '</a>';
}
/**
 * Add to favourite HTML code LIKE addtofav link and remove to fav link.
 *
 * @param integer $user_id 			User Id.
 * @param array   $post 			Post Array.
 */
function tevolution_favourite_html( $user_id = '', $post = '' ) {
	if ( function_exists( 'tmpl_wp_is_mobile' ) && ! tmpl_wp_is_mobile() ) {
			global $current_user, $post;
			$post_id = $post->ID;
			$add_to_favorite = __( 'Add to favorites', 'templatic' );
			$added = __( 'Added', 'templatic' );
			$remove = __( 'Remove from favourites', 'templatic' );
		if ( function_exists( 'icl_register_string' ) ) {
				icl_register_string( 'templatic', 'tevolution' . $add_to_favorite, $add_to_favorite );
				$add_to_favorite = icl_t( 'templatic', 'tevolution' . $add_to_favorite, $add_to_favorite );
				icl_register_string( 'templatic', 'tevolution' . $added, $added );
				$added = icl_t( 'templatic', 'tevolution' . $added, $added );
		}
			$user_meta_data = get_user_meta( $current_user->ID, 'user_favourite_post', true );
		if ( 'post' != $post->post_type ) {
			if ( is_tax() ) {
					$class = '';
			} else {
				if ( ! isset( $_GET['post_type'] ) ) {
					$class = '';
				} else {
					$class = '';
				}
			}
			if ( $user_meta_data && in_array( $post_id, $user_meta_data ) ) {
					?>
					<span id="tmplfavorite_<?php echo intval( $post_id );?>" class="fav fav_<?php echo intval( $post_id );?> " > <a title="<?php echo esc_attr( $remove ); ?>" href="javascript:void(0);" id="tmpl_login_frm_<?php echo intval( $post_id ); ?>" data-id='<?php echo intval( $post_id ); ?>' class="removefromfav  <?php echo esc_attr( $class ); ?> small_btn" onclick="javascript:addToFavourite( '<?php echo intval( $post_id );?>', 'remove' );"><?php echo wp_kses_post( wp_unslash( $added ) );?></a>  </span>
					<?php
			} else {
				if ( '' == $current_user->ID ) {
						$data_reveal_id = 'data-open="tmpl_reg_login_container"';
				} else {
						$data_reveal_id = '';
				}
					?>
					<span id="tmplfavorite_<?php echo intval( $post_id );?>" class="fav fav_<?php echo intval( $post_id );?>"><a title="<?php echo esc_attr( $add_to_favorite ); ?>" href="javascript:void(0);" <?php echo wp_kses_post( wp_unslash( $data_reveal_id ) ); ?> id="tmpl_login_frm_<?php echo intval( $post_id ); ?>" data-id='<?php echo intval( $post_id ); ?>' class="addtofav listing-cat-unit-add-fav <?php echo esc_attr( $class ); ?> small_btn" onclick="javascript:addToFavourite( '<?php echo intval( $post_id );?>', 'add' );"><?php echo wp_kses_post( wp_unslash( strip_tags( $add_to_favorite ) ) );?></a></span>
					<?php }
		}
	} // End if().
}
		add_action( 'init', 'tev_add_to_favourites', 11 );
		/**
		 * Add this to add to favourites only if current theme is support.
		 */
function tev_add_to_favourites() {
			global $current_user;
	if ( current_theme_supports( 'tevolution_my_favourites' ) && defined( 'DOING_AJAX' ) && ! @DOING_AJAX ) {
				global $post;
				add_action( 'templ_post_title', 'tevolution_favourite_html', 11,@$post );
	}
}

add_action( 'wp_footer', 'add_post_id_add_to_fav', 99 );
/**
 * Add post id while add to favorite as a logout user.
 */
function add_post_id_add_to_fav() {
	global $current_user;
	if ( '' == $current_user->ID ) {
		?>
		<script type="text/javascript" async >
			jQuery(function( ) {
				jQuery(document).on( 'click', '.addtofav', function() {
					post_id = jQuery(this).attr( 'data-id' );
					/*add html while login to add to favorite*/
					jQuery( '#tmpl_login_frm form#loginform' ).append( '<input type="hidden" name="post_id" value="'+post_id+'" />' );
					jQuery( '#tmpl_login_frm form#loginform' ).append( '<input type="hidden" name="addtofav" value="addtofav" />' );
					jQuery( '#tmpl_login_frm form#loginform [name=redirect_to]' ).val(jQuery(location).attr( 'href' ) );
					/*add html while register to add to favorite*/
					jQuery( '#tmpl_sign_up form#userform' ).append( '<input type="hidden" name="post_id" value="'+post_id+'" />' );
					jQuery( '#tmpl_sign_up form#userform' ).append( '<input type="hidden" name="addtofav" value="addtofav" />' );
					jQuery( '#tmpl_sign_up form#userform [name=reg_redirect_link]' ).val(jQuery(location).attr( 'href' ) );
				});
			});
		</script>
		<?php
	}
}

add_filter( 'tevolution_register_redirect', 'tmpl_add_fav_logout_user', 10,2 );
add_filter( 'tevolution_login_redirect', 'tmpl_add_fav_logout_user', 10,2 );
/**
 * Update user while add to favorite when he/she login or register.
 *
 * @param string $redirect_url 		Redirect url.
 * @param array  $user 				User Array.
 */
function tmpl_add_fav_logout_user( $redirect_url, $user ) {
	if ( isset( $_POST['addtofav'] ) && 'addtofav' == $_POST['addtofav'] && isset( $_POST['post_id'] ) && '' != $_POST['post_id'] ) {
		if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
			global $sitepress;
			$sitepress->switch_lang( $language );
		}
		$user_meta_data = array();
		$user_meta_data = get_user_meta( $current_user->ID, 'user_favourite_post', true );
		$user_meta_data[] = intval( $_POST['post_id'] );
		update_user_meta( $user->ID, 'user_favourite_post', $user_meta_data );
	}
	if ( isset( $_POST['reg_redirect_link'] ) && '' != $_POST['reg_redirect_link'] ) {
		$return_url = wp_kses_post( wp_unslash( $_POST['reg_redirect_link'] ) );
	} elseif ( isset( $_POST['redirect_to'] ) && '' != $_POST['redirect_to'] ) {
		$return_url = wp_kses_post( wp_unslash( $_POST['redirect_to'] ) );
	} else {
		$return_url = $redirect_url;
	}
	return $return_url;
}
