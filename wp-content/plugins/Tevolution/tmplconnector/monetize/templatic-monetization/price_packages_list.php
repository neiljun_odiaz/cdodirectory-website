<?php
/**
 * Include package class file to fetch listing of of price package.
 *
 * @package wordpress.
 * @subpackage Tevolution
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

include_once( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-monetization/price_package_class.php' );

?>
<form method="post" action="" id="posts-filter">
	<div class="wrap">

		<div class="tevo_sub_title"><?php echo esc_html__( 'Manage Price Packages', 'templatic-admin' ); ?>
			<a id="add_price_package" class="add-new-h2" href="<?php echo esc_url( admin_url( 'admin.php?page=monetization&action=add_package&tab=packages' ) ); ?>"><?php echo esc_html__( 'Add New Package', 'templatic-admin' ); ?></a>
		</div>
		<p class="tevolution_desc"><?php echo wp_kses_post( wp_unslash( __( 'Price Packages allow you to monetize your submission form and make money. For more on how they work visit the <a href="//templatic.com/docs/tevolution-guide/#price_packages" title="Price Packages" target="_blank">Price Package Guide</a>. If you have added only one package to any post type then it will get selected by default on the submission page. Thus it will not be visible there. ', 'templatic-admin' ) ) );?></p>
		<?php if ( isset( $_REQUEST['package_msg'] ) ) {
			?>
		<div class="updated fade below-h2" id="message" style="padding:5px; font-size:12px;" >
			<?php if ( 'delete' == $_REQUEST['package_msg'] ) {
				echo esc_html__( 'Package permanently deleted. ', 'templatic-admin' );
} elseif ( 'success' == $_REQUEST['package_msg'] ) {
	if ( isset( $_REQUEST['package_msg_type'] ) && 'add' == $_REQUEST['package_msg_type'] ) {
					echo esc_html__( 'Package created successfully. ', 'templatic-admin' );
	} else {
		echo esc_html__( 'Package updated successfully. ', 'templatic-admin' );
	}
} ?>
		</div>
		<?php }
		wp_enqueue_script( 'jquery-ui-sortable' );
		echo '<div class="tevolution_price_package">';
		$templ_list_table = new templatic_List_Table();
		$templ_list_table->prepare_items();
		$templ_list_table->search_box( 'search', 'search_id' );
		$templ_list_table->display();
		echo '</div>';
if ( isset( $_REQUEST['page'] ) && isset( $_REQUEST['tag'] ) ) : ?>
		<input type="hidden" name="page" value="<?php echo wp_kses_post( wp_unslash( $_REQUEST['page'] ) ); ?>" />
		<input type="hidden" name="tag" value="<?php echo wp_kses_post( wp_unslash( $_REQUEST['tag'] ) ); ?>" />

	<?php endif; ?>
</div>
</form>
