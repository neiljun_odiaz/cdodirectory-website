<?php
$login_usr_id=get_current_user_id();
$results_per_page = 10;
$current = $_GET['page'] ? $_GET['page'] : 1;

global $wpdb;

$table = $wpdb->prefix."transactions";  
$rows = $wpdb->get_results("SELECT * FROM " . $table." Where user_id=".$login_usr_id );

$args = array(
  'base' => @add_query_arg('page','%#%'),
  'format' => '',
  'total' => ceil(sizeof($rows)/$results_per_page),
  'current' => $current,
  'show_all' => false,
  'type' => 'plain',
  );


  $start = ($current - 1) * $results_per_page;
  $end = $start + $results_per_page;
  $end = (sizeof($rows) < $end) ? sizeof($rows) : $end;
  echo '<br />';

		
$rows_data = $wpdb->get_results("SELECT * FROM " . $table ." Where user_id=".$login_usr_id." LIMIT $start, $results_per_page");
		?>
		<div class="tbl-row thead">
	        <div class="col cl-user"><?php echo __( 'User', 'templatic-admin' );?></div>
	        <div class="col cl-price-pkg"><?php echo __( 'Price Package', 'templatic-admin' );?></div>
	        <div class="col cl-pay-mtd"><?php echo __( 'Payment Method', 'templatic-admin' );?></div>
	        <div class="col cl-amt"><?php echo __( 'Amount', 'templatic-admin' );?></div>
	        <div class="col cl-paid-on"><?php echo __( 'Paid On', 'templatic-admin' );?></div>
	        <div class="col cl-exp-date"><?php echo __( 'Exp. Date', 'templatic-admin' );?></div>
	        <div class="col cl-status"><?php echo __( 'Status', 'templatic-admin' );?></div>
    	</div>
	<?php
	if(!empty($rows_data))
	{
		foreach($rows_data as $p):
				
			$post_package = get_post( $p->package_id );
			$userdata = get_userdata( $p->user_id );
				
			$validat_no=get_post_meta( $p->package_id, 'billing_num',true);
			$validat_dur=get_post_meta( $p->package_id, 'billing_per',true);
			$pdate=strtotime($p->payment_date);
			$exp='';
			if($validat_dur=='M')
			{
				$exp=date("Y-m-d", strtotime("+".$validat_no." month", $pdate));
			}
			if($validat_dur=='D')
			{
				$exp=date("Y-m-d", strtotime("+".$validat_no." day", $pdate));
			}
			if($validat_dur=='Y')
			{
				$exp=date("Y-m-d", strtotime("+".$validat_no." year", $pdate));
			}
		?>    
			<div class="tbl-row">
				<div class="col cl-user"><?php echo $userdata->user_nicename;?></div>
				<div class="col cl-price-pkg"><?php echo $post_package->post_title;?></div>
				<div class="col cl-pay-mtd"><?php echo $p->payment_method;?></div>
				<div class="col cl-amt"><?php echo $p->payable_amt;?></div>
				<div class="col cl-paid-on"><?php echo date('M d Y',strtotime($p->payment_date)); ?></div>
				<div class="col cl-exp-date"><?php echo date('M d Y',strtotime($exp)); ?></div>
				<div class="col cl-status"><?php echo tmpl_get_transaction_status( $p->trans_id, $p->post_id );?></div>
			</div>
	  <?php
		endforeach;
	}
	else
	{
		echo __( 'No Transaction found. ','templatic-admin' );
	}
	?>
    <div class="trans-pagin">
    <?php
	echo paginate_links($args);
 ?>
 </div>