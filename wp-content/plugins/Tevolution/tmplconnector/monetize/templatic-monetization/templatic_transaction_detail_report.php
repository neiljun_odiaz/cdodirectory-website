<?php
/**
 * Transaction detail page for backend.
 *
 * @package wordpress.
 * @subpackage Tevolution
 */

if ( isset( $_REQUEST['trans_id'] ) ) {
	$orderId = intval( $_REQUEST['trans_id'] );
}

if ( isset( $_REQUEST['submit'] ) && '' != $_REQUEST['submit'] ) {
	do_action( 'tevolution_transaction_mail' );
}
global $wpdb;
$transaction_db_table_name = $wpdb ->prefix . 'transactions';
$ordersql = "select * from $transaction_db_table_name where trans_id=\"$orderId\"";
$orderinfoObj = $wpdb->get_row( $ordersql );


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<style>
	.postbox h3, .stuffbox h3.transaction_title {
		padding: 0 10px;
		margin-bottom: 0;
	}
</style>
<div class="wrap">
	<div id="icon-edit" class="icon32 icon32-posts-post"><br></div>
	<h2><?php echo wp_kses_post( wp_unslash( TRANSACTION_REPORT_TEXT ) ); ?> <a title="Back to transaction list" class="add-new-h2" name="btnviewlisting" href="<?php echo esc_url( site_url() ) ?>/wp-admin/admin.php?page=transcation"><?php echo wp_kses_post( wp_unslash( BACK_TO_TRANSACTION_LINK ) ); ?></a>
	</h2>
	<p class="description"><?php echo esc_html__( 'Here you can view the transaction detail. ', 'templatic-admin' ); ?></p>
	<?php if ( isset( $_REQUEST['msg'] ) && 'success' == $_REQUEST['msg'] ) { ?>
	<div class="update-nag" style="text-align:left;">
		<?php echo wp_kses_post( wp_unslash( ORDER_STATUS_SAVE_MSG ) );?>
	</div>
	<?php }?>
	<div class="tevolution_normal">
		<div id="poststuff">
			<div class="postbox">
				<h3 class="transaction_title"><span><?php esc_html_e( 'Transaction Report', 'templatic' ); ?></span></h3>
				<div class="transaction_detail_page">
					<div class="order_frm">
						<h3 class="transaction_title"><?php esc_html_e( 'Transaction Detail', 'templatic' ); ?></h3>
						<div class="inside">
							<p class="stat"><?php echo wp_kses_post( wp_unslash( get_order_detailinfo_transaction_report( $orderId ) ) ); ?></p>
						</div>
					</div>
				</div>
				<?php
				/*if ( $orderinfoObj->post_id !=0) {*/
				$package_select_id = get_post_meta( $orderinfoObj ->post_id, 'package_select', true );
				if ( '' == $package_select_id ) {
					$package_select_id = $orderinfoObj ->package_id;
				}
				if ( $package_select_id ) {
					?>
					<div class="transaction_detail_frm">
						<h3><?php esc_html_e( 'Package Detail', 'templatic' ); ?></h3>
						<div class="inside">
							<p class="stat"><?php echo wp_kses_post( wp_unslash( get_order_detailinfo_price_package( $orderId ) ) ); ?></p>
						</div>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<?php if ( 0 == $orderinfoObj->payforpackage || 1 == $orderinfoObj->payforpackage ) : ?>
		<div id="poststuff">
			<div class="postbox">
				<h3 class="transaction_title">
					<span><?php
					$post_type = get_post( $orderinfoObj ->post_id );
					echo esc_html__( 'Transaction Information', 'templatic-admin' );
					?></span>
				</h3>

				<div class="inside">
					<p class="stat"><?php echo get_order_detailinfo_tableformat( $orderId,1 ); ?></p>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="tevolution_side">
		<div id="poststuff">
			<div class="postbox">
				<h3 class="transaction_title"><span><?php esc_html_e( 'Transaction Status', 'templatic' ); ?></span></h3>
				<div class="inside">
					<p class="stat">
						<form action="<?php echo esc_url( site_url( '/wp-admin/admin.php?page=transcation&amp;action=edit&amp;msg=success&amp;trans_id=' . $_GET['trans_id'] ) );?>" method="post">
							<div class="orderstatus_class">
								<input type="hidden" name="act" value="orderstatus">
								<select name="ostatus">
									<option value="0" <?php if ( 0 == $orderinfoObj ->status ) {?> selected="selected"<?php }?>><?php echo wp_kses_post( wp_unslash( PENDING_MONI ) );?></option>
									<option value="1" <?php if ( 1 == $orderinfoObj ->status ) {?> selected="selected"<?php }?>><?php echo wp_kses_post( wp_unslash( APPROVED_TEXT ) );?></option>
									<option value="2" <?php if ( 2 == $orderinfoObj ->status ) {?> selected="selected"<?php }?>><?php echo wp_kses_post( wp_unslash( ORDER_CANCEL_TEXT ) );?></option>
									<option value="3" <?php if ( 3 == $orderinfoObj ->status ) {?> selected="selected"<?php }?>><?php echo esc_html__( 'Delete', 'templatic-admin' );?></option>
								</select>
							</div>
							<div class="submit_orderstatus_class"><input type="submit" name="submit" value="<?php echo wp_kses_post( wp_unslash( ORDER_UPDATE_TITLE ) ); ?>" class="button-primary" ></div>
							<input type="hidden" name="update_transaction_status" id="update_transaction_status" value="<?php echo intval( $orderinfoObj->post_id ); ?>" />
						</form>
					</p>
				</div>
			</div>
		</div>
		<div id="poststuff">
			<div class="postbox">
				<h3><span><?php esc_html_e( 'User Information', 'templatic' ); ?></span></h3>
				<div class="inside">
					<?php echo wp_kses_post( wp_unslash( get_order_user_info( $orderId ) ) ); ?>
				</div>
			</div>
		</div>
	</div>
</div>
