<?php
/**
 * File is called after if cancel the payment
 */

global $current_user, $wpdb;


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$transaction_tabel = $wpdb->prefix . 'transactions';
define( 'PAY_CANCELATION_TITLE', __( 'Payment Cancellation', 'templatic' ) );
define( 'PAY_FAST_CANCEL_MSG', __( 'Payment has been cancelled successfully. The post submitted by you has not been published. ', 'templatic' ) );


/* added limit to query for query performance */

$post_author = $wpdb->get_row( $wpdb->prepare( 'select max( trans_id ) as trans_id,status,post_id,user_id from ' . $wpdb->prefix . 'transactions LIMIT 0,1' ) );

$postid = wp_kses_post( @$post_author->post_id );
$trans_id = wp_kses_post( @$post_author->trans_id );
$post_author = wp_kses_post( @$post_author->user_id );
get_header();
?>
<div id="content">
	<div class="hfeed">
		<h1 class="page_head"><?php echo PAY_CANCELATION_TITLE;?></h1>
		<?php
		if ( $postid && $post_author == $current_user->ID ) {
			if ( '' != @$postid ) {
				/*	Update post status	*/
				$my_post = array();
				$my_post['ID'] = $postid;
				$my_post['post_status'] = 'draft';
				wp_update_post( $my_post );

				/*	Update transaction status	*/
				if ( '' != $trans_id ) {
					$sql_status_update = $wpdb->query( "update $transaction_tabel set status = 2 where trans_id= {$trans_id} " );
				}
			}
			?>
			<h4><?php echo PAY_FAST_CANCEL_MSG; ?></h4> 
			<?php
		}
		?>
	</div>
</div> <!-- content #end -->
<?php
get_sidebar( 'primary' );
get_footer();

?>
