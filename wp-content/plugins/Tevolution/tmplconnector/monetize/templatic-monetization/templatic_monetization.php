<?php
/**
 * Show monetization html includes transaction and monetize files for backend settings.
 *
 * @package wordpress.
 * @subpackage Tevolution
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="wrap">
	<div id="icon-edit" class="icon32"><br></div>
	<h2><?php echo esc_html__( 'Monetization', 'templatic-admin' );?></h2>
	<p class="tevolution_desc"><?php echo esc_html__( 'Tevolution provides a lot of options using which you can monetize your site. You can charge your users for submitting their listing in different ways using price packages, we have a wide range of payment gateways using which you can charge your users. What&acute;s more? You can even add discount coupon codes to give seasonal discount to your users. ', 'templatic-admin' ); ?></p>
	<?php if ( @$message ) {?>
	<div class="updated fade below-h2" id="message" style="padding:5px; font-size:12px; width:47%" >
		<?php echo wp_kses_post( wp_unslash( $message ) );?>
	</div>
	<?php }?>
	<div id="icon-options-general" class="icon32"><br></div>
	<h2 class="nav-tab-wrapper">
		<?php
		$tab = '';
		if ( isset( $_REQUEST['tab'] ) ) {
			$tab = wp_kses_post( wp_unslash( $_REQUEST['tab'] ) );
		}
		$class = ' nav-tab-active'; ?>

		<a id="packages_settings" class='nav-tab<?php if ( 'packages' == $tab || '' == $tab ) { echo wp_kses_post( wp_unslash( $class ) );} ?>' href='?page=monetization&tab=packages'><?php echo esc_html__( 'Price Packages', 'templatic-admin' ); ?> </a>
		<a id="currency_settings" class='nav-tab<?php if ( 'currency_settings' == $tab ) { echo wp_kses_post( wp_unslash( $class ) );} ?>' href='?page=monetization&tab=currency_settings'><?php echo esc_html__( 'Currency', 'templatic-admin' ); ?> </a>
		<a id="payment_options_settings" class='nav-tab<?php if ( 'payment_options' == $tab ) { echo wp_kses_post( wp_unslash( $class ) );} ?>' href='?page=monetization&tab=payment_options'><?php echo esc_html__( 'Payment Gateways', 'templatic-admin' ); ?> </a>

		<?php
		/* add additional tabs in monetization section */
		do_action( 'templatic_monetizations_tabs', $tab, $class ); ?>
	</h2>
	<?php
	$monetization_tabs = apply_filters( 'tmpl_monetization_tabs', array( 'payment_options', 'packages' ) );

	if ( 'payment_options' == $tab ) {
		/* to fetch current installed payment add-ons */
		payment_option_plugin_function();
	} elseif ( 'currency_settings' == $tab ) {
		/* get the currency settings */
		tmpl_currency_settings();
	} elseif ( '' != $tab && ! in_array( $tab, $monetization_tabs ) ) {
		do_action( 'monetization_tabs_content' );

	} elseif ( isset( $_REQUEST['tab'] ) && ( 'payment_options' != $_REQUEST['tab'] && 'manage_coupon' != $_REQUEST['tab'] && 'packages' != $_REQUEST['tab'] ) ) {

		do_action( 'templatic_monetizations_tab_content' );

	} else {
		if ( (isset( $_REQUEST['action'] ) && 'add_package' == $_REQUEST['action'] ) || (isset( $_REQUEST['action'] ) && 'edit' == $_REQUEST['action'] ) ) {
			include( TEMPL_MONETIZATION_PATH . 'add_price_packages.php' );
		} else {
			if ( 'packages' == $tab || '' == $tab ) {
				include( TEMPL_MONETIZATION_PATH . 'price_packages_list.php' ); }
		}
	}
	?>
</div>
