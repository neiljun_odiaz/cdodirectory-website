<?php
/**
 * Header Section.
 *
 * @package Wordpress
 * @subpackage Tevolution
 */
	?>

<div class="wrap tevolution-table">
	<div class="icon32" id="icon-index"><br/></div>
	<h2><?php echo esc_html__( 'Tevolution', 'templatic-admin' );
	tevolution_version();
		?></h2>
		<?php

		if ( ! defined( 'ABSPATH' ) ) {
			exit;
		}

		if ( isset( $_REQUEST['tab'] ) ) {
			$tab = wp_kses_post( wp_unslash( $_REQUEST['tab'] ) );
		}else{
			$tab ='';
		}
		switch ( $tab ) {
			case 'overview':
				$sclass = '';
				$title = __( 'Overview', 'templatic-admin' );
				$oclass = 'nav-tab-active';
				$eclass = '';
				$pclass = '';
				$class = 'tevolution_setup_boxes';
			break;
			case 'setup-steps':
				$sclass = 'nav-tab-active';
				$title = __( 'Setup steps', 'templatic-admin' );
				$oclass = '';
				$eclass = '';
				$pclass = '';
				$class = 'tevolution_setup_boxes';
			break;
			case 'extend':
				$eclass = 'nav-tab-active';
				$title = __( 'Extend', 'templatic-admin' );
				$eaclass = 'active';
				$oclass = '';
				$pclass = '';
				$sclass = '';
				$class = 'tevolution_setup_boxes';
			break;
			case 'payment-gateways':
				$pclass = 'nav-tab-active';
				$title = __( 'Payment Gateways', 'templatic-admin' );
				$class = 'tevolution_setup_boxes';
				$sclass = '';
				$oclass = '';
				$eclass = '';
			break;
			case 'system_status':
				$syclass = 'nav-tab-active';
				$title = __( 'System Status', 'templatic-admin' );
				$class = 'tevolution_setup_boxes';
				$sclass = '';
				$oclass = '';
				$eclass = '';
			break;
			case '':
				$oclass = 'nav-tab-active';
				$title = __( 'Overview', 'templatic-admin' );
				$eclass = '';
				$class = 'tevolution_setup_boxes';
				$pclass = '';
				$sclass = '';
			break;
		} // End switch().
		?>
		<h2 class="nav-tab-wrapper">
			<a href="?page=templatic_system_menu&amp;tab=overview" class="nav-tab <?php echo wp_kses_post( wp_unslash( $oclass ) ); ?>"><?php echo esc_html__( 'Overview', 'templatic-admin' ); ?></a>
			<a href="?page=templatic_system_menu&amp;tab=payment-gateways" class="nav-tab <?php echo wp_kses_post( wp_unslash( $pclass ) ); ?>"><?php echo esc_html__( 'Payment gateways', 'templatic-admin' ); ?></a>
			<a href="?page=templatic_system_menu&amp;tab=system_status" class="nav-tab <?php echo wp_kses_post( wp_unslash( $syclass ) ); ?>"><?php echo esc_html__( 'System Status', 'templatic-admin' ); ?></a>
		</h2>
		<?php do_action( 'tevolution_plugin_list' ); ?>
		<div id="tevolution_bundled_boxes" class="<?php echo wp_kses_post( wp_unslash( $class ) ); ?>">
