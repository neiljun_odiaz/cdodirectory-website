<?php
/**
 * Edit profile form and save function.
 *
 * @package wordpress.
 * @subpackage Tevolution
 */

global $current_user, $wpdb;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/* save user data while editing his/her profile */
if ( isset( $_POST['action'] ) && 'user_profile' == $_POST['action'] ) {
	$user_id = $current_user->ID;
	if ( isset( $_POST['user_id'] ) && $user_id == $_POST['user_id'] ) {
		if ( isset( $_POST['user_email'] ) ) {
			$user_email = sanitize_email( $_POST['user_email'] );
		}
		if ( isset( $_POST['user_fname'] ) ) {
			$userName = sanitize_text_field( $_POST['user_fname'] );
		}
		if ( isset( $_POST['user_website'] ) ) {
			$user_website = sanitize_text_field( $_POST['user_website'] );
		}

		if ( isset( $_POST['profile_photo'] ) ) {
			$profile_photo = $_POST['profile_photo'];
		}
		$user_data = array(
			'ID' => $user_id,
			'user_email'  => $user_email,
			'display_name' => ( isset( $_POST['display_name'] ) ) ? sanitize_text_field( $_POST['display_name'] ) : '',
			'user_url'   => $user_website,
			);
		wp_update_user( $user_data );
		global $form_fields_usermeta;
		/* Fetch the user custom fields */
		$form_fields_usermeta = fetch_user_custom_fields();
		foreach ( $form_fields_usermeta as $fkey => $fval ) {
			if ( isset( $_POST[ "$fkey" ] ) ) {
				$fldkey = "$fkey";
				$fldkey = wp_kses_post( wp_unslash( $_POST[ "$fkey" ] ) );
			}
			update_user_meta( $user_id, $fkey, $fldkey ); /* User Custom Metadata Here */
		}
		if ( isset( $_POST['facebook'] ) ) {
			$user_facebook = wp_kses_post( wp_unslash( $_REQUEST['facebook'] ) );
		}
		if ( isset( $_POST['twitter'] ) ) {
			$user_twitter = wp_kses_post( wp_unslash( $_REQUEST['twitter'] ) );
		}
		if ( isset( $_POST['description'] ) ) {
			$description = wp_kses_post( wp_unslash( $_REQUEST['description'] ) );
		}
		update_user_meta( $user_id, 'user_facebook', $user_facebook );
		update_user_meta( $user_id, 'user_twitter', $user_twitter );
		update_user_meta( $user_id, 'description', trim( $description ) );
		update_user_meta( $user_id, 'profile_photo', trim( $profile_photo ) );
		$user_name = ( get_user_meta( $user_id, 'first_name', true ) ) ? get_user_meta( $user_id, 'first_name', true ): $userName;
		echo '<p class="success_msg"> ' . esc_html__( 'Hi ', 'templatic' ) . ' <a href="' . esc_url( get_author_posts_url( $user_id ) ) . '">' . wp_kses_post( wp_unslash( $user_name ) ) . '</a>, ' . esc_html__( 'Your profile is updated successfully.', 'templatic' ) . ' </p>';
	} // End if().
} // End if().

/* save changed password */
if ( isset( $_POST['action'] ) && 'changepwd' == $_POST['action'] ) {
	$user_id = $current_user->ID;
	if ( isset( $_POST['new_passwd'] ) && isset( $_POST['cnew_passwd'] ) && $_POST['new_passwd'] == $_POST['cnew_passwd'] ) {
		$user_data = array(
			'ID' => $user_id,
			'user_pass'  => wp_kses_post( $_POST['new_passwd'] ),
			);
		wp_update_user( $user_data );
		echo '<p class="success_msg"> ' . esc_html__( 'Password changed successfully. Please login with your new password. ', 'templatic' ) . ' </p>';
		$_SESSION['update_password'] = '1';
		wp_logout();
		wp_redirect( get_tevolution_login_permalink() );
	} else {
		echo '<p class="error_msg"> ' . esc_html__( PW_NO_MATCH_MSG, 'templatic' ) . ' </p>';
	}
}
global $submit_form_validation_id, $current_user;
$submit_form_validation_id = 'userform';
remove_filter( 'the_content', 'wpautop' , 12 );
?>

<div class="reg_cont_right">
	<!--user profile form -->
	<form name="userform" id="userform" action="<?php echo esc_url( get_permalink() ); ?>" method="post" enctype="multipart/form-data" class="clearfix" >
		<input type="hidden" name="action" value="user_profile" />
		<input type="hidden" name="user_id" value="<?php echo $current_user->ID; ?>" />
		<input type="hidden" name="user_email_already_exist" id="user_email_already_exist" value="1" />
		<input type="hidden" name="user_fname_already_exist" id="user_fname_already_exist" value="1" />
		<?php
		if ( $_POST ) {
			$user_email = sanitize_email( $_POST['user_email'] );
			$user_fname = sanitize_text_field( $_POST['user_fname'] );
		} else {
			$user_email = $current_user->user_email;
			$user_fname = $current_user->display_name;
		}
		do_action( 'templ_profile_form_start' );
		fetch_user_registration_fields( 'profile' );
		do_action( 'templ_profile_form_end' );
		?>

		<input type="submit" name="update" value="<?php esc_html_e( 'Update', 'templatic' );?>" class="b_registernow btn_update_profile update-btn" />
		<input type="button" name="Cancel" value="<?php esc_html_e( 'Cancel', 'templatic' ); ?>" class="b_registernow cancel-btn" onclick="window.location.href= '<?php echo esc_url( get_author_posts_url( $current_user->ID ) );?>'"/>
	</form>
	<!--end user profile form -->
	<!--Change password form -->
	<form name="chngpwdform" id="chngpwdform" action="<?php echo esc_url( get_permalink() ); ?>" method="post">
		<input type="hidden" name="action" value="changepwd"/>
		<input type="hidden" name="user_id" value="<?php echo intval( get_current_user_id() );?>" />
		<?php if ( isset( $message1 ) ) { ?>
		<div class="sucess_msg"> <?php esc_html_e( 'Password changed successfully. Please login with your new password. ', 'templatic' ); ?> </div>
		<?php } ?>
		<h3> <?php echo esc_html__( CHANGE_PW_TEXT, 'templatic' ); ?> </h3>
		<div class="form_row clearfix">
			<label><?php esc_html_e( 'New Password', 'templatic' ); ?> <span class="indicates">*</span></label>
			<input type="password" name="new_passwd" id="new_passwd" class="textfield" />
		</div>
		<div class="form_row clearfix ">
			<label><?php esc_html_e( 'Confirm New Password', 'templatic' ); ?> <span class="indicates">*</span></label>
			<input type="password" name="cnew_passwd" id="cnew_passwd" class="textfield" />
		</div>
		<input type="submit" name="update" value="<?php esc_html_e( 'Update', 'templatic' );?>" class="b_registernow update-btn" onclick="return chk_form_pw();" />
		<input type="button" name="Cancel" value="<?php esc_html_e( 'Cancel', 'templatic' ); ?>" class="b_registernow cancel-btn" onclick="window.location.href= '<?php echo esc_url( get_author_posts_url( $current_user->ID ) );?>'"/>
	</form>
	<!-- end change password form -->
</div>
<script type="text/javascript" async >
	/* <![CDATA[ */
	function chk_form_pw()
	{
		if ( document.getElementById( 'new_passwd' ).value == '' )
		{
			alert( "<?php esc_html_e( 'Please enter New Password', 'templatic' ) ?>");
			document.getElementById( 'new_passwd' ).focus();
			return false;
		}
		if ( document.getElementById( 'new_passwd' ).value.length < 4 )
		{
			alert( "<?php esc_html_e( 'Please enter New Password minimum 5 chars', 'templatic' ) ?>");
			document.getElementById( 'new_passwd' ).focus();
			return false;
		}
		if ( document.getElementById( 'cnew_passwd' ).value == '' )
		{
			alert( "<?php esc_html_e( 'Please enter Confirm New Password', 'templatic' ) ?>");
			document.getElementById( 'cnew_passwd' ).focus();
			return false;
		}
		if ( document.getElementById( 'cnew_passwd' ).value.length < 4 )
		{
			alert( "<?php esc_html_e( 'Please enter Confirm New Password minimum 5 chars', 'templatic' ) ?>");
			document.getElementById( 'cnew_passwd' ).focus();
			return false;
		}
		if ( document.getElementById( 'new_passwd' ).value != document.getElementById( 'cnew_passwd' ).value)
		{
			alert( "<?php esc_html_e( 'New Password and Confirm New Password should be same', 'templatic' ) ?>");
			document.getElementById( 'cnew_passwd' ).focus();
			return false;
		}
	}
	/* ]]> */
</script>
<?php
wp_register_script( 'drag_drop_media_upload_scripts', TEVOLUTION_PAGE_TEMPLATES_URL . 'js/jquery.uploadfile.js', array( 'jquery' ),false );
wp_enqueue_script( 'drag_drop_media_upload_scripts' );
if ( get_option( 'users_can_register' ) ) {
	include_once( TT_REGISTRATION_FOLDER_PATH . 'registration_validation.php' );
}
