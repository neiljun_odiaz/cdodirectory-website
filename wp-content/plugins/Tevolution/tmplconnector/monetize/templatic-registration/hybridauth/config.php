<?php

/**
* HybridAuth
* //hybridauth.sourceforge.net | //github.com/hybridauth/hybridauth
* (c) 2009-2015, HybridAuth authors | //hybridauth.sourceforge.net/licenses.html
*/
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: //hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

$tmpdata = get_option( 'templatic_settings' );
return
array(
	'base_url' => plugin_dir_url( __FILE__ ),
	'providers' => array(
// openid providers

		'Google' => array(
			'enabled' => true,
			'keys'  => array(
				'id' => $tmpdata['google_key'],
				'secret' => $tmpdata['google_secret_key'],
			),
			),
		'Facebook' => array(
			'enabled' => true,
			'keys'  => array(
				'id' => $tmpdata['facebook_key'],
				'secret' => $tmpdata['facebook_secret_key'],
			),
			'trustForwarded' => false,
			),
		'Twitter' => array(
			'enabled' => true,
			'keys'  => array(
				'key' => $tmpdata['twitter_key'],
				'secret' => $tmpdata['twitter_secret_key'],
			),
			'includeEmail' => false,
			),

		),
// If you want to enable logging, set 'debug_mode' to true.
// You can also set it to
// - "error" To log only error messages. Useful in production
// - "info" To log info and error messages (ignore debug messages)
	'debug_mode' => false,
// Path to file writable by the web server. Required if 'debug_mode' is not false
	'debug_file' => '',
);
