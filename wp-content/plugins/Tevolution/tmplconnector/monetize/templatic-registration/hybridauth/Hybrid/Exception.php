<?php

/* !
 * HybridAuth
 * //hybridauth.sourceforge.net | //github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | //hybridauth.sourceforge.net/licenses.html
 */

/**
 * Exception implementation
 *
 * The base Exception is extended to allow applications to handle exceptions from hybrid auth
 * separately from general exceptions.
 */
class Hybrid_Exception extends Exception {

}
