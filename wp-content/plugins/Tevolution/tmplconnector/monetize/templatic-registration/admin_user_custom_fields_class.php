<?php
/**
 * Class to fetch the user custom fileds listing for backend.
 *
 * @package Wordpress.
 * @subpackage Tevolution
 */

global $pagenow;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( 'admin.php' == $pagenow && ( isset( $_REQUEST['page'] ) && 'custom_setup' == $_REQUEST['page'] && isset( $_REQUEST['ctab'] ) && 'user_custom_fields' == $_REQUEST['ctab'] ) ) {
	if ( ! class_exists( 'Tmpl_WP_List_Table' ) ) {
		include_once( WP_PLUGIN_DIR . '/Tevolution/templatic.php' );
	}
	/**
	 * Show user custom fields.
	 */
	class wp_list_custom_user_field extends Tmpl_WP_List_Table {
		/**
		 * FETCH ALL THE DATA AND STORE THEM IN AN ARRAY *****
		 * Call a function that will return all the data in an array and we will assign that result to a variable $custom_user_field_data. FIRST OF ALL WE WILL FETCH DATA FROM POST META TABLE STORE THEM
		 * IN AN ARRAY $custom_user_field_data.
		 *
		 * @param integer $post_id 			Post id.
		 * @param string  $post_title 		Post Title.
		 * @param string  $post_status 		Post Status.
		 * @param string  $post_name 		Post name.
		 */
		function fetch_custom_user_fields_meta_data( $post_id = '', $post_title = '', $post_status = '', $post_name = '' ) {
			$ctype 	 = get_post_meta( $post_id, 'ctype', true );
			$sort_order = get_post_meta( $post_id, 'sort_order', true );
			$meta_data = array(
				'ID'			 => $post_id,
				'title'		 => '<strong><a href="' . site_url() . '/wp-admin/admin.php?page=custom_setup&ctab=user_custom_fields&action=addnew&cf=' . $post_id . '">' . $post_title . '</a></strong><input type="hidden" name="user_field_sort[]" value="' . $post_id . '">',
				'type' 		 => $ctype,
				'variable_name' => $post_name,
				'active' 		 => $post_status,
				'display_order' => $sort_order,
					);
					return $meta_data;
		}
		/**
		 * Fetch all the custom user fields.
		 */
		function custom_user_fields() {
			global $post;
			$package_data = array();
			remove_all_actions( 'posts_where' );
			$paged  = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
			if ( isset( $_POST['s'] ) && '' != $_POST['s'] ) {
				$search_key = wp_kses_post( wp_unslash( $_POST['s'] ) );
				$args = array(
				'post_type' 		=> 'custom_user_field',
				'posts_per_page' 	=> $per_page,
				'post_status' 		=> array( 'publish', 'draft' ),
				'paged' 			=> $paged,
				's'				=> $search_key,
				'meta_key' => 'sort_order',
				'orderby' => 'meta_value_num',
				'meta_value_num' => 'sort_order',
				'order' => 'ASC',
						);
			} else {
				$args = array(
				'post_type' 		=> 'custom_user_field',
				'posts_per_page' 	=> -1,
				'post_status'		=> array( 'publish', 'draft' ),
				'paged' 			=> $paged,
				'meta_key' => 'sort_order',
				'orderby' => 'meta_value_num',
				'meta_value_num' => 'sort_order',
				'order' => 'ASC',
				);
			}
			$post_query = null;
			$post_query = new WP_Query( $args );
			while ( $post_query->have_posts() ) : $post_query->the_post();
				$custom_user_field_data[] = $this->fetch_custom_user_fields_meta_data( $post->ID, $post->post_title, $post->post_status, $post->post_name );
			endwhile;
			wp_reset_query();
			return $custom_user_field_data;
		}
		/**
		 * EOF - FETCH PACKAGE DATA.
		 */

		/**
		 * DEFINE THE COLUMNS FOR THE TABLE.
		 */
		function get_columns() {
			$columns = array(
			'cb'      => '<input type="checkbox" />',
			'title'     => __( 'Title', 'templatic-admin' ),
			'type'     => __( 'Type', 'templatic-admin' ),
			'variable_name' => __( 'Variable Name', 'templatic-admin' ),
			'active'    => __( 'Active', 'templatic-admin' ),
			/*'display_order' => __( 'Display Order', 'templatic' )*/
					);
					return $columns;
		}
		/**
		 * Bulk Action.
		 */
		function process_bulk_action() {
			/*Detect when a bulk action is being triggered...*/
			
			
			if ( !empty( $_REQUEST['cf'] ) && 'delete' === $this->current_action() ) {
				$cids = $_REQUEST['cf'];
				
				
				foreach ( $cids as $cid ) {
					wp_delete_post( $cid );
				}
				$url = site_url() . '/wp-admin/admin.php';
				echo '
				<input type="hidden" value="custom_setup" name="page"><input type="hidden" value="user_custom_fields" name="ctab"><input type="hidden" value="delsuccess" name="usermetamsg">
				<script>document.register_custom_fields.submit();</script>';
				exit;
			}
		}
		/**
		 * Create prepare item.
		 */
		function prepare_items() {
			if ( $this->get_items_per_page( 'taxonomy_per_page' ) == '' ) {
				$per_page = $this->get_items_per_page( 'taxonomy_per_page', 50 );
			}else{
				$per_page = 10;
			}
			$columns = $this->get_columns(); /* CALL FUNCTION TO GET THE COLUMNS */
			$hidden = array();
			$sortable = array();
			$sortable = $this->get_sortable_columns(); /* GET THE SORTABLE COLUMNS */
			$this->_column_headers = array( $columns, $hidden, $sortable );
			$this->process_bulk_action();
			/**
			 *FUNCTION TO PROCESS THE BULK ACTIONS.
			 */
			/*$action = $this->current_action();*/
			$data = $this->custom_user_fields();
			/**
			 * RETIRIVE THE USER FIELDS DATA.
			 */
			/**
			  * FUNCTION THAT SORTS THE COLUMNS.
			  *
			  * @param string $a 		Order by.
			  * @param string $b 		Order by.
			  */
			function usort_reorder( $a, $b ) {
				$orderby = ( ! empty( $_REQUEST['orderby'] ) )  ? wp_kses_post( wp_unslash( $_REQUEST['orderby'] ) ) : 'title'; /*If no sort, default to title*/
				$order = ( ! empty( $_REQUEST['order'] ) )  ? wp_kses_post( wp_unslash( $_REQUEST['order'] ) ) : 'asc'; /*If no order, default to asc*/
				$result = strcmp( $a[ $orderby ], $b[ $orderby ] ); /*Determine sort order*/
				return ( 'asc' === $order ) ? $result : -$result; /*Send final sort direction to usort*/
			}
			if ( is_array( $data ) && isset( $_REQUEST['orderby'] ) ) {
				usort( $data, 'usort_reorder' );
			}
			$current_page = $this->get_pagenum(); /* GET THE PAGINATION */
			$total_items = count( $data ); /* CALCULATE THE TOTAL ITEMS */
			if ( is_array( $data ) ) {
				$this->found_data = array_slice( $data,( ( $current_page -1 ) * $per_page ), $per_page );
			} // End if().

			$this->items = $this->found_data; /* ASSIGN SORTED DATA TO ITEMS TO BE USED ELSEWHERE IN CLASS */
			$this->set_pagination_args( array(
				'total_items' => $total_items,
				'per_page'  => $per_page,
				)
			);
		}

		/**
		 * To avoid the need to create a method for each column there is column_default that will process any column for which no special method is defined.
		 *
		 * @param array  $item 			Item Array.
		 * @param string $column_name 	Column Name.
		 */
		function column_default( $item, $column_name ) {
			switch ( $column_name ) {
				case 'ID' :
				case 'title' :
				case 'type' :
				case 'variable_name' :
				case 'display_order' :
				return $item[ $column_name ];
				case 'active' :
					$active = ( 'publish' == $item[ $column_name ] )? 'Yes':'No';
				return $active;
				default :
				return print_r( $item, true ); /*Show the whole array for troubleshooting purposes*/
			}
		}

		/**
		 * DEFINE THE COLUMNS TO BE SORTED.
		 */
		function get_sortable_columns() {
			$sortable_columns = array(
			'title' => array( 'title', true ),
			);
			return $sortable_columns;
		}
		/**
		 * Add column title.
		 *
		 * @param array $item 		User custom field array.
		 */
		function column_title( $item ) {
			if ( isset( $_REQUEST['page'] ) ) {
				$actions = array(
				'edit' => sprintf( '<a href="?page=%s&ctab=%s&action=%s&cf=%s">' . esc_html__( 'Edit', 'templatic-admin' ) . '</a>', wp_kses_post( wp_unslash( $_REQUEST['page'] ) ), 'user_custom_fields', 'addnew', wp_kses_post( wp_unslash( $item['ID'] ) ) ),
				'delete' => sprintf( '<a href="?page=%s&ctab=%s&action_del=%s&cf[]=%s" onclick="return confirm(\'' . esc_html__( 'Are you sure for deleteing custom field?', 'templatic-admin' ) . '\' )">' . esC_html__( 'Delete', 'templatic-admin' ) . '</a>', wp_kses_post( wp_unslash( $_REQUEST['page'] ) ), 'user_custom_fields', 'delete', wp_kses_post( wp_unslash( $item['ID'] ) ) ),
				);
			}
			return sprintf( '%1$s %2$s', $item['title'], $this->row_actions( $actions , $always_visible = false ) );
		}
		/**
		 * Add Bulk action.
		 */
		function get_bulk_actions() {
			$actions = array(
			'delete' => 'Delete',
			);
			return $actions;
		}
		/**
		 * Add column title.
		 *
		 * @param array $item 		User custom field array.
		 */
		function column_cb( $item ) {
			return sprintf(
				'<input type="checkbox" name="cf[]" value="%s" />', $item['ID']
			);
		}
	}
} // End if().


