<?php
/**
 * Function to register new user EOF.
 * Generate and email the password to the existing users.
 *
 * @package wordpress.
 * @subpackage Tevolution
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Generate and email the password to the existing users.
 */
function templatic_widget_retrieve_password() {
	global $wpdb;
	$errors = new WP_Error();
	if ( isset( $_POST['user_login'] ) ) {
		$login = trim( wp_kses_post( wp_unslash( $_POST['user_login'] ) ) );
	}
	if ( isset( $_POST['user_login'] ) && empty( $_POST['user_login'] ) ) {
		$errors->add( 'empty_username', __( '<strong>ERROR</strong>: Enter a username or e-mail address. ', 'templatic' ) );
	}
	if ( isset( $_POST['user_login'] ) && strpos( wp_kses_post( wp_unslash( $_POST['user_login'] ) ), '@' ) ) {
		$user_data = get_user_by( 'email', $login );
		if ( empty( $user_data ) ) {
			$errors->add( 'invalid_email', __( '<strong>ERROR</strong>: There is no user registered with that email address. ', 'templatic' ) );
		}
	} else {
		$user_data = get_user_by( 'email', $login );
	}
	if ( $errors->get_error_code() ) {
		return $errors;
	}
	if ( ! $user_data ) {
		$errors->add( 'invalidcombo', __( '<strong>ERROR</strong>: Incorrect username or e-mail. ', 'templatic' ) );
		return $errors;
	}
	/* redefining user_login ensures we return the right case in the email */
	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;
	if ( isset( $_POST['widget_user_remail'] ) ) {
		$user_email = wp_kses_post( wp_unslash( $_POST['widget_user_remail'] ) );
	}
	if ( isset( $_POST['user_login'] ) ) {
		$user_login = wp_kses_post( wp_unslash( $_POST['user_login'] ) );
	}
	$user = $wpdb->get_row( "SELECT * FROM $wpdb->users WHERE user_login like \"$user_login\" or user_email like \"$user_login\"" );
	if ( empty( $user ) ) {
		return new WP_Error( 'invalid_key', __( 'Invalid key', 'templatic' ) );
	}
	$new_pass = wp_generate_password( 12,false );
	wp_set_password( $new_pass, $user->ID );
	update_user_meta( $user->ID, 'default_password_nag', true ); /*Set up the Password change nag.*/
	$user_email = $user_data->user_email;
	$user_name = $user_data->user_nicename;
	$fromEmail = get_site_emailId_plugin();
	$fromEmailName = get_site_emailName_plugin();
	$tmpdata = get_option( 'templatic_settings' );
	$email_subject = @stripslashes( $tmpdata['reset_password_subject'] );
	if ( '' == @$email_subject ) {
		$email_subject = __( '[#site_title#] Your new password', 'templatic' );
	}
	$email_content = @stripslashes( $tmpdata['reset_password_content'] );
	if ( '' == @$email_content ) {
		$email_content = __( '<p>Hi [#to_name#],</p><p>Here is the new password you have requested for your account [#user_email#].</p><p> Login URL: [#login_url#] </p><p>User name: [#user_login#]</p> <p> Password: [#user_password#]</p><p>You may change this password in your profile once you login with the new password above.</p><p>Thanks <br/> [#site_title#] </p>', 'templatic-admin' );
	}
	$title = sprintf( '[%s]' . __( ' Your new password', 'templatic' ), get_option( 'blogname' ) );

	$email_subject_array = array( '[#site_title#]' );
	$email_subject_replace_array = array( get_option( 'blogname' ) );
	$email_subject = str_replace( $email_subject_array, $email_subject_replace_array, $email_subject );

	$site_name = stripslashes( get_option( 'blogname' ) );
	$admin_email = get_option( 'admin_email' );

	$store_login = '';
	$store_login_link = '';
	if ( function_exists( 'get_tevolution_login_permalink' ) ) {
		$store_login = '<a href="' . get_tevolution_login_permalink() . '">' . __( 'Click Login', 'templatic' ) . '</a>';
		$store_login_link = get_tevolution_login_permalink();
	}

	$login_url = "<a href='" . get_tevolution_login_permalink() . "'>" . __( 'Login', 'templatic' ) . '</a>';
	$search_array_content = array( '[#to_name#]', '[#user_email#]', '[#login_url#]', '[#user_login#]', '[#user_password#]', '[#site_title#]', '[#site_name#]', '[#admin_email#]', '[#site_login_url#]', '[#site_login_url_link#]' );

	$replace_array_content = array( $user_name, $user_data->user_email, $login_url, $user->user_login, $new_pass,get_option( 'blogname' ), $site_name, $admin_email, $store_login, $store_login_link );
	$email_content = str_replace( $search_array_content, $replace_array_content, $email_content );
	templ_send_email( $fromEmail, $fromEmailName, $user_email, $user_name, $email_subject, $email_content, $extra = '' );/*/forgot password email*/
	return true;
}
/**-- Function for retrieve password EOF --**/
add_action( 'after_setup_theme', 'tmpl_custom_login' );
/**
 * Go inside when user login.
 */
function tmpl_custom_login() {
	if ( isset( $_REQUEST['widgetptype'] ) == 'login' ) {
		include_once( ABSPATH . 'wp-load.php' );
		include_once( ABSPATH . 'wp-includes/registration.php' );
		$secure_cookie = '';
		if ( ! empty( $_POST['log'] ) && ! force_ssl_admin() ) {
			if ( isset( $_POST['log'] ) ) {
				$user_name = sanitize_user( $_POST['log'] );
			}
			if ( $user = get_userdata( $user_name ) ) {
				if ( get_user_option( 'use_ssl', $user->ID ) ) {
					$secure_cookie = true;
					force_ssl_admin( true );
				}
			}
		} if ( isset( $_REQUEST['redirect_to'] ) && '' == wp_kses_post( wp_unslash( $_REQUEST['redirect_to'] ) ) && isset( $user ) ) {
			$_REQUEST['redirect_to'] = site_url() . '/author/' . $user->user_nicename;
		}
		if ( isset( $_REQUEST['redirect_to'] ) ) {
			$redirect_to = wp_kses_post( wp_unslash( $_REQUEST['redirect_to'] ) );
			/* Redirect to https if user wants ssl */
			if ( $secure_cookie && false !== strpos( $redirect_to, 'wp-admin' ) ) {
				$redirect_to = preg_replace( '|^//|', 'https://', $redirect_to );
			}
		} else {
			$redirect_to = admin_url();
		}
		if ( ! $secure_cookie && is_ssl() && ! force_ssl_admin() && ( 0 !== strpos( $redirect_to, 'https' ) ) && ( 0 === strpos( $redirect_to, 'http' ) ) ) {
			$secure_cookie = false;
		}
		$creds = array();
		if ( isset( $_POST['log'] ) ) {
			$creds['user_login'] = sanitize_text_field( $_POST['log'] );
		}
		if ( isset( $_POST['pwd'] ) ) {
			$creds['user_password'] = $_POST['pwd'];
		}
		$creds['remember'] = true;
		$user = wp_signon( $creds, $secure_cookie );
		$redirect_to = apply_filters( 'login_redirect', $redirect_to, isset( $_REQUEST['redirect_to'] ) ? wp_kses_post( wp_unslash( $_REQUEST['redirect_to'] ) ) : '', $user );

		if ( ! is_wp_error( $user ) ) {
			/* If the user can't edit posts, send them to their profile.*/
			if ( ! current_user_can( 'edit_posts' ) && ( empty( $redirect_to ) || 'wp-admin/' == $redirect_to || admin_url() == $redirect_to ) ) {
				$redirect_to = admin_url( 'profile.php' );
			}
			wp_safe_redirect( $redirect_to );
			exit();
		}
		$errors = $user;

		/* If cookies are disabled we can't log in even with a valid user+pass */
		if ( isset( $_POST['testcookie'] ) && empty( $_COOKIE[ TEST_COOKIE ] ) ) {
			$errors->add( 'test_cookie', __( "<strong>ERROR</strong>: Cookies are blocked or not supported by your browser. You must <a href='//www.google.com/cookies.html'>enable cookies</a> to use WordPress.", 'templatic' ) );
		}
		if ( ! is_wp_error( $user ) ) {
			wp_safe_redirect( $redirect_to );
			exit();
		}
	} // End if().
}
/**
 * Login Widget Class
 */
class loginwidget_plugin extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname' => 'Login Dashboard wizard',
			'description' => __( 'The widget shows account-related links to logged-in visitors. Visitors that are not logged-in will see a login form. Works best in sidebar areas. ', 'templatic-admin' ),
			);
		parent::__construct( 'widget_login', __( 'T &rarr; Login Box', 'templatic-admin' ), $widget_ops );
	}
	/**
	 * Prints the widget.
	 *
	 * @param var $args args.
	 * @param var $instance instance.
	 */
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		$title = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
		if ( isset( $_REQUEST['widgetptype'] ) && 'forgetpass' == $_REQUEST['widgetptype'] ) {
			$errors = templatic_widget_retrieve_password();
			if ( ! is_wp_error( $errors ) ) {
				$for_msg = __( 'Check your e-mail for the new password. ', 'templatic' );
			}
		}
		$login_page_id = get_option( 'tevolution_login' );
		$register_page_id = get_option( 'tevolution_register' );
		global $post;

		/*condition to check whether the page is login or not*/
		if ( $post->ID != $login_page_id && $post->ID != $register_page_id && get_post_meta( $post->ID, 'is_tevolution_submit_form', true ) != 1 || is_user_logged_in() ) {
			?>
			<div class="widget login_widget" id="login_widget">
				<?php
				global $current_user;
				if ( $current_user->ID && is_user_logged_in() ) { /* user loged in*/
					?>
					<h3 class="widget-title"><?php echo wp_kses_post( wp_unslash( $title ) );?></h3>
					<ul class="xoxo blogroll">
						<?php
						$authorlink = get_author_posts_url( $current_user->ID );
						echo '<li><a href="' . esc_url( get_author_posts_url( $current_user->ID ) ) . '">';
						esc_html_e( 'Dashboard', 'templatic' );
						echo '</a></li>';
						echo '<li><a href="' . esc_url( get_tevolution_profile_permalink() ) . '">';
						esc_html_e( 'Edit profile', 'templatic' );
						echo '</a></li>';
						echo '<li><a href="' . esc_url( get_tevolution_profile_permalink() ) . '#chngpwdform">';
						esc_html_e( 'Change password', 'templatic' );
						echo '</a></li>';
						$user_link = get_author_posts_url( $current_user->ID );
						if ( strstr( $user_link, '?' ) ) { $user_link = $user_link . '&list=favourite';
						} else {
							$user_link = $user_link . '?list=favourite';}
						do_action( 'tevolution_login_dashboard_content' );
						echo '<li><a href="' . esc_url( wp_logout_url( get_option( 'siteurl' ) . '/' ) ) . '">';
						esc_html_e( 'Logout', 'templatic' );
						echo '</a></li>';
						?>
					</ul>
					<?php
				} else { /* user not logend in*/
					if ( $title ) {
						echo '<h3 class="widget-title">' . wp_kses_post( wp_unslash( $title ) ) . '</h3>';
					}
					global $errors, $reg_msg ;
					if ( 'login' == wp_kses_post( wp_unslash( $_REQUEST['widgetptype'] ) ) ) {
						if ( is_object( $errors ) ) {
							$login_link = sprintf( esc_html__( '<a href="%s">Lost your password?</a>', 'templatic' ), wp_kses_post( wp_unslash( get_tevolution_login_permalink() ) ) );
							echo '<p class=\"error_msg\">';
							esc_html_e( 'The password you entered is incorrect. Please try again. ', 'templatic' );
							echo ' ' . wp_kses_post( wp_unslash( $login_link ) );
							echo '</p>';
						}
						$errors = new WP_Error();
					}
					?>

					<!-- Login form -->

					<div id="tmpl_login_frm">
						<?php echo do_shortcode( '[tevolution_login form_name="widget_login"]' ); ?>
					</div>

					<?php if ( get_option( 'users_can_register' ) ) { ?>
					<!-- Registration form -->
					<div id="tmpl_sign_up" style="display:none;">

						<?php echo do_shortcode( '[tevolution_register form_name="register_login_widget"]' ); ?>
						<?php include( TT_REGISTRATION_FOLDER_PATH . 'registration_validation.php' );?>
						<p><?php esc_html_e( 'Already have an account? ', 'templatic' ); ?><a href="javascript:void(0 )" class="widgets-link" id="tmpl-back-login"><?php esc_html_e( 'Sign in', 'templatic' );?></a></p>
					</div>
					<?php } ?>

					<?php
					if ( 'login' == wp_kses_post( wp_unslash( $_REQUEST['widgetptype'] ) ) ) {
						if ( $reg_msg ) {
							echo '<p class=\"error_msg\">' . wp_kses_post( wp_unslash( $reg_msg ) ) . '</p>';
						}
						if ( is_object( $errors ) ) {
							foreach ( $errors as $errorsObj ) {
								foreach ( $errorsObj as $key => $val ) {
									$val_count = count( $val );
									for ( $i = 0;$i < $val_count ; $i++ ) {
										echo '<p class=\"error_msg\">' . wp_kses_post( wp_unslash( $val[ $i ] ) ) . '</p>';
									}
								}
							}
						}
						$errors = new WP_Error();
					}
				} // End if().
				echo '</div>';
		} // End if().
	}
	/**
	 * Save the widget.
	 *
	 * @param var $new_instance instance.
	 * @param var $old_instance instance.
	 */
	function update( $new_instance, $old_instance ) {
			return $new_instance;
	}
	/**
	 * Widgetform in backend.
	 *
	 * @param var $instance instance.
	 */
	function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array(
				'title' => __( 'Dashboard', 'templatic' ),
				 )
			);
			$title = strip_tags( $instance['title'] );
			?>
			<p>
				<label for="<?php echo wp_kses_post( wp_unslash( $this->get_field_id( 'title' ) ) ); ?>">
					<?php echo esc_html__( 'Login Box Title', 'templatic' );?>: <input class="widefat" id="<?php echo wp_kses_post( wp_unslash( $this->get_field_id( 'title' ) ) ); ?>" name="<?php echo wp_kses_post( wp_unslash( $this->get_field_name( 'title' ) ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
				</label>
			</p>
			<?php
	}
}
	/**- function to add facebook login EOF -**/
	add_action( 'widgets_init', 'loginwidget_plugin_callback' );
	add_action( 'wp_footer', 'tmpl_loginwidget_script', 100 );

function loginwidget_plugin_callback(){
	return register_widget('loginwidget_plugin');
}	
/**
	 * Add show hide script for login widget.
	 */

function tmpl_loginwidget_script() {

		?>
	<script type="text/javascript" async >

		jQuery(document ).ready(function() {

			/* When click on links available in login box widget */

			jQuery( '#login_widget #tmpl-reg-link' ).click(function() {
				jQuery( '#login_widget #tmpl_sign_up' ).show();
				jQuery( '#login_widget #tmpl_login_frm' ).hide();
			});

			jQuery( '#login_widget #tmpl-back-login' ).click(function() {
				jQuery( '#login_widget #tmpl_sign_up' ).hide();
				jQuery( '#login_widget #tmpl_login_frm' ).show();
			});

			/* When click on links Login/reg pop ups */

			jQuery( '#tmpl_reg_login_container #tmpl-reg-link' ).click(function() {
				jQuery( '#tmpl_reg_login_container #tmpl_sign_up' ).show();
				jQuery( '#tmpl_reg_login_container #tmpl_login_frm' ).hide();
			});

			jQuery( '#tmpl_reg_login_container #tmpl-back-login' ).click(function() {
				jQuery( '#tmpl_reg_login_container #tmpl_sign_up' ).hide();
				jQuery( '#tmpl_reg_login_container #tmpl_login_frm' ).show();
			});

			jQuery( '#login_widget .lw_fpw_lnk' ).click(function() {
				if ( jQuery( '#login_widget #lostpassword_form' ).css( 'display' ) =='none' ) {
					jQuery( '#login_widget #lostpassword_form' ).show();
				} else {
					jQuery( '#login_widget #lostpassword_form' ).hide();
				}
				jQuery( '#login_widget #tmpl_sign_up' ).hide();
			});

		});
</script>
<?php
}

add_action( 'wp_head', 'add_to_fav_login_box' );

/**
 * Login registration pop up.
 */
function add_to_fav_login_box() {
	global $current_user;
	if ( '' == $current_user->ID && ! is_user_logged_in() ) { /* user loged in*/
		global $post;
		$login_page_id = get_option( 'tevolution_login' );
		$register_page_id = get_option( 'tevolution_register' );
		add_action( 'wp_footer', 'tmpl_add_login_reg_popup' );
	}
}

/**
 * Show address and login and registration pop up.
 */
function tmpl_add_login_reg_popup() {
	?>
<!-- Login form -->
<div id="tmpl_reg_login_container" class="reveal-modal tmpl_login_frm_data" data-reveal>
	<a href="javascript:;" class="modal_close"></a>
	<div id="tmpl_login_frm" >
		<?php echo do_shortcode( '[tevolution_login form_name="popup_login"]' ); ?>
	</div>

	<?php if ( get_option( 'users_can_register' ) ) { ?>
	<!-- Registration form -->
	<div id="tmpl_sign_up" style="display:none;">
		<?php echo do_shortcode( '[tevolution_register form_name="popup_register"]' ); ?>
		<?php include( TT_REGISTRATION_FOLDER_PATH . 'registration_validation.php' );?>
		<p><?php esc_html_e( 'Already have an account? ', 'templatic' ); ?><a href="javascript:void(0 )" class="widgets-link" id="tmpl-back-login"><?php esc_html_e( 'Sign in', 'templatic' );?></a></p>
	</div>
	<?php } ?>

</div>
<?php
}
