<?php
/**
 * For display rating add rating option in tevolution general settings.
 *
 * @package Wordpress
 * @subpackage Tevolution
 */

global $wp_query, $wpdb;


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/* Add action 'templatic_general_setting_data' for display rating*/
add_action( 'after_detail_page_setting', 'rating_setting_data', 12 );
/**
 * Add the rating options in tevolution.
 *
 * @param string $column 		Show rating Column.
 */
function rating_setting_data( $column ) {
	$tmpdata = get_option( 'templatic_settings' );
	if ( ! is_plugin_active( 'Templatic-MultiRating/multiple_rating.php' ) ) {
		?>
		<tr>
			<th><?php echo esc_html__( 'Ratings', 'templatic-admin' );?></th>
			<td>
				<div class="input-switch">
					<input id="rating_yes" type="checkbox" name="templatin_rating" value="yes" <?php if ( @$tmpdata['templatin_rating'] == 'yes' ) { echo 'checked';}?> />
					<label for="rating_yes">&nbsp;<?php echo esc_html__( 'Enable', 'templatic-admin' );?></label>
				</div>
				<p class="description"><?php echo sprintf( __( 'Allows visitors to star rate a listing when leaving comments. For more comprehensive ratings check out the "Multi Rating" add-on from %s', 'templatic-admin' ), '<a href="//templatic.com/directory-add-ons/star-rating-plugin-multirating/" title="Multi Rating" target="_blank">Here</a>' ); ?></p>
			</td>
			</tr>
			<tr>
			<th><?php echo esc_html__( 'Force Ratings', 'templatic-admin' );?></th>
			<td>
				<div class="input-switch">
					<input id="validate_rating" type="checkbox" name="validate_rating" value="yes" <?php if ( @$tmpdata['validate_rating'] == 'yes' ) { echo 'checked';}?> />
					<label for="validate_rating">&nbsp;<?php echo esc_html__( 'Enable', 'templatic-admin' );?></label>
				</div>
				<p class="description"><?php echo esc_html__( 'If enabled, visitors won&#39;t be able to submit a comment without entering a rating first. ', 'templatic-admin' ); ?></p>
			</td>
			</tr>
			<?php
	}
}

if ( file_exists( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-ratings/templatic_post_rating.php' ) ) {
	include_once( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-ratings/templatic_post_rating.php' );
}
if ( file_exists( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-ratings/language.php' ) ) {
	include( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-ratings/language.php' );
}
