<?php
/**
 * Rating html for comment form.
 *
 * @package Wordpress
 * @subpackage Tevolution
 */
global $post, $rating_image_on, $rating_image_off, $rating_table_name;
$tmpdata = get_option( 'templatic_settings' );


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<script src="<?php echo esc_url( plugin_dir_url( __FILE__ ) ) . 'post_rating.js';?>" type="text/javascript"></script>
<?php
echo '<ul>';
if ( current_theme_supports( 'rating_text' ) ) {
	_e( 'Rating ', 'templatic' );
}
for ( $i = 1;$i <= POSTRATINGS_MAX;$i++ ) {
	if ( $i == 1 ) {$rating_text = __( 'Terrible', 'templatic' );
	} elseif ( $i == 2 ) { $rating_text =  __( 'Poor', 'templatic' );}
	elseif ( $i == 3 ) {$rating_text = __( 'Average', 'templatic' );
	} elseif ( $i == 4 ) { $rating_text =  __( 'Very Good', 'templatic' );}
	else { $rating_text = __( 'Exceptional', 'templatic' );}
	echo '<li id="rating_' . intval( $post->ID ) . '_' . intval( $i ) . '" onmouseover="current_rating_star_on(\'' . intval( $post->ID ) . '\', \'' . intval( $i ) . '\', \'' . wp_kses_post( wp_unslash( $rating_text ) ) . '\' );" onmousedown="current_rating_star_off(\'' . intval( $post->ID ) . '\', \'' . intval( $i ) . '\' );" >';
	echo '<i class="fas fa-star rating-off" ></i>';
	echo '</li>';
}
echo '</ul>';
echo '<span class="rate-comment" id="ratings_' . intval( $post->ID ) . '_text" style="display:inline-table; position:relative; top:-2px; padding-left:10px; " ></span>';
echo '<input type="hidden" name="post_id" id="rating_post_id" value="' . intval( $post->ID ) . '" />';
echo '<input type="hidden" name="post_' . intval( $post->ID ) . '_rating" id="post_' . intval( $post->ID ) . '_rating" value="" />';
echo '<script type="text/javascript">current_rating_star_on(\'' . intval( $post->ID ) . '\', 0,\'0 ' . esc_html__( 'ratings', 'templatic' ) . '\' );</script>';
