<?php
/**
 * Function related to custim post type.
 *
 * @package Wordpress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// START of XSS code.
foreach ( array( 'mkey[]', 'post_type', 's', 'tevolution_sortby' ) as $vuln ) {
	isset( $_REQUEST[ $vuln ] ) and $_REQUEST[ $vuln ] = html_entity_decode( wp_kses_post( wp_unslash( $_REQUEST[ $vuln ] ) ) );
	isset( $_GET[ $vuln ] )   and $_GET[ $vuln ]   = html_entity_decode( wp_kses_post( wp_unslash( $_GET[ $vuln ] ) ) );
	isset( $_POST[ $vuln ] )  and $_POST[ $vuln ]  = html_entity_decode( wp_kses_post( wp_unslash( $_POST[ $vuln ] ) ) );
	isset( $$vuln ) and $$vuln = html_entity_decode( $$vuln );
}
// END of XSS code.
/**
 * Function related to search filter.
 *
 * @param var $query query.
 */
function tmpl_searchfilter( $query ) {
	if ( $query->query_vars[ 's' ] ) {
		$query->query_vars[ 's'] = html_entity_decode( $query->query_vars[ 's' ] );
	}
	if ( $query->query_vars[ 'paged' ] ) {
		$query->query_vars[ 'paged' ] = html_entity_decode( $query->query_vars[ 'paged' ] );
	}
	return $query;
}
add_filter( 'pre_get_posts', 'tmpl_searchfilter' );

/**
 * Return the collection for category listing page.
 */
function listing_fields_collection($post_types, $category_id = '', $taxonomy = '', $heading_type = '', $remove_post_id = '', $pakg_id = '') {
	global $wpdb, $post;
	remove_all_actions( 'posts_where' );
	$cus_post_type = get_post_type();
	$args = array(
	'post_type' => 'custom_fields',
	'posts_per_page' => -1,
	'post_status' => array( 'publish' ),
	'meta_query' => array(
	 'relation' => 'AND',
		array(
			'key' => 'post_type_' . $cus_post_type . '',
			'value' => $cus_post_type,
			'compare' => '=',
			'type' => 'text',
		),
		array(
			'key' => 'show_on_page',
			'value' => array( 'user_side', 'both_side' ),
			'compare' => 'IN',
		),
		array(
			'key' => $post_types . '_heading_type',
			'value' => array( 'basic_inf', htmlspecialchars_decode( $heading_type ) ),
			'compare' => 'IN',
		),
		array(
			'key' => 'is_active',
			'value' => '1',
			'compare' => '=',
		),
		array(
			'key' => 'show_on_listing',
			'value' => '1',
			'compare' => '=',
		)
	),
		'meta_key' => 'sort_order',
		'orderby' => 'meta_value',
		'order' => 'ASC',
	);
	$post_query = null;
	add_filter( 'posts_join', 'custom_field_posts_where_filter' );
	$post_query = new WP_Query( $args );
	remove_filter( 'posts_join', 'custom_field_posts_where_filter' );
	return $post_query;
}
/* EOF */

/**
 * This function wil return the custom fields of the post detail page.
 */
function details_field_collection() {
	global $wpdb, $post, $htmlvar_name;
	remove_all_actions( 'posts_where' );
	remove_all_actions( 'posts_orderby' );
	$cus_post_type = get_post_type();
	$args = array(
	'post_type' => 'custom_fields',
	'posts_per_page' => -1,
	'post_status' => array( 'publish' ),
	'meta_query' => array(
	 'relation' => 'AND',
		array(
			'key' => 'post_type_' . $cus_post_type . '',
			'value' => $cus_post_type,
			'compare' => '=',
			'type' => 'text',
		),
		array(
			'key' => 'show_on_page',
			'value' => array( 'user_side', 'both_side' ),
			'compare' => 'IN',
		),
		array(
			'key' => 'is_active',
			'value' => '1',
			'compare' => '=',
		),
		array(
			'key' => 'show_on_detail',
			'value' => '1',
			'compare' => '=',
		)
	),
		'meta_key' => 'sort_order',
		'orderby' => 'meta_value_num',
		'meta_value_num' => 'sort_order',
		'order' => 'ASC',
	);
	$post_meta_info = null;
	add_filter( 'posts_join', 'custom_field_posts_where_filter' );
	$post_meta_info = new WP_Query( $args );
	remove_filter( 'posts_join', 'custom_field_posts_where_filter' );
	return $post_meta_info;
}
/* EOF */

/**
 * Display the breadcrumb.
 */
function the_breadcrumb() {
	if ( ! is_home() ) {
		echo '<div class="breadcrumb"><a href="';
		echo wp_kses_post( wp_unslash( get_option( 'home' ) ) );
		echo '">' . esc_html__( 'Home', 'templatic' );
		echo '</a>';
		if ( is_category() || is_single() || is_archive() ) {
			the_category( 'title_li=' );
			if ( is_archive() ) {
				echo ' » ';
				single_cat_title();
			}
			if ( is_single() ) {
				echo ' » ';
				the_title();
			}
		} elseif ( is_page() ) {
			echo the_title();
		}
		echo '</div>';
	}
}

if ( isset( $_SERVER['REQUEST_URI'] ) && ! strstr( wp_kses_post( wp_unslash( $_SERVER['REQUEST_URI'] ) ), '/wp-admin/' ) && ( ! isset( $_REQUEST['slider_search'] ) && '' == wp_kses_post( wp_unslash( @$_REQUEST['slider_search'] ) ) ) ) {
	add_action( 'init', 'templ_featured_ordering' );
}
/**
 * Filtering for featured listing start.
 */
function templ_featured_ordering() {
	add_filter( 'posts_orderby', 'feature_filter_order', 99 );
	add_filter( 'posts_where', 'tmpl_sort_by_character' );
	add_filter( 'pre_get_posts', 'home_page_feature_listing' );
	add_filter( 'posts_orderby', 'home_page_feature_listing_orderby' );
}

/**
 * Search by alphabetical.
 *
 * @param var $where where.
 */
function tmpl_sort_by_character( $where ) {
	global $wpdb;
	if ( isset( $_REQUEST['sortby'] ) && '' != $_REQUEST['sortby'] ) {
		$where .= " AND $wpdb->posts.post_title like '" . wp_kses_post( wp_unslash( $_REQUEST['sortby'] ) ) . "%'";
	}
	return $where;
}

/**
 * Featured posts filter for listing page.
 *
 * @param var $orderby orderby.
 */
function feature_filter_order( $orderby ) {
	global $wpdb, $wp_query;
	if ( (is_category() || is_tax() || is_archive() || is_search() )  && 'product_cat' != $wp_query->tax_query->queries[0]['taxonomy'] ) {
		if ( isset( $_REQUEST['tevolution_sortby'] ) && 'title_asc' == wp_kses_post( wp_unslash( @$_REQUEST['tevolution_sortby'] ) ) || 'alphabetical' == wp_kses_post( wp_unslash( @$_REQUEST['tevolution_sortby'] ) ) ) {
			$orderby = "$wpdb->posts.post_title ASC,(select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_c' AND $wpdb->postmeta.meta_value = 'c' ) ASC";
		} elseif ( isset( $_REQUEST['tevolution_sortby'] ) && 'title_desc' == $_REQUEST['tevolution_sortby'] ) {
			$orderby = "$wpdb->posts.post_title DESC,(select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_c' AND $wpdb->postmeta.meta_value = 'c' ) DESC";
		} elseif ( isset( $_REQUEST['tevolution_sortby'] ) && 'date_asc' == $_REQUEST['tevolution_sortby'] ) {
			$orderby = "$wpdb->posts.post_date ASC,(select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_c' AND $wpdb->postmeta.meta_value = 'c' ) DESC";
		} elseif ( isset( $_REQUEST['tevolution_sortby'] ) && 'date_desc' == $_REQUEST['tevolution_sortby'] ) {
			$orderby = "$wpdb->posts.post_date DESC,(select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_c' AND $wpdb->postmeta.meta_value = 'c' ) DESC";
		} elseif ( isset( $_REQUEST['tevolution_sortby'] ) && 'random' == $_REQUEST['tevolution_sortby'] ) {
			$orderby = " (select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_c' AND $wpdb->postmeta.meta_value = 'c' ) DESC,rand()";
		} elseif ( isset( $_REQUEST['tevolution_sortby'] ) && 'reviews' == $_REQUEST['tevolution_sortby'] ) {
			$orderby = 'DESC';
			$orderby = " $wpdb->posts.comment_count $orderby,(select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_c' AND $wpdb->postmeta.meta_value = 'c' ) DESC";
		} elseif ( isset( $_REQUEST['tevolution_sortby'] ) && 'rating' == $_REQUEST['tevolution_sortby'] ) {

			$orderby = " (select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id = $wpdb->posts.ID and $wpdb->postmeta.meta_key like \"average_rating\") DESC,(select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_c' AND $wpdb->postmeta.meta_value = 'c' ) DESC";
		} else {
			$orderby = " (SELECT DISTINCT $wpdb->postmeta.meta_value from $wpdb->postmeta where ( $wpdb->posts.ID = $wpdb->postmeta.post_id) AND $wpdb->postmeta.meta_key = 'featured_c' AND $wpdb->postmeta.meta_value = 'c' ) DESC, $wpdb->posts.post_date DESC";
		}
	}
	return $orderby;
}
/* sorting option for homepage listings */
add_action( 'pre_get_posts', 'homepage_front_page_ordering', 12 );
/**
 * Sorting option for homepage listings.
 */
function homepage_front_page_ordering() {
	if ( is_home() && ( get_option( 'show_on_front' ) == 'posts' ) ) {
		add_filter( 'posts_orderby', 'homepage_front_page_order', 99 );
	}
}
/**
 * Featured posts filter for listing page.
 *
 * @param var $orderby orderby.
 */
function homepage_front_page_order( $orderby ) {
	$tmpdata = get_option( 'templatic_settings' );
	$ordervalue = @$tmpdata['tev_front_page_order'];
	if ( '' == $ordervalue ) { $ordervalue = 'desc'; }
	global $wpdb;
	if ( 'asc' == $ordervalue ) {
			$orderby = " ( select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_h' AND $wpdb->postmeta.meta_value = 'h' ) ASC, $wpdb->posts.post_title ASC";
	} elseif ( 'desc' == $ordervalue ) {
			$orderby = " (select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_h' AND $wpdb->postmeta.meta_value = 'h' ) DESC, $wpdb->posts.post_title DESC";
	} elseif ( 'random' == $ordervalue ) {
			$orderby = " (select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_h' AND $wpdb->postmeta.meta_value = 'h' ) DESC,rand()";
	} elseif ( 'dasc' == $ordervalue ) {
			$orderby = " (select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_h' AND $wpdb->postmeta.meta_value = 'h' ) DESC, $wpdb->posts.post_date ASC";
	} elseif ( 'ddesc' == $ordervalue ) {
			$orderby = " (select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_h' AND $wpdb->postmeta.meta_value = 'h' ) DESC, $wpdb->posts.post_date DESC";
	} else {
			$orderby = " (select distinct $wpdb->postmeta.meta_value from $wpdb->postmeta where $wpdb->postmeta.post_id=$wpdb->posts.ID and $wpdb->postmeta.meta_key = 'featured_h' AND $wpdb->postmeta.meta_value = 'h' ) DESC, $wpdb->posts.post_date ASC";
	}

	 return $orderby;
}

/**
 * Fetch featured posts filter for home page.
 *
 * @param var $query query.
 */
function home_page_feature_listing( &$query ) {
	if ( isset( $_REQUEST['post_type'] ) && '' != $_REQUEST['post_type'] ) :
		$post_type = @$query->query_vars['post_type'];
	else :
		$post_type = '';
	endif;
	if ( is_home() || @is_front_page() || (get_option( 'show_on_front' ) == 'posts' && ( ! is_archive() && ! is_single() && ! is_page() && ! is_tax() ) ) ) {
		$tmpdata = get_option( 'templatic_settings' );
		$home_listing_type_value = @$tmpdata['home_listing_type_value'];
		if ( ! empty( $home_listing_type_value ) && $query->is_main_query() ) {
			$attach = array( 'attachment' );
			if ( is_array( $home_listing_type_value ) ) {
				$merge = array_merge( $home_listing_type_value, $attach );
			}
			if ( 'booking_custom_field' == $post_type ) :
				$query->set( 'post_type', $post_type ); /* set custom field post type*/
			else :
				$query->set( 'post_type', @$merge ); /* set post type events */
			endif;

		}
		$query->set( 'post_status', array( 'publish' ) ); /* set post type events */
	} else {
		remove_action( 'pre_get_posts', 'home_page_feature_listing' );
	}
}

/**
 * Sort featured posts filter for home page.
 *
 * @param var $orderby orderby.
 */
function home_page_feature_listing_orderby( $orderby ) {
	global $wpdb;
	if ( is_home() || @is_front_page() ) {
		$orderby = " (SELECT DISTINCT( $wpdb->postmeta.meta_value) from $wpdb->postmeta where ( $wpdb->posts.ID = $wpdb->postmeta.post_id) AND $wpdb->postmeta.meta_key = 'featured_h' AND $wpdb->postmeta.meta_value = 'h' ) DESC, $wpdb->posts.post_date DESC";
	}
	return $orderby;
}
/* filtering for featured listing end*/

/**
 * Get the image path.
 *
 * @param int $post_id postid.
 * @param var $size thumbnail.
 */
function get_templ_image( $post_id, $size = 'thumbnail' ) {
	global $post;
	/*get the thumb image*/
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size );
	if ( '' != $thumbnail[0] ) {
		$image_src = $thumbnail[0];
	} else {
		$post_img_thumb = bdw_get_images_plugin( $post_id, $size );
		$image_src = $post_img_thumb[0]['file'];
	}
	return $image_src;
}


/**
 * Return the sorting options and views button.
 */
function tmpl_archives_sorting_opt() {
	global $wp_query, $sort_post_type;
	if ( ! is_search() ) {
		$post_type = ( get_post_type() != '' )? get_post_type() : get_query_var( 'post_type' );
		$sort_post_type = apply_filters( 'tmpl_tev_sorting_for_' . $post_type, $post_type );
	} else {
		/* on search page what happens if user search with multiple post types */
		if ( isset( $_REQUEST['post_type'] ) ) {
			if ( is_array( $_REQUEST['post_type'] ) && count( wp_kses_post( wp_unslash( $_REQUEST['post_type'] ) ) ) == 1 ) {
				$sort_post_type = wp_kses_post( wp_unslash( $_REQUEST['post_type'][0] ) );
			} else {
				$sort_post_type = wp_kses_post( wp_unslash( $_REQUEST['post_type'] ) );
			}
		}
		if ( ! $sort_post_type ) {
				$sort_post_type = 'directory';
		}
	}
	$templatic_settings = get_option( 'templatic_settings' );
	$googlemap_setting = get_option( 'city_googlemap_setting' );
	/*custom post type link */
	$current_posttype = get_post_type();
	if ( empty( $current_posttype ) ) {
		$current_posttype = $wp_query->query['post_type'];
	}
	if ( ! is_tax() && is_archive() && ! is_search() ) {
		$current_term = $wp_query->get_queried_object();
		$permalink = get_post_type_archive_link( $current_posttype );
		if ( isset( $_REQUEST['sortby'] ) ) {
			$permalink = str_replace( '&' . $sort_post_type . '_sortby=alphabetical&sortby=' . wp_kses_post( wp_unslash( $_REQUEST['sortby'] ) ), '', $permalink );
		}
	} elseif ( is_search() && isset( $_REQUEST['sortby'] ) && isset( $_SERVER['QUERY_STRING'] ) ) {
		$search_query_str = str_replace( '&' . $sort_post_type . '_sortby=alphabetical&sortby=' . wp_kses_post( wp_unslash( $_REQUEST['sortby'] ) ), '', wp_kses_post( wp_unslash( $_SERVER['QUERY_STRING'] ) ) );
		$permalink = site_url() . '?' . $search_query_str;
	} else {
		$current_term = $wp_query->get_queried_object();
		$permalink = ( $current_term->slug ) ? get_term_link( $current_term->slug, $current_term->taxonomy ) : '' ;
		if ( isset( $_REQUEST['sortby'] ) && '' != wp_kses_post( wp_unslash( $_REQUEST['sortby'] ) ) ) {
			$permalink = str_replace( '&' . $sort_post_type . '_sortby=alphabetical&sortby=' . wp_kses_post( wp_unslash( $_REQUEST['sortby'] ) ), '', $permalink );
		}
	}
	$post_type = get_post_type_object( get_post_type() );
	/* get all the request url and con-cat with permalink to get the exact results */
	$req_uri = '';
	foreach ( $_GET as $key => $val ) {
		if ( '' != $key && ! strstr( $key, '_sortby' ) ) {
			$req_uri .= $key . '=' . $val . '&';
		}
	}
	/* permalink */
	if ( false === strpos( $permalink, '?' ) ) {
		$url_glue = '?' . $req_uri;
	} else {
		$url_glue = '&amp;' . $req_uri;
	}
	/* no grid view list view if no results found */
	if ( 0 != $wp_query->found_posts ) {
	?>
	<div class='directory_manager_tab clearfix'>
	<div class="sort_options">
	<?php if ( have_posts() != '' && current_theme_supports( 'tmpl_show_pageviews' ) ) : ?>
		<ul class='view_mode viewsbox'>
			<?php if ( function_exists( 'tmpl_wp_is_mobile' ) && tmpl_wp_is_mobile() ) {
				if ( isset( $templatic_settings['category_googlemap_widget'] ) && 'yes' == $templatic_settings['category_googlemap_widget'] ) {
				?>
				<li><a class='switcher last listview <?php if ( 'listview' == $templatic_settings['default_page_view'] ) {echo 'active';}?>' id='listview' href='#'><?php esc_html_e( 'LIST VIEW', 'templatic' );?></a></li>
				<li><a class='map_icon <?php if ( 'mapview' == $templatic_settings['default_page_view'] ) {echo 'active';}?>' id='locations_map' href='#'><?php esc_html_e( 'MAP', 'templatic' );?></a></li>
			<?php }
} else { ?>
				<li><a class='switcher first gridview <?php if ( 'gridview' == $templatic_settings['default_page_view'] ) {echo 'active';}?>' id='gridview' href='#'><?php esc_html_e( 'GRID VIEW', 'templatic' );?></a></li>
				<li><a class='switcher last listview <?php if ( 'listview' == $templatic_settings['default_page_view'] ) {echo 'active';}?>' id='listview' href='#'><?php esc_html_e( 'LIST VIEW', 'templatic' );?></a></li>
				<?php if ( isset( $templatic_settings['category_googlemap_widget'] ) && 'yes' == $templatic_settings['category_googlemap_widget'] ) :?>
				<li><a class='map_icon <?php if ( 'mapview' == $templatic_settings['default_page_view'] ) {echo 'active';}?>' id='locations_map' href='#'><?php esc_html_e( 'MAP', 'templatic' );?></a></li>
				<?php endif;
}
			?>
		</ul>
	<?php endif;

if ( isset( $_GET[ $sort_post_type . '_sortby' ] ) && 'alphabetical' == $_GET[ $sort_post_type . '_sortby' ] ) {
		$_SESSION['alphabetical'] = '1';
} else {
		unset( $_SESSION['alphabetical'] );
}

if ( ! empty( $templatic_settings['sorting_option'] ) ) {

		/* take "directory" as a post type if additional post type is detected */
		$exclude_arr = apply_filters( 'exclude_sorting_posttypes', array( 'event', 'property', 'classified' ) );
	if ( ! in_array( get_post_type(), $exclude_arr ) ) {
			$sort_post_type_name = 'tevolution';
	} else {
			$sort_post_type_name = get_post_type();
	}

		$sel_sort_by = isset( $_REQUEST[ $sort_post_type_name . '_sortby' ] ) ? wp_kses_post( wp_unslash( $_REQUEST[ $sort_post_type_name . '_sortby' ] ) ) : '';
		$sel_class = 'selected=selected';
	?>
		<div class="tev_sorting_option">
			<form action="<?php if ( function_exists( 'tmpl_directory_full_url' ) ) { echo wp_kses_post( wp_unslash( tmpl_directory_full_url( 'directory' ) ) ); } ?>" method="get" id="<?php echo wp_kses_post( wp_unslash( $sort_post_type . '_sortby_frm' ) ); ?>" name="<?php echo wp_kses_post( wp_unslash( $sort_post_type . '_sortby_frm' ) ); ?>">
<select name="<?php echo wp_kses_post( wp_unslash( $sort_post_type_name . '_sortby' ) ); ?>" id="<?php echo wp_kses_post( wp_unslash( $sort_post_type_name . '_sortby' ) ); ?>" onchange="sort_as_set(this.value)" class="tev_options_sel">
				<option <?php if ( ! $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Sort By', 'templatic' ); ?></option>
				<?php
					do_action( 'tmpl_before_sortby_title_alphabetical' );
				if ( ! empty( $templatic_settings['sorting_option'] ) && in_array( 'title_alphabetical', $templatic_settings['sorting_option'] ) ) :?>
						<option value="alphabetical" <?php if ( 'alphabetical' == $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Alphabetical', 'templatic' );?></option>
				<?php endif;
					do_action( 'tmpl_after_sortby_title_alphabetical' );
					do_action( 'tmpl_before_sortby_title_asc' );
				if ( ! empty( $templatic_settings['sorting_option'] ) && in_array( 'title_asc', $templatic_settings['sorting_option'] ) ) :?>
						<option value="title_asc" <?php if ( 'title_asc' == $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Title Ascending', 'templatic' );?></option>
				<?php endif;
					do_action( 'tmpl_after_sortby_title_asc' );
					do_action( 'tmpl_before_sortby_title_desc' );
				if ( ! empty( $templatic_settings['sorting_option'] ) && in_array( 'title_desc', $templatic_settings['sorting_option'] ) ) :?>
				<option value="title_desc" <?php if ( 'title_desc' == $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Title Descending', 'templatic' );?></option>
				<?php endif;
					do_action( 'tmpl_after_sortby_title_desc' );
					do_action( 'tmpl_before_sortby_date_asc' );
				if ( ! empty( $templatic_settings['sorting_option'] ) && in_array( 'date_asc', $templatic_settings['sorting_option'] ) ) :?>
						<option value="date_asc" <?php if ( 'date_asc' == $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Publish Date Ascending', 'templatic' );?></option>
				<?php endif;
					do_action( 'tmpl_after_sortby_date_asc' );
					do_action( 'tmpl_before_date_desc' );
				if ( ! empty( $templatic_settings['sorting_option'] ) && in_array( 'date_desc', $templatic_settings['sorting_option'] ) ) :?>
						<option value="date_desc" <?php if ( 'date_desc' == $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Publish Date Descending', 'templatic' );?></option>
				<?php endif;
					do_action( 'tmpl_after_sortby_date_desc' );
					do_action( 'tmpl_before_sortby_reviews' );
				if ( ! empty( $templatic_settings['sorting_option'] ) && in_array( 'reviews', $templatic_settings['sorting_option'] ) ) :?>
						<option value="reviews" <?php if ( 'reviews' == $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Reviews', 'templatic' );?></option>
				<?php endif;
					do_action( 'tmpl_after_sortby_reviews' );
					do_action( 'tmpl_before_sortby_rating' );
				if ( ! empty( $templatic_settings['sorting_option'] ) && in_array( 'rating', $templatic_settings['sorting_option'] ) ) :?>
						<option value="rating" <?php if ( 'rating' == $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Rating', 'templatic' );?></option>
				<?php endif;
					do_action( 'tmpl_after_sortby_rating' );
					do_action( 'tmpl_before_sortby_random' );
				if ( ! empty( $templatic_settings['sorting_option'] ) && in_array( 'random', $templatic_settings['sorting_option'] ) ) :?>
						<option value="random" <?php if ( 'random' == $sel_sort_by ) { echo wp_kses_post( wp_unslash( $sel_class ) ); } ?>><?php esc_html_e( 'Random', 'templatic' );?></option>
				<?php endif;
					do_action( 'tmpl_after_sortby_random' );
					?>
			 </select>
			 </form>
		<?php add_action( 'wp_footer', 'sorting_option_of_listing' ); ?>
		</div>
	<?php
} // End if().

	?>
</div><!--END sort_options div -->
</div><!-- END directory_manager_tab Div -->
	<?php
	} // End if().
	/* On archive and category pages - alphabets order should display even there is no post type pass in argument */
	$exclude_arr = array( 'event', 'property', 'classified' );
	if ( isset( $_REQUEST['alpha_sort_post_type'] ) && '' != $_REQUEST['alpha_sort_post_type'] ) {
		$sort_post_type = wp_kses_post( wp_unslash( $_REQUEST['alpha_sort_post_type'] ) );
	}
	if ( ! in_array( $sort_post_type, $exclude_arr ) ) {
		$sort_post_type = 'tevolution';
	} else { $sort_post_type = $sort_post_type;
	}
	if ( ! $sort_post_type ) { $sort_post_type = 'tevolution'; }
	if ( ( isset( $_REQUEST[ $sort_post_type . '_sortby' ] ) && 'alphabetical' == $_REQUEST[ $sort_post_type . '_sortby' ] ) || ( isset( $_SESSION['alphabetical'] ) && 1 == $_SESSION['alphabetical'] ) || 'alphabetical' == $_REQUEST[ get_post_type() . '_sortby' ] ) :
		$alphabets = array( __( 'A', 'templatic' ), __( 'B', 'templatic' ), __( 'C', 'templatic' ), __( 'D', 'templatic' ), __( 'E', 'templatic' ), __( 'F', 'templatic' ), __( 'G', 'templatic' ), __( 'H', 'templatic' ), __( 'I', 'templatic' ), __( 'J', 'templatic' ), __( 'K', 'templatic' ), __( 'L', 'templatic' ), __( 'M', 'templatic' ), __( 'N', 'templatic' ), __( 'O', 'templatic' ), __( 'P', 'templatic' ), __( 'Q', 'templatic' ), __( 'R', 'templatic' ), __( 'S', 'templatic' ), __( 'T', 'templatic' ), __( 'U', 'templatic' ), __( 'V', 'templatic' ), __( 'W', 'templatic' ), __( 'X', 'templatic' ), __( 'Y', 'templatic' ), __( 'Z', 'templatic' ) );
		/*show all result when we click on all in alphabetical sort order*/
		if ( isset( $_REQUEST['sortby'] ) ) {
			$all = str_replace( '?sortby=' . wp_kses_post( wp_unslash( $_REQUEST['sortby'] ) ) . '&', '/?', $url_glue );
		}
	?>
<div id="directory_sort_order_alphabetical" class="sort_order_alphabetical">
		<input type="hidden" name="alpha_sort" id="alpha_sort" /> <!-- for listfilter -->
	  <ul>
			<li class="<?php echo ( ! isset( $_REQUEST['sortby'] ) ) ?'active':''?>"><a href="<?php echo esc_url( remove_query_arg( 'sortby', $permalink . $all . $sort_post_type . '_sortby=alphabetical' ) );?>"><?php esc_html_e( 'All', 'templatic' );?></a></li>
			<?php
			foreach ( $alphabets as &$value ) {
				$key = $value;
				$val = strtolower( $key );
				?>
				<li class="<?php echo ( isset( $_REQUEST['sortby'] ) && wp_kses_post( wp_unslash( $_REQUEST['sortby'] ) ) == $val )? 'active':''?>"><a href="<?php echo esc_url( $permalink . $url_glue . $sort_post_type . '_sortby=alphabetical&sortby=' . $val . '&alpha_sort_post_type=' . $sort_post_type );?>"><?php echo wp_kses_post( wp_unslash( $key ) ); ?></a></li>
				<?php
			} ?>
	  </ul>
</div>
	<?php endif;
}

/**
 * Sorting option for listing page.
 */
function sorting_option_of_listing() {
	?>
<script type="text/javascript" async >
		function sort_as_set(val)
		{
			<?php
			global $sort_post_type;
			$current_post_type = get_post_type();
			$addons_posttype = tmpl_addon_name(); /* all tevolution addons' post type as key and folter name as a value */
			$exclude_arr = apply_filters( 'exclude_sorting_posttypes', array( 'event', 'property', 'classified' ) );
			if ( ! in_array( $current_post_type, $exclude_arr ) ) {
				$sort_post_type = 'tevolution';
			} else {
				$sort_post_type = $current_post_type;
			}
			if ( function_exists( 'tmpl_directory_full_url' ) ) { ?>
			if ( document.getElementById( '<?php echo wp_kses_post( wp_unslash( $sort_post_type ) ); ?>_sortby' ).value )
			{
				<?php if ( strstr( tmpl_directory_full_url( $sort_post_type ), '?' ) ) :
						if( is_search() ){
							echo "var url = window.location.href;";
							echo "window.location = url+'&'+'".wp_kses_post( wp_unslash( $sort_post_type ) )."'+'_sortby='+val;";
						}else{
							echo "window.location = '".wp_kses_post( wp_unslash( tmpl_directory_full_url( $sort_post_type ) ) )."'+'&'+'".wp_kses_post( wp_unslash( $sort_post_type ) )."'+'_sortby='+val;";
						}
				else :
					if( is_search() ){
						echo "window.location = url+'?'+'". wp_kses_post( wp_unslash( $sort_post_type ) ) ."'+'='+val;";
					}else{
						echo "window.location = '".wp_kses_post( wp_unslash( tmpl_directory_full_url( $sort_post_type ) ) )."'+'?'+'".wp_kses_post( wp_unslash( $sort_post_type ) )."'+'='+val;";
					}
				endif; ?>
			}
			<?php } ?>
		}
	</script>
	<?php
}

/**
 * Return the related listings query without HTML.
 */
function tmpl_get_related_posts_query() {
	global $post, $claimpost, $sitepress;
	$claimpost = $post;
	$tmpdata = get_option( 'templatic_settings' );
	if ( 0 == @$tmpdata['related_post_numbers'] && '' != $tmpdata['related_post_numbers'] ) {
		return '';
	}
	$related_post = @$tmpdata['related_post'];
	$related_post_numbers = ( @$tmpdata['related_post_numbers'] ) ? @$tmpdata['related_post_numbers'] : 3;
	$taxonomies = get_object_taxonomies( (object) array(
		'post_type' => $post->post_type,
		'public'  => true,
		'_builtin' => true,
		)
	);
	/* get location wise post type */
	$location_post_type = ', ' . implode( ',', get_option( 'location_post_type' ) );
	remove_all_actions( 'posts_where' );
	if ( '' != $post->ID ) {
		if ( 'tags' == $related_post ) {
			$terms = wp_get_post_terms( $post->ID, $taxonomies[1], array(
				 'fields' => 'ids',
				 )
			);
			$postQuery = array(
			'post_type'  => $post->post_type,
			'post_status' => 'publish',
			'tax_query' => array(
						array(
							'taxonomy' => $taxonomies[1],
							'field' => 'ID',
							'terms' => $terms,
							'operator' => 'IN',
						),
					 ),
			'posts_per_page' => apply_filters( 'tmpl_related_post_per_page', $related_post_numbers ),
			'orderby'   => 'rand',
			'post__not_in' => array( $post->ID ),
			);
		} else {
			$terms = wp_get_post_terms( $post->ID, $taxonomies[0], array(
				'fields' => 'ids',
				)
			);
			$postQuery = apply_filters( 'tmpl_related_post_custom', array(
				'post_type'  => $post->post_type,
				'post_status' => 'publish',
				'tax_query' => array(
						array(
							'taxonomy' => $taxonomies[0],
							'field' => 'ID',
							'terms' => $terms,
							'operator' => 'IN',
						),
					 ),
				'posts_per_page' => apply_filters( 'tmpl_related_post_per_page', $related_post_numbers ),
				'ignore_sticky_posts' => 1,
				'orderby'   => 'rand',
				'post__not_in' => array( $post->ID ),
				)
			);
		} // End if().
	} // End if().
	if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
		remove_action( 'parse_query', array( $sitepress, 'parse_query' ) );
	}
	if ( is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) && strpos( $location_post_type, ',' . $post->post_type ) !== false ) {
		add_filter( 'posts_where', 'location_related_posts_where_filter' );
	}
	$my_query = new wp_query( $postQuery );
	if ( is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) ) {
		remove_filter( 'posts_where', 'location_related_posts_where_filter' );
	}
	return $my_query;
}

add_action( 'wp_footer', 'hide_archive_sort_option' );
/**
 * Hide sort option while map view is selected.
 */
function hide_archive_sort_option() {
	global $posts;
	if ( ( is_tax() || is_archive() ) && ! empty( $posts ) && !is_category() ) {
	?>
<script type="text/javascript" async>
		jQuery(window).load(function () {
/* Code start for map reload and center it on category page after tabs changing */
jQuery( function() {
jQuery( document ).on( 'click', "ul.view_mode li #locations_map", function() {
})
});
/* Code end */
if ( jQuery( '#locations_map' ).hasClass( 'active' ) )
			{
				jQuery( '.tev_sorting_option' ).css( 'display', 'none' );
				jQuery( '#directory_sort_order_alphabetical' ).css( 'display', 'none' );
			}
			else
			{
				jQuery( '.tev_sorting_option' ).css( 'display', '' );
				jQuery( '#directory_sort_order_alphabetical' ).css( 'display', '' );
			}
			jQuery( '.viewsbox a.listview' ).click(function(e) {
				jQuery( '.tev_sorting_option' ).css( 'display', '' );
				jQuery( '#directory_sort_order_alphabetical' ).css( 'display', '' );
			});
			jQuery( '.viewsbox a.gridview' ).click(function(e) {
				jQuery( '.tev_sorting_option' ).css( 'display', '' );
				jQuery( '#directory_sort_order_alphabetical' ).css( 'display', '' );
			});
			jQuery( '.viewsbox a#locations_map' ).click(function(e) {
				jQuery( '.tev_sorting_option' ).css( 'display', 'none' );
				jQuery( '#directory_sort_order_alphabetical' ).css( 'display', 'none' );
			});
		});
	</script>
	<?php
	} // End if().
}


/**
 * Pass the custom post types for feeds.
 */
if ( ! function_exists( 'directory_myfeed_request' ) ) {
	/**
	 * Pass the custom post types for feeds.
	 *
	 * @param var $qv qv.
	 */
	function directory_myfeed_request( $qv ) {
		
		$post_type_post['post']= (array)get_post_type_object( 'post' );
		$post_types = array();
		$custom_post_types= apply_filters('tmpl_allow_postmetas_posttype',get_option('templatic_custom_post'));
		$custom_post_types=array_merge($custom_post_types,$post_type_post);
		foreach($custom_post_types as $post_type => $value) {
			$post_types[$post_type] = $post_type;
		}
		if ( isset( $qv['feed'] ) ) {
			$qv['post_type'] = $post_types;
			
		}
		
		return $qv;
	}
}
add_filter( 'request', 'directory_myfeed_request' );

if ( ! function_exists( 'print_excerpt' ) ) {
	/**
	 * Category page and search page excerpt content limit.
	 *
	 * @param integer $length 			Cahracters to show on listing page desctipion.
	 */
	function print_excerpt( $length ) {
		// Max excerpt length. Length is set in characters.
		global $post;
		$length = ( '' == $length )? '50' : $length;
		/*condition for supreme related theme*/
		if ( function_exists( 'supreme_prefix' ) ) {
			$pref = supreme_prefix();
		} else {
			$pref = sanitize_key( apply_filters( 'hybrid_prefix', get_template() ) );
		}
		$tmpdata = get_option( $pref . '_theme_settings' );
		$morelink = @$tmpdata['templatic_excerpt_link'];
		if ( ! empty( $morelink ) ) {
			$morelink = sprintf( __( '<a href="%1$s" class="more moretag">%2$s</a>', 'templatic' ), esc_url( get_permalink() ), wp_kses_post( wp_unslash( $morelink ) ) );
		} else { $morelink = '<a class="moretag" href="' . get_permalink() . '" class="more"> <b>' . __( 'Read more' ) . '...</b></a>';
		}
		$text = $post->post_excerpt;
		if ( '' == $text ) {
			$text = $post->post_content;
			$text = apply_filters( 'the_excerpt', $text );
			$text = str_replace( ']]>', ']]>', $text );
		}
		$text = strip_shortcodes( $text ); // optional, recommended
		$text = strip_tags( $text ); // use ' $text = strip_tags( $text, '<p><a>' ); ' if you want to keep some tags.

		$text = wp_trim_words( $text, $length ); /* shows perticular words */
		if ( reverse_strrchr( $text, '. ', 1 ) ) {
			$excerpt = reverse_strrchr( $text, '. ', 1 ) . '' . sprintf( esc_html__( '%s', 'templatic' ), wp_kses_post( wp_unslash( $morelink ) ) );
		} else {
			$excerpt = $text . '' . sprintf( esc_html__( '%s', 'templatic' ), wp_kses_post( wp_unslash( $morelink ) ) );
		}
		if ( $excerpt ) {
			echo wp_kses_post( wp_unslash( apply_filters( 'the_excerpt', $excerpt ) ) );
		} else {
			echo wp_kses_post( wp_unslash( apply_filters( 'the_excerpt', $text ) ) );
		}
	}
} // End if().
