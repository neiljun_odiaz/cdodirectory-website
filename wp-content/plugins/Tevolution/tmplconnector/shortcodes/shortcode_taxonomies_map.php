<?php


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_filter( 'body_class', 'map_body_class', 11);
function map_body_class( $classes ) {
	global $post;

	$template = get_post_meta( $post->ID, '_wp_page_template', true );
	if ( $template =='page-templates/full-page-map.php' ) {
		$classes[] = 'full-width-map';
		return $classes;
	} else {
		$classes[] = '';
		return $classes;	
	}
}
/* tevolution_all_list_map - All Listings on Map */
function tevolution_all_list_map( $atts ) {
	global $current_cityinfo;
	extract( shortcode_atts( array (
		'post_type'  => 'post', 
		'latitude'  => '21.167086220869788', 
		'longitude'  => '72.82231945000001', 
		'map_type'  => 'ROADMAP', 
		'map_display' => '', 
		'zoom_level' => 13,
		'clustering' => 1,
		'height'   => 900,
		), $atts ) 
	);
	ob_start();
	$clustering = $clustering;
	if ( $post_type !='' ) {
		$post_info=(strstr( $post_type, ',' ) ) ? explode( ',', $post_type):explode( ',', $post_type) ;
	} else {
		$post_info= array( 'post' );
	}		
	$jsAdmin = TEMPL_PLUGIN_URL."/js/taxonomiesmap.js";
	wp_register_script( 'wp_show_taxonomies_map_js', $jsAdmin);
	wp_enqueue_script( 'wp_show_taxonomies_map_js' );

	$mapcategory_info =get_shortcode_map_categoryinfo( $post_info);	
	/*$mappost_info =get_map_postinfo( $post_info);	*/


	$tmpdata = get_option( 'templatic_settings' );			
	$maptype = $map_type;		
	$latitude  = $latitude;
	$longitude  = $longitude;
	$map_type  = $map_type;
	$map_display = $map_display;
	$zoom_level = $zoom_level;

	wp_print_scripts( 'wp_show_taxonomies_map_js' );

	$google_map_customizer=get_option( 'google_map_customizer' );/* store google map customizer required formate.*/
	if ( !isset( $_GET['h'] ) ) {
		$PHP_SELF = $_SERVER['PHP_SELF'];
		?>	
		<script type="text/javascript">	
			jQuery(document).ready(function() {

				var totlaheight = jQuery(window).height();
				var header = jQuery( '.header_container' ).height();	
				var footer = jQuery( '#footer' ).height();
				var removal_height = header + footer;
				var height = totlaheight - removal_height;
				var lang = '';
				<?php
				if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
					global $sitepress;
					if ( isset( $_REQUEST['lang'] ) ) {
					?>
					var lang = '<?php echo $_REQUEST['lang']; ?>'
					<?php
					} elseif ( $sitepress->get_current_language() ) {
						if ( $sitepress->get_default_language() != $sitepress->get_current_language() ) {
							?>
							var lang = '<?Php echo $sitepress->get_current_language(); ?>'
							<?php							
						} else {
							?>
							var lang = '<?php echo $sitepress->get_default_language(); ?>'
							<?php
						}
					} else {
					?>
						var lang = '';
					<?php						
					}
				}
				?>
				window.location.href = "?h=" + height + "&lang=" + lang;

			});

		</script>
		<?php }
 ?>
			<script type="text/javascript">

				var map_latitude= '<?php echo $latitude?>';
				var map_longitude= '<?php echo $longitude?>';
				var map_zomming_fact= <?php echo $zoom_level;?>;		
				<?php if ( $map_display == 1 ) { ?>
					var multimarkerdata = new Array();
					<?php }?>
					var zoom_option = '<?php echo $map_display; ?>';
					var markers = '';
					var markerArray = [];	
					var ClustererMarkers=[];
					var m_counter=0;
					var map = null;
					var mgr = null;
					var mc = null;		
					var mClusterer = null;
					var showMarketManager = false;
					var PIN_POINT_ICON_HEIGHT = 32;
					var PIN_POINT_ICON_WIDTH = 20;
					var clustering = '<?php echo $clustering; ?>';
					var infobox;
					var infoBubble;
					function initialize() {		
						var isDraggable = jQuery(document).width() > 480 ? true : true;
						var myOptions = {
							scrollwheel: false,
							clickableIcons: false,
							zoom: map_zomming_fact,
							draggable: isDraggable,
							center: new google.maps.LatLng(map_latitude, map_longitude),
							mapTypeId: google.maps.MapTypeId.<?php echo $map_type;?>
						}
						map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
						var styles = [<?php echo substr( $google_map_customizer,0,-1);?>];			
						map.setOptions({styles: styles});
						/* Initialize Fluster and give it a existing map		 */
						mgr = new MarkerManager( map );

					}

					google.maps.event.addDomListener(window, 'load', initialize);
					google.maps.event.addDomListener(window, 'load', taxo_googlemap_initialize);
				</script>   
				<div class="full_map_page full_map_template">
					<div class="map_sidebar">
						<div class="top_banner_section_in clearfix">
							<div class="TopLeft"><span id="triggermap"></span></div>
							<div class="TopRight"></div>
							<div class="iprelative">
								<div id="map_canvas" style="width: 100%; height:<?php echo $_GET['h']; ?>; " class="map_canvas"></div>        
								<div id="map_loading_div" style="width: 100%; height:<?php echo $_GET['h']-200; ?>px; display: none;"></div>           
								<div id="map_marker_nofound"><?php echo '<p>';_e( 'Your selected category do not have any records yet at your current location. ', 'templatic' );echo '</p>'; ?></div>   
							</div>       

							<form id="ajaxform" name="slider_search" class="pe_advsearch_form" action="javascript:void(0);" onsubmit="return(new_googlemap_ajaxSearch() ) ;">
								<div class="paf_search"><input type="text" class="" id="search_string" name="search_string" value="" placeholder="<?php _e( 'Title or Keyword', 'templatic' );?>" onclick="this.placeholder=''" onmouseover="this.placeholder='<?php _e( 'Title or Keyword', 'templatic' );?>'"/></div>

								<?php if ( $post_info):?>
									<div class="paf_row map_post_type" id="toggle_postID" style="display:block;">
										<?php for ( $c=0;$c<count( $post_info);$c++):
										if ( @$post_info[$c] )
										{
											$PostTypeObject = get_post_type_object( $post_info[$c] );
											$_PostTypeName = $PostTypeObject->labels->name;?>
											<div class="mw_cat_title">
												<label><input type="checkbox" data-category="<?php echo str_replace("&", '&amp;', $post_info[$c] ). 'categories';?>" onclick="taxo_googlemap_initialize(this);" value="<?php echo str_replace("&", '&amp;', $post_info[$c] );?>" <?php if ( ! empty( $_POST['posttype'] ) && !in_array(str_replace("&", '&amp;', $post_info[$c] ) , $_POST['posttype'] ) ) :?> <?php else:?> checked="checked" <?php endif;?> class="<?php echo str_replace("&", '&amp;', $post_info[$c] ). 'custom_categories';?>" id="<?php echo str_replace("&", '&amp;', $post_info[$c] ). 'custom_categories';?>" name="posttype[]"> <?php echo ucfirst( $_PostTypeName);?></label><span id='<?php echo $post_info[$c]. '_toggle';?>' class="toggle_post_type toggleon" onclick="custom_post_type_taxonomy( '<?php echo $post_info[$c]. 'categories';?>', this)"></span></div>
												<div class="custom_categories <?php echo str_replace("&", '&amp;', $post_info[$c] ). 'custom_categories';?>" id="<?php echo str_replace("&", '&amp;', $post_info[$c] ). 'categories';?>" >
													<?php foreach ( $mapcategory_info[$post_info[$c]] as $key => $value) { ?>
													<label><input type="checkbox" onclick="taxo_googlemap_initialize(this);" value="<?php echo $value['termid'];?>" <?php if ( ! empty( $_POST['categoryname'] ) && !in_array( $key, $_POST['categoryname'] ) ) :?> <?php else:?> checked="checked" <?php endif;?> id="<?php echo $key;?>" name="categoryname[]"><img height="14" width="8" alt="" src="<?php echo $value['icon']?>"> <?php echo $value['name']?></label>

													<?php }?>
												</div>

												<?php }
												endfor;?>
											</div>
											<div id="toggle_post_type" class="paf_row toggleon" onclick="toggle_post_type();"></div>
										<?php endif;?>
									</form>   

								</div>
							</div>
						</div>
						<script type="text/javascript" async>
							var maxMap = document.getElementById( 'triggermap' );		
							google.maps.event.addDomListener(maxMap, 'click', showFullscreen);
							function showFullscreen( ) {
								/* window.alert( 'DIV clicked' );*/
								jQuery( '#map_canvas' ).toggleClass( 'map-fullscreen' );
								jQuery( '.map_category' ).toggleClass( 'map_category_fullscreen' );
								jQuery( '.map_post_type' ).toggleClass( 'map_category_fullscreen' );
								jQuery( '#toggle_post_type' ).toggleClass( 'map_category_fullscreen' );
								jQuery( '#trigger' ).toggleClass( 'map_category_fullscreen' );
								jQuery( 'body' ).toggleClass( 'body_fullscreen' );
								jQuery( '#loading_div' ).toggleClass( 'loading_div_fullscreen' );
								jQuery( '#advmap_nofound' ).toggleClass( 'nofound_fullscreen' );
								jQuery( '#triggermap' ).toggleClass( 'triggermap_fullscreen' );

								jQuery( '.TopLeft' ).toggleClass( 'TopLeft_fullscreen' );		
								/*map.setCenter(darwin);*/
								window.setTimeout(function( ) { 
									var center = map.getCenter(); 
									google.maps.event.trigger(map, 'resize' ); 
									map.setCenter(center); 
								}, 100);			 }
							</script>
							<?php
							return ob_get_clean();
						}
						function get_shortcode_map_categoryinfo( $post_type) {
							for ( $i=0;$i<count( $post_type);$i++) {		
								$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type[$i], 'public'  => true, '_builtin' => true ) ) ;	
								$cat_args = array(
									'taxonomy'=>$taxonomies[0],
									'orderby' => 'name', 				
									'hierarchical' => 'true', 
									'title_li'=>''
									);	
								$r = wp_parse_args( $cat_args);	
								$catname_arr=get_categories( $r );	
								$categoriesinfo = array();
								foreach ( $catname_arr as $cat)	{			
									if ( @$cat->term_icon)
										$term_icon=$cat->term_icon;
									else
										$term_icon=apply_filters( 'tmpl_default_map_icon', TEMPL_PLUGIN_URL. 'images/pin.png' );


									$categoriesinfo[]=array( 'termid' => $cat->term_id , 'slug'=> @$cat->slug, 'name'=>$cat->name, 'icon'=>$term_icon);	


								}
								$catinfo_arr[$post_type[$i]]=$categoriesinfo;
							}		
							return $catinfo_arr;
						}
?>