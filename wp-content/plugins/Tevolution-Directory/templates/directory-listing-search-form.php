<?php
/**
 * Listing post type search form.
 *
 * @package WordPress
 * @subpackage Tevolution-Directory
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<form id="listing_searchform" action="<?php echo esc_url( home_url() ); ?>" method="get" role="search">
	<div>
		<label class="screen-reader-text" for="s"><?php esc_html_e( 'Search for','templatic' )?>:</label>
		<input id="s" type="text" name="s" value="">
		<input type="hidden" name="post_type" value="listing" />
		<input id="searchsubmit" type="submit" value="<?php esc_html_e( 'Search','templatic' );?>">
	</div>
</form>
