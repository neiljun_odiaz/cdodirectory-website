<?php
/**
 * Tevolution single custom post type Preview Page template.
 *
 * @package WordPress
 * @subpackage Tevolution-Directory
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wp_enqueue_script( 'jquery' );
wp_enqueue_script( 'jquery-ui-tabs' );
$cur_post_type = get_post_meta( $cur_post_id,'submit_post_type',true );
$tmpdata = get_option( 'templatic_settings' );
if ( isset( $_REQUEST['address'] ) ) {
	$address = sanitize_text_field( wp_unslash( $_REQUEST['address'] ) );
}
if ( isset( $_REQUEST['geo_latitude'] ) ) {
	$geo_latitude = sanitize_text_field( wp_unslash( $_REQUEST['geo_latitude'] ) );
}
if ( isset( $_REQUEST['geo_longitude'] ) ) {
	$geo_longitude = sanitize_text_field( wp_unslash( $_REQUEST['geo_longitude'] ) );
}
if ( isset( $_REQUEST['map_view'] ) ) {
	$map_type = sanitize_text_field( wp_unslash( $_REQUEST['map_view'] ) );
}
if ( isset( $_REQUEST['website'] ) ) {
	$website = sanitize_text_field( wp_unslash( $_REQUEST['website'] ) );
}
if ( isset( $_REQUEST['phone'] ) ) {
	$phone = sanitize_text_field( wp_unslash( $_REQUEST['phone'] ) );
}
if ( isset( $_REQUEST['listing_logo'] ) ) {
	$listing_logo = sanitize_text_field( wp_unslash( $_REQUEST['listing_logo'] ) );
}
if ( isset( $_REQUEST['listing_timing'] ) ) {
	$listing_timing = sanitize_text_field( wp_unslash( $_REQUEST['listing_timing'] ) );
}
if ( isset( $_REQUEST['email'] ) ) {
	$email = sanitize_email( wp_unslash( $_REQUEST['email'] ) );
}
if ( isset( $_REQUEST['proprty_feature'] ) ) {
	$special_offer = sanitize_text_field( wp_unslash( $_REQUEST['proprty_feature'] ) );
}
if ( isset( $_REQUEST['video'] ) ) {
	$video = sanitize_text_field( wp_unslash( $_REQUEST['video'] ) );
}
if ( isset( $_REQUEST['facebook'] ) ) {
	$facebook = sanitize_text_field( wp_unslash( $_REQUEST['facebook'] ) );
}
if ( isset( $_REQUEST['google_plus'] ) ) {
	$google_plus = sanitize_text_field( wp_unslash( $_REQUEST['google_plus'] ) );
}
if ( isset( $_REQUEST['twitter'] ) ) {
	$twitter = sanitize_text_field( wp_unslash( $_REQUEST['twitter'] ) );
}
if ( isset( $_REQUEST['linkedin'] ) ) {
	$linkedin = sanitize_text_field( wp_unslash( $_REQUEST['linkedin'] ) );
}
if ( isset( $_REQUEST['instagram'] ) ) {
	$instagram = sanitize_text_field( wp_unslash( $_REQUEST['instagram'] ) );
}
if ( isset( $_REQUEST['youtube'] ) ) {
	$instagram = sanitize_text_field( wp_unslash( $_REQUEST['youtube'] ) );
}
if ( isset( $_REQUEST['zooming_factor'] ) ) {
	$zooming_factor = sanitize_text_field( wp_unslash( $_POST['zooming_factor'] ) );
}
if ( isset( $_REQUEST['imgarr'] ) ) {
	$_REQUEST['imgarr'] = explode( ',',$_REQUEST['imgarr'] );
}
/* to get the common/context custom fields display by default with current post type */
if ( function_exists( 'tmpl_single_page_default_custom_field' ) && isset( $_REQUEST['cur_post_type'] ) ) {
	$tmpl_flds_varname = tmpl_single_page_default_custom_field( sanitize_text_field( wp_unslash( $_REQUEST['cur_post_type'] ) ) );
}

/* Set curent language in cookie */
if ( is_plugin_active( 'wpml-translation-management/plugin.php' ) ) {
	global $sitepress;
	$_COOKIE['_icl_current_language'] = $sitepress->get_current_language();
}
global $htmlvar_name, $heading_type, $tmpl_flds_varname;
/* Get heading type to display the custom fields as per selected section.  */
if ( function_exists( 'tmpl_fetch_heading_post_type' ) && isset( $_REQUEST['cur_post_type'] ) ) {

	$heading_type = tmpl_fetch_heading_post_type( sanitize_text_field( wp_unslash( $_REQUEST['cur_post_type'] ) ) );
}

/* get all the custom fields which select as " Show field on detail page" from back end */

if ( function_exists( 'tmpl_single_page_custom_field' ) && isset( $_REQUEST['cur_post_type'] ) ) {
	$htmlvar_name = tmpl_single_page_custom_field( sanitize_text_field( wp_unslash( $_REQUEST['cur_post_type'] ) ) );
} else {
	global $htmlvar_name;
}


/* to get the common/context custom fields display by default with current post type */
if ( function_exists( 'tmpl_single_page_default_custom_field' ) && isset( $_REQUEST['cur_post_type'] ) ) {
	$tmpl_flds_varname = tmpl_single_page_default_custom_field( sanitize_text_field( wp_unslash( $_REQUEST['cur_post_type'] ) ) );
}
?>
<script id="tmpl-foudation" src="<?php echo esc_url( TEMPL_PLUGIN_URL ); ?>js/foundation.min.js"> </script>
<div id="content" role="main" class="directory-single-page large-9 small-12 columns">
	<div class="listing type-listing listing-type-preview hentry" >
		<header class="entry-header">
			<?php if ( $listing_logo != '' ) :?>
				<div class="entry-header-logo">
					<img src="<?php echo esc_url( $listing_logo ); ?>" alt="<?php esc_html_e( 'Logo','templatic' );?>" />
				</div>
			<?php endif;?>
			<section class="entry-header-title">
			<?php if ( isset( $_REQUEST['post_title'] ) ) { ?>
				<h1 itemprop="name" class="entry-title"><?php echo wp_kses_post( wp_unslash( $_REQUEST['post_title'] ) ); ?></h1>
				<?php } ?>
				<article class="entry-header-custom-wrap">
					<div class="entry-header-custom-left">
						<?php if ( $address != '' ) :?>
							<p><?php echo wp_kses_post( $address ); ?></p>
						<?php endif;?>
						<?php if ( $website != '' ) :
							if ( ! strstr( $website,'http' ) ) {
								$website = '//' . $website;
							} ?>
						<p><a href="<?php echo wp_kses_post( $website );?>"><?php esc_html_e( 'Website','templatic' );?></a></p>
					<?php endif;?>
					<?php do_action( 'directory_display_custom_fields_preview_default_left' ); ?>
				</div>
				<div class="entry-header-custom-right">
					<?php if ( $phone != '' ) :?>
						<!--googleoff: all-->
						<p class="phone"><label><?php esc_html_e( 'Phone','templatic' );?>: </label><span class="listing_custom"><?php echo wp_kses_post( $phone );?></span></p>
						<!--googleon: all-->
					<?php endif;?>
					<?php if ( $listing_timing != '' ) :?>
						<p class="time"><label><?php esc_html_e( 'Time','templatic' );?>: </label><span class="listing_custom"><?php echo wp_kses_post( $listing_timing );?></span></p>
					<?php endif;?>
					<?php if ( $email != '' ) :?>
						<p class="email"><label><?php esc_html_e( 'Email','templatic' );?>: </label><span class="listing_custom"><?php echo wp_kses_post( $email );?></span></p>
					<?php endif;?>
					<?php do_action( 'directory_display_custom_fields_preview_default_right' ); ?>
				</div>

			</article>
		</section>
	</header>
	<section class="entry-content">
		<?php do_action( 'directory_preview_before_post_content' ); /*Add Action for after preview post content. */?>
		<div class="share_link">
			<?php if ( is_ssl() ) { $http = 'https://';
} else { $http = '//'; } ?>
				<script  type="text/javascript" async  src="<?php echo wp_kses_post( $http ); ?>s7.addthis.com/js/250/addthis_widget.js#username=xa-4c873bb26489d97f"></script>
				<?php if ( $facebook != '' ) :?>
					<a href="<?php echo esc_url( $facebook );?>" id="facebook"><i class="fab fa-facebook-f"></i> Facebook</a>
				<?php endif;?>

				<?php if ( $twitter != '' ) :?>
					<a href="<?php echo esc_url( $twitter );?>" id="twitter"><i class="fab fa-twitter"></i> Twitter</a>
				<?php endif;?>

				<?php if ( $google_plus != '' ) :?>
					<a href="<?php echo esc_url( $google_plus );?>" id="google_plus"><i class="fab fa-google-plus-g"></i> Google Plus</a>
				<?php endif;?>
				
				<?php if ( $linkedin != '' ) :?>
					<a href="<?php echo esc_url( $linkedin );?>" id="linkedin"><i class="fab fa-linkedin-in"></i> Linkedin </a>
				<?php endif;?>
				
				<?php if ( $instagram != '' ) :?>
					<a href="<?php echo esc_url( $instagram );?>" id="instagram"><i class="fab fa-instagram"></i> Instagram </a>
				<?php endif;?>
                <?php if ( $youtube != '' ) :?>
					<a href="<?php echo esc_url( $youtube );?>" id="youtube"><i class="fab fa-youtube"></i> Youtube </a>
				<?php endif;?>
			</div>


			<ul class="tabs" data-tabs role="tablist" id="single-pw-pg-tb"><!-- data-tabs id -->
				<li class="tab-title active tabs-title is-active"><a href="#listing_description"><?php esc_html_e( 'Overview','templatic' );?></a></li><!-- tabs-title is-active -->

				<?php if ( $address != '' ) :?>
					<li class="tab-title tabs-title"><a href="#listing_map"><?php esc_html_e( 'Map','templatic' );?></a></li>
				<?php endif;?>

				<?php if ( $special_offer != '' && $tmpl_flds_varname['proprty_feature'] ) :?>
					<li class="tab-title tabs-title"><a href="#special_offer"><?php echo wp_kses_post( $tmpl_flds_varname['proprty_feature']['label'] ); ?></a></li>
				<?php endif;?>

				<?php if ( $video != '' && $tmpl_flds_varname['video'] ) :?>
					<li class="tab-title tabs-title"><a href="#listing_video"><?php echo wp_kses_post( $tmpl_flds_varname['video']['label'] ); ?></a></li>
				<?php endif;?>

				<?php do_action( 'dir_end_tabs_preview' ); ?>

			</ul>
			<div class="tabs-content" data-tabs-content="single-pw-pg-tb"><!-- data-tabs-content-->
				<!--Overview Section Start -->
				<section role="tabpanel" aria-hidden="false" class="content active tabs-panel is-active" id="listing_description" > <!-- tabs-panel is-active -->
					<div class="<?php if ( $_REQUEST['imgarr'][0] != '' ) :?>listing_content<?php else : ?>content_listing <?php endif;?>">
						<?php if ( isset( $_REQUEST['post_content'] ) ) {
								echo wp_kses_post( wp_unslash( $_REQUEST['post_content'] ) ); }?>
					</div>
					<?php if ( $_REQUEST['imgarr'][0] != '' ) :?>
						<div id="directory_detail_img" class="entry-header-image">
							<?php do_action( 'directory_preview_before_post_image' );
							$thumb_img_counter = 0;
							$thumb_img_counter = $thumb_img_counter + count( $_REQUEST['imgarr'] );
							$image_path = get_image_phy_destination_path_plugin();
							$tmppath = '/' . $upload_folder_path . 'tmp/';

							foreach ( $_REQUEST['imgarr'] as $image_id => $val ) :
								$thumb_image = get_template_directory_uri() . '/images/tmp/' . trim( $val );
								break;
							endforeach;
							if ( isset( $_REQUEST['pid'] ) && $_REQUEST['pid'] != '' ) {	/* execute when comes for edit the post */
									$large_img_arr = bdw_get_images_plugin( intval( $_REQUEST['pid'] ),'directory-single-image' );
									$thumb_img_arr = bdw_get_images_plugin( intval( $_REQUEST['pid'] ),'tevolution_thumbnail' );
									$largest_img_arr = bdw_get_images_plugin( intval( $_REQUEST['pid'] ),'large' );
							}
								?>
								<div class="listing-image">
									<?php $f = 0; foreach ( $_REQUEST['imgarr'] as $image_id => $val ) :
										$curry = date( 'Y' );
										$currm = date( 'm' );
										$src = get_template_directory() . '/images/tmp/' . $val;
										$img_title = pathinfo( $val );

									?>
									<?php if ( $largest_img_arr ) : ?>
										<?php  foreach ( $largest_img_arr as $value ) :
											$tmp_v = explode( '/', $value['file'] );
											$name = end( $tmp_v );
											if ( $val == $name ) :
												?>
												<img src="<?php echo esc_url( $value['file'] );?>" alt="" width="300" height="230" class="Thumbnail thumbnail large post_imgimglistimg"/>
											<?php endif;
									endforeach;
									else : ?>
									<img src="<?php echo esc_url( $thumb_image );?>" alt="" width="300" height="230" class="Thumbnail thumbnail large post_imgimglistimg"/>
								<?php endif;
									if ( $f == 0 ) { break;
									} endforeach;?>
							</div>
							<?php  if ( count( array_filter( $_REQUEST['imgarr'] ) ) > 1 ) :?>
								<div id="gallery" class="image_title_space">
									<ul class="more_photos">
										<?php foreach ( $_REQUEST['imgarr'] as $image_id => $val ) {
											$curry = date( 'Y' );
											$currm = date( 'm' );
											$src = get_template_directory() . '/images/tmp/' . $val;
											$img_title = pathinfo( $val );
											if ( $val ) :
												if ( file_exists( $src ) ) :
													$thumb_image = get_template_directory_uri() . '/images/tmp/' . $val; ?>
												<li><img src="<?php echo esc_url( $thumb_image );?>" alt="" height="60" width="60" title="<?php echo esc_attr( $img_title['filename'] ) ?>" /></li>
											<?php else : ?>
												<?php
												if ( $largest_img_arr ) :
													foreach ( $largest_img_arr as $value ) :
														$tmpl = explode( '/', $value['file'] );
														$name = end( $tmpl );
														if ( $val == $name ) :?>
															<li><img src="<?php echo esc_attr( $value['file'] );?>" alt="" height="60" width="60" title="<?php echo esc_attr( $img_title['filename'] ) ?>" /></li>
															<?php
													endif;
													endforeach;
													endif;
													?>
												<?php endif;

												else :
													if ( $thumb_img_arr ) : ?>
												<?php
												$thumb_img_counter = $thumb_img_counter + count( $thumb_img_arr );
												$thumb_img_arr_count = count( $thumb_img_arr );
												for ( $i = 0; $i < $thumb_img_arr_count; $i++ ) :
													$thumb_image = $large_img_arr[ $i ];

													if ( ! is_array( $thumb_image ) ) :
														?>
														<li><img src="<?php echo esc_url( $thumb_image );?>" alt="" height="60" width="60" title="<?php echo esc_attr( $img_title['filename'] ); ?>" /></li>
													<?php endif;?>
										<?php endfor; ?>
									<?php endif; ?>
								<?php endif; ?>
								<?php
								$thumb_img_counter++;
}// End foreach().
	?>

						</ul>
					</div>
				<?php endif;?>
				<!-- -->
				<?php do_action( 'directory_preview_after_post_image' );?>
			</div>
		<?php endif;?>
	</section>


	<?php if ( $address != '' ) :?>
		<!--Map Section Start -->
		<section role="tabpanel" aria-hidden="false" class="content tabs-panel" id="listing_map" >
			<div id="directory_location_map" style="width:100%;">
				<div class="directory_google_map" id="directory_google_map_id" style="width:100%;">
					<?php
					include_once( TEMPL_MONETIZE_FOLDER_PATH . 'templatic-custom_fields/google_map_detail.php' );?>
				</div>  <!-- google map #end -->
			</div>
		</section>
		<!--Map Section End -->
	<?php endif;

if ( $special_offer != '' ) :?>
	<!--Special Offer Start -->
	<section role="tabpanel" aria-hidden="false" class="content tabs-panel" id="special_offer" >
		<?php echo wp_kses_post( $special_offer );?>
	</section>
	<!--Special Offer End -->
<?php endif;?>
<?php if ( $video != '' ) : ?>
	<!--Video Code Start -->
	<section role="tabpanel" aria-hidden="false" class="content tabs-panel" id="listing_video" >
		<?php
		$embed_video = wp_oembed_get( $video );
		if ( $embed_video != '' ) {
			echo wp_kses_post( $embed_video );
		} else {
			echo wp_kses_post( $video );
		}?>
	</section>
	<!--Video code End -->
<?php endif;

do_action( 'listing_end_preview' );?>
</div>
<?php do_action( 'directory_preview_page_fields_collection' ); /*Add Action for after preview post content. */?>
<div class="post-meta">
	<?php
	/* Display selected category and listing tags */
	if ( function_exists( 'directory_post_preview_categories_tags' ) && isset( $_REQUEST['category'] ) && isset( $_REQUEST['post_tags'] ) ) {
		$_REQUEST['category'] = array_filter( sanitize_text_field( wp_unslash( $_REQUEST['category'] ) ) );
		echo wp_unslash( directory_post_preview_categories_tags( sanitize_text_field( wp_unslash( $_REQUEST['category'] ) ), sanitize_text_field( wp_unslash( $_REQUEST['post_tags'] ) ) ) );
	}
	?>
</div>
</section>

</div>
</div>

<script  type="text/javascript" async >
	jQuery(function() {jQuery(document).on('click',"ul.tabs li a", function() {
		var n=jQuery(this).attr("href");
		if ( n=="#listing_map")
		{
			Demo.init();
		}
	})});
</script>
