<?php
/**
 * Add The listing custom categories fields like map marker.
 *
 * @package WordPress
 * @subpackage Tevolution-Directory
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'admin_init', 'directory_category_custom_field' );
/**
 * Created and edit the listing custom post type category custom field store in term table.
 */
function directory_category_custom_field() {
	global $wpdb;

	$tevolution_taxonomy_marker = get_option( 'tevolution_taxonomy_marker' );
	if ( $tevolution_taxonomy_marker['listingcategory'] != 'enable' ) {
		return '';
	}
	add_action( 'edited_listingcategory','directory_custom_fields_alter_fields' );
	add_action( 'created_listingcategory','directory_custom_fields_alter_fields' );

	add_filter( 'manage_listingcategory_custom_column', 'manage_directory_category_columns', 10, 3 );

	add_filter( 'manage_edit-listingcategory_columns', 'directory_category_columns' );

	/**
	 * Created and edit the event custom post type category custom field store in term table.
	 */
	if ( isset( $_GET['taxonomy'] ) && ( $_GET['taxonomy'] == 'listingcategory' ) ) {
		$taxnow = $_GET['taxonomy'];
		add_action( $taxnow . '_edit_form_fields','directory_custom_fields_edit_fields',11 );
		add_action( $taxnow . '_add_form_fields','directory_custom_fields_add_fields_action',11 );
	}
}
/**
 * Custom fields display on edit taxonomy.
 *
 * @param string $tag 				Tag Slug.
 */
function directory_custom_fields_edit_fields( $tag ) {
	directory_custom_fields_add_fields( $tag,'edit' );
}
/**
 * Custom fields display on add taxonomy.
 *
 * @param string $tag 				Tag Slug.
 */
function directory_custom_fields_add_fields_action( $tag ) {
	directory_custom_fields_add_fields( $tag,'add' );
}
/**
 * Display the map marker upload field in add/edit category of custom post type.
 *
 * @param string $tag 				Tag Slug.
 * @param string $screen 			While edit or add.
 */
function directory_custom_fields_add_fields( $tag, $screen ) {
	$tax = @$tag->taxonomy;
	?>
	<div class="form-field-category">
		<tr class="form-field form-field-category">
			<th scope="row" valign="top"><label for="cat_icon"><?php echo esc_html__( 'Map Marker', 'templatic-admin' ); ?></label></th>
			<td>
				<input id="cat_icon" type="text" size="60" name="cat_icon" value="<?php echo wp_kses_post( ( @$tag->term_icon)? @$tag->term_icon:apply_filters( 'tmpl_default_map_icon',TEMPL_PLUGIN_URL . 'images/pin.png' ) ); ?>"/>
				<?php echo esc_html__( 'Or','templatic-admin' );?>
				<a data-id="cat_icon" id="Map Marker" type="submit" class="upload_file_button button"><?php  echo esc_html__( 'Browse','templatic-admin' );?></a>
				<p class="description"><?php echo esc_html__( 'It will appear on the homepage Google map for listings placed in this category. ','templatic-admin' );?></p>
			</td>
		</tr>
	</div>
	<?php
}
/**
 * Add/ edit listing and event custom taxonomy custom field.
 *
 * @param integer $term_id 				Term id.
 */
function directory_custom_fields_alter_fields( $term_id ) {
	global $wpdb;
	$term_table = $wpdb->prefix . 'terms';
	if ( isset( $_POST['cat_icon'] ) ) {
		$cat_icon = sanitize_text_field( wp_unslash( $_POST['cat_icon'] ) );
	}
	/*update the service price value in terms table field*/
	if ( isset( $_POST['cat_icon'] ) ) {
		$sql = "update $term_table set term_icon='" . $cat_icon . "' where term_id=" . $term_id;
		$wpdb->query( $sql );
	}

}
/**
 * To Add the column of post type in categories listed table e.g. wp-admin/edit-tags.php?taxonomy=listingcategory&post_type=listing.
 *
 * @param string $columns 				Add Column.
 */
function directory_category_columns( $columns ) {
	$columns['icon'] = esc_html__( 'Map Marker','templatic-admin' );
	if ( isset( $_GET['taxonomy'] ) && $_GET['taxonomy'] == 'ecategory' ) {
		$columns['posts'] = esc_html__( 'Events','templatic-admin' );
	}
	if ( isset( $_GET['taxonomy'] ) && $_GET['taxonomy'] == 'listingcategory' ) {
		$columns['posts'] = esc_html__( 'Listings','templatic-admin' );
	}

	return $columns;
}
/**
 * Display listing and event custom taxonomy custom field display in category columns.
 *
 * @param string $out 				Output the result.
 * @param string $column_name 		Coumn name wehre to show result.
 * @param string $term_id 			Term id.
 */
function manage_directory_category_columns( $out, $column_name, $term_id ) {
	global $wpdb;
	$term_table = $wpdb->prefix . 'terms';
	$sql = "select * from $term_table where term_id=" . $term_id;
	$term = $wpdb->get_results( $sql );
	if ( $term[0]->term_icon ) {
		$icon = $term[0]->term_icon;
	} else {
		$icon = apply_filters( 'tmpl_default_map_icon',TEVOLUTION_DIRECTORY_URL . 'images/pin.png' );
	}
	switch ( $column_name ) {

		case 'icon':
			$out = '<img src="' . $icon . '" >';
		break;
		default:
		break;
	}
	return $out;
}

add_filter( 'manage_edit-listing_sortable_columns', 'templatic_edit_listing_columns',11 );
/**
 * Admin and posted on columns in manage edit listing sortable columns.
 *
 * @param string $columns 				Columns add in taxonomy.
 */
function templatic_edit_listing_columns( $columns ) {
	$columns['address'] = 'address';
	$columns['posted_on'] = 'posted_on';
	return $columns;
}
