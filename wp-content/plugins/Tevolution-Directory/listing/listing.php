<?php
/**
 * Register place taxonomy.
 *
 * @package WordPress
 * @subpackage Tevolution-Directory
 */

if ( ! defined( 'CUSTOM_POST_TYPE_LISTING' ) ) {
	@define( 'CUSTOM_POST_TYPE_LISTING','listing' );
}
define( 'CUSTOM_CATEGORY_TYPE_LISTING','listingcategory' );
define( 'CUSTOM_TAG_TYPE_LISTING','listingtags' );
define( 'CUSTOM_MENU_TITLE_LISTING',esc_html__( 'Listings','templatic-admin' ) );
define( 'CUSTOM_MENU_NAME_LISTING',esc_html__( 'Listings','templatic-admin' ) );
define( 'CUSTOM_MENU_SIGULAR_NAME_LISTING',esc_html__( 'Listing','templatic-admin' ) );
define( 'CUSTOM_MENU_ADD_NEW_LISTING',esc_html__( 'Add Listing','templatic-admin' ) );
define( 'CUSTOM_MENU_ADD_NEW_ITEM_LISTING',esc_html__( 'Add new listing','templatic-admin' ) );
define( 'CUSTOM_MENU_EDIT_LISTING',esc_html__( 'Edit','templatic-admin' ) );
define( 'CUSTOM_MENU_EDIT_ITEM_LISTING',esc_html__( 'Edit listing','templatic-admin' ) );
define( 'CUSTOM_MENU_NEW_LISTING',esc_html__( 'New listing','templatic-admin' ) );
define( 'CUSTOM_MENU_VIEW_LISTING',esc_html__( 'View listing','templatic-admin' ) );
define( 'CUSTOM_MENU_SEARCH_LISTING',esc_html__( 'Search listing','templatic-admin' ) );
define( 'CUSTOM_MENU_NOT_FOUND_LISTING',esc_html__( 'No listing found','templatic-admin' ) );
define( 'CUSTOM_MENU_NOT_FOUND_TRASH_LISTING',esc_html__( 'No listing found in trash','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_LABEL_LISTING',esc_html__( 'Listing Categories','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_TITLE_LISTING',esc_html__( 'Listing Categories','templatic-admin' ) );
define( 'CUSTOM_MENU_SIGULAR_CAT_LISTING',esc_html__( 'Category','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_SEARCH_LISTING',esc_html__( 'Search category','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_POPULAR_LISTING',esc_html__( 'Popular categories','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_ALL_LISTING',esc_html__( 'All categories','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_PARENT_LISTING',esc_html__( 'Parent category','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_PARENT_COL_LISTING',esc_html__( 'Parent category:','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_EDIT_LISTING',esc_html__( 'Edit category','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_UPDATE_LISTING',esc_html__( 'Update category','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_ADDNEW_LISTING',esc_html__( 'Add new category','templatic-admin' ) );
define( 'CUSTOM_MENU_CAT_NEW_NAME_LISTING',esc_html__( 'New category name','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_LABEL_LISTING',esc_html__( 'Listing Tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_TITLE_LISTING',esc_html__( 'Listing Tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_NAME_LISTING',esc_html__( 'Listing Tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_SEARCH_LISTING',esc_html__( 'Listing Tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_POPULAR_LISTING',esc_html__( 'Popular listing tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_ALL_LISTING',esc_html__( 'All listing tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_PARENT_LISTING',esc_html__( 'Parent listing tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_PARENT_COL_LISTING',esc_html__( 'Parent listing tags:','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_EDIT_LISTING',esc_html__( 'Edit listing tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_UPDATE_LISTING',esc_html__( 'Update listing tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_ADD_NEW_LISTING',esc_html__( 'Add new listing tags','templatic-admin' ) );
define( 'CUSTOM_MENU_TAG_NEW_ADD_LISTING',esc_html__( 'New listing tag name','templatic-admin' ) );

add_action( 'init','register_place_post_type',0 );
/**
 * Check weather auto intall is completed or not.
 */
function register_place_post_type() {
	if ( get_option( 'directory_auto_install' ) == 'true' || (is_admin() && defined( 'DOING_AJAX' ) && DOING_AJAX ) ) :

		if ( is_admin( ) ) {

			if ( get_option( 'directory_auto_install' ) == 'true' ) {
				include( TEVOLUTION_DIRECTORY_DIR . 'listing/install.php' );
			}
		}
		endif;

}

	add_action( 'admin_head','tmpl_directory_auto_install' );

if ( ! function_exists( 'tmpl_directory_auto_install' ) ) {
	/**
	 * Hook to perform auto install.
	 */
	function tmpl_directory_auto_install() {
		global $pagenow;
		if ( get_option( 'tmpl_is_tev_auto_insall' ) == 'true' && get_option( 'directory_auto_install' ) != 'true' && get_option( 'directory_auto_install' ) != '' ) :

			/* If plugin page display auto install process */
			if ( $pagenow == 'plugins.php' ) :
				echo '<div id="auto_install_html" class="notice notice-info is-dismissible">
			<p>
				<strong id="custom_message"></strong>
				<img id="install_loader" src="' . esc_url( TEVOLUTION_DIRECTORY_URL ) . 'images/install_loader.gif">
			</p>
		</div>';
		endif;

			/* Action array to define auto intall process steps */
			$process_array[] = array(
				'action' => 'tmpl_insert_listing_data',
				'message' => esc_html__( 'Setting up default options...','templatic-admin' ),
			) ;

			/* ajax url for auto install process */
			$ajax_url = esc_js( get_bloginfo( 'wpurl' ) . '/wp-admin/admin-ajax.php' );
			$counter = 1;
			$total_step = count( $process_array );
			echo '<script async type="text/javascript">window.onload = function () {
			jQuery("#auto_install_html .notice-dismiss").remove();';

			/* Call first step function on load */
			echo wp_kses_post( 'action_' . $process_array[0]['action'] . '();' );

			/* Loop through all process */
			foreach ( $process_array as $process ) {
				$script_code = 'function action_' . $process['action'] . '() {
					jQuery("#custom_message").html("' . $process['message'] . '");
					jQuery.ajax({
						url:"' . $ajax_url . '",
						type:"POST",
						data:"action=' . $process['action'] . '&auto_install=yes",
						success:function(results) {
							setTimeout(
								function()
								{
									';
				if ( $total_step != $counter ) {
					$script_code .= 'action_' . $process_array[ $counter ]['action'] . '();';
				} else {
					$script_code .= 'action_tmpl_directory_finish_install();';
					$script_code .= 'jQuery("#install_loader").remove();';
				}
									$script_code .= '}, 1000);
}
});
}';
				echo wp_kses_post( $script_code );
				$counter++;
			}
			echo 'function action_tmpl_directory_finish_install() {
	jQuery("#auto_install_html").remove();
	jQuery.ajax({
		url:"' . esc_url( $ajax_url ) . '",
		type:"POST",
		data:"action=tmpl_directory_finish_default_install",
		success:function(results) {';
			if ( $pagenow == 'plugins.php' ) :
				echo 'location.reload();';
		endif;
			echo '}
	}); }';
			echo '}</script>';
else :
	update_option( 'directory_auto_install','true' );
endif;
	}
}// End if().

/* Install listings auto install data */
add_action( 'wp_ajax_tmpl_insert_listing_data', 'tmpl_create_listing_auto_install_data' );
add_action( 'wp_ajax_tmpl_insert_listing_data', 'tables_creatation' );
add_action( 'wp_ajax_tmpl_insert_listing_data', 'register_place_post_type' );


if ( ! function_exists( 'tmpl_create_listing_auto_install_data' ) ) {
	/**
	 * Alter term_icon field in terms table if not exists.
	 */
	function tmpl_create_listing_auto_install_data() {
		global $wpdb;

		$field_check = $wpdb->get_var( "SHOW COLUMNS FROM $wpdb->terms LIKE 'term_icon'" );
		if ( 'term_icon' != $field_check ) {
			$wpdb->query( "ALTER TABLE $wpdb->terms ADD term_icon varchar(255) NOT NULL DEFAULT ''" );
		}

		/* add rule for urls */
		$tevolution_taxonomies_data1 = get_option( 'tevolution_taxonomies_rules_data' );
		$tevolution_taxonomies_data1['tevolution_single_post_add']['listing'] = 'listing';
		update_option( 'tevolution_taxonomies_rules_data', $tevolution_taxonomies_data1 );
		if ( function_exists( 'tevolution_taxonimies_flush_event' ) ) {
			tevolution_taxonimies_flush_event();
		}
		/* Delete Tevolution query catch on permalink update  changes */
		$wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->options WHERE option_name like '%s'",'%_tevolution_quer_%' ) );
		exit;
	}
}

add_action( 'wp_ajax_tmpl_directory_finish_default_install', 'tmpl_finish_listing_auto_install_data' );
if ( ! function_exists( 'tmpl_finish_listing_auto_install_data' ) ) {
	/**
	 * Complete listing auto install.
	 */
	function tmpl_finish_listing_auto_install_data() {
		global $wpdb;
		update_option( 'directory_auto_install','true' );
		exit;
	}
}

/**
 * Auto install listing sample data.
 */
function insert_listing_data() {
	global $pagenow, $wpdb;
	$theme_name = wp_get_theme();
	if ( $pagenow == 'plugins.php' && ! get_option( 'hide_listing_ajax_notification' ) && $theme_name != 'Directory 2' ) {
		$classified_auto_install = get_option( 'listing_auto_install' );
		$param = 'insert';
		$submit_text = esc_html__( 'Install sample data','templatic-admin' );
		$class = 'button button-primary';
		$msg = esc_html__( 'Wish to insert listing sample data?','templatic-admin' );
		?>
		<strong><?php echo wp_kses_post( $msg ); ?> </strong><span><a href="<?php echo esc_url( admin_url( 'plugins.php?listing_dummy=' . $param ) ); ?>" ><?php echo wp_kses_post( $submit_text ); ?></a></span>
		<?php  echo ' <a href="//templatic.com/docs/directory-plugin-guide/">' . esc_html__( 'Installation Guide','templatic-admin' ) . '</a>';
	}
}
add_filter( 'post_type_key','listing_type_key' );
/**
 * Set Listing post type as default.
 *
 * @param string $post_type_key 			Post Type.
 */
function listing_type_key( $post_type_key ) {
	if ( 0 === $post_type_key ) {
		$post_type_key = 'listing';
	} else {
		$post_type_key .= ',listing';
	}
	return $post_type_key;
}
