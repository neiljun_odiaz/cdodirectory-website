<?php
/**
 * Add action for display the Tevolution - Location Manager bundle box in tevolution plugin dashboard.
 *
 * @package WordPress
 * @subpackage Tevolution-LocationManager
 */

global $wp_query,$wpdb;
/**-- condition for activate booking system --**/
update_option( 'no_alive_days',1 );
if ( isset( $_REQUEST['activated'] ) && @$_REQUEST['activated'] == 'tevolution_location' && isset( $_REQUEST['true'] ) && @$_REQUEST['true'] == 1 ) {
	if ( ! is_plugin_active( 'Tevolution-LocationManager/location-manager.php' ) ) {
		$current = get_option( 'active_plugins' );
		$plugin = plugin_basename( trim( 'Tevolution-LocationManager/location-manager.php' ) );
		if ( ! in_array( $plugin, $current ) ) {
			$current[] = $plugin;
			sort( $current );
			update_option( 'active_plugins', $current );
		}
	}
	update_option( 'currency_symbol','$' );
	update_option( 'currency_code','USD' );
	update_option( 'currency_pos','1' );

} elseif ( isset( $_REQUEST['deactivate'] ) && $_REQUEST['deactivate'] == 'tevolution_location' && isset( $_REQUEST['true'] ) && $_REQUEST['true'] == 0 ) {
	delete_option( 'tevolution_location' );
}
