<div class="home-categories-area parent-taxonomy-listing banner-view-classic new-banner-view-category-st">
    <?php echo listingpro_banner_categories($args['id']); ?>
</div>
<div class="section-contianer">

    <div class="container page-container-five">
        <!-- page title close -->
        <div class="row">
            <!-- content open -->
            <div class="col-md-12 col-sm-12">
                <?php
                    $args = array(
                        'post_type' => 'post',
                        'post_count' => 1,
                        'posts_per_page' => 1,
                        'category_name' => $args['slug'],
                    );
                    $query = new WP_Query( $args );
                    if ( $query->have_posts() ) : ?>
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
            </div>

        </div>
    </div>

</div>