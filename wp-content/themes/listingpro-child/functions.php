<?php
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');
function my_theme_enqueue_styles()
{
    wp_enqueue_style('listingpr-parent-style', get_template_directory_uri() . '/style.css');
}

/* Remove Author meta box from post editing */
function wpse50827_author_metabox_remove()
{
    remove_meta_box('authordiv', 'listing', 'normal');
}
add_action('admin_menu', 'wpse50827_author_metabox_remove');


/* Replace with custom Author meta box */
function wpse39084_custom_author_metabox()
{
    add_meta_box('authordiv', __('Author'), 'wpse39084_custom_author_metabox_insdes', 'listing');
}
add_action('add_meta_boxes', 'wpse39084_custom_author_metabox');


/* Include all users in post author dropdown*/
/* Mimics the default metabox http://core.trac.wordpress.org/browser/trunk/wp-admin/includes/meta-boxes.php#L514 */
function wpse39084_custom_author_metabox_insdes()
{
    global $user_ID;
    global $post;
?>
    <label class="screen-reader-text" for="post_author_override"><?php _e('Author'); ?></label>

<?php
    wp_dropdown_users(array(
        'name' => 'post_author_override',
        'selected' => empty($post->ID) ? $user_ID : $post->post_author,
        'include_selected' => true
    ));
}
